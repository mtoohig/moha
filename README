=========
 M O H A
=========

### I N T R O D U C T I O N

This is the README file belonging to the Ministry of Health's Asset (MOHA) database. 
It was developed throughout 2017 with the original intention to make facility 
information available in Excel files navigateable via an web interface; however, 
the project grew and has become a complete system for managing facilities, their 
assets, and files.

Moha is written primarily with [Python] for the back end  and some [jQuery] can be 
found in the frontend especially for the maps. It is built using the [Flask] micro 
web-framework and a number of extensions including:

[Python]: https://www.python.org/
[jQuery]: https://jquery.com/
[Flask]: https://flask.pocoo.org/

- [Flask-Login] - For managing user sessions
- [Flask-Bcrypt] - For encrypting and decrypting passwords
- [Flask-Caching] - For caching resource intensive calculations (using [Redis])
- [Flask-DebugToolbar] - For assisting debuging during development
- [Flask-Migrate] - For migrating model changes in Python to SQL Server
- [Flask-SQLAlchemy] - For providing an easy Python interface to the database
- [Flask-WTForms] - For making dynamic forms and validating form data
- [Flask-Admin] - For making an admin interface

[Flask-Login]: https://flask-login.readthedocs.io/en/latest/
[Flask-Bcrypt]: http://flask-bcrypt.readthedocs.io/en/latest/
[Flask-Caching]: http://pythonhosted.org/Flask-Caching/
[Redis]: https://redis.io/
[Flask-DebugToolbar]: https://flask-debugtoolbar.readthedocs.io/en/latest/
[Flask-Migrate]: https://flask-migrate.readthedocs.io/en/latest/
[Flask-SQLAlchemy]: http://flask-sqlalchemy.readthedocs.io/en/stable/
[Flask-WTForms]: https://flask-wtf.readthedocs.io/en/stable/
[Flask-Admin]: https://flask-admin.readthedocs.io/en/latest/

Some interactive elements of Moha found in the browser were built with different 
libraries including:

- [Leaflet.js] - For GIS  maps
- [Sigma.js] - For network maps
- [PyGal] - For graphs and charts

[Leaflet.js]: http://leafletjs.com/
[Sigma.js]: http://sigmajs.org/
[PyGal]: http://pygal.org/en/stable/

The deployment of this application is hosted on OGCIO servers. The following packages 
and software are used for this deployment.

- [Gunicorn] - For handling of the WSGI protocool to the HTTP server when deployed
- [Nginx] - For handling HTTP requests and static file delivery when de

[Gunicorn]: http://gunicorn.org/
[Nginx]: https://nginx.org/en/

### Quick Start

#### Development

Create a virtual environment in the top-level directory of the project.

    virtualenv venv

Then you will activate the virtual environment with source.

    source venv/bin/activate

You can then install the required packages using the `pip` command.

    pip install -r requirements/dev.txt
    
Export the Flask variables into your environment or use `set` if on Windows.

    export FLASK_APP=autoapp.py
    export FLASK_DEBUG=1
    
Start our app with the `flask` CLI command and you should see an empty 
application.

    flask run
    
Do build the database set the `SQLALCHEMY_DATABASE_URI` in `settings.py` then 
run the following commands.

    flask db init
    flask db migrate
    flask db upgrade
    flask run
    
Whenever you make a change to the models in this application you will need 
to migrate those changes to the database. **Never** make changes directly to 
the database itself. **Always** make changes to the models then migrate as 
follows.

    flask db migrate
    
Edit the generated migration script if needed (usually not).

    flask db upgrade
    
Upgrades that change the name of column will delete the column and its data 
and add a new column with your new name. Be careful of this.

### Production

Deployment to production is varied by system and personal choices. This 
document will define how to deploy on Debian 8 with Nginx and Gunicorn as 
that is what is used on the VanGov network.

First update and upgrade your machine

    sudo apt-get update
    sudo apt-get upgrade


#### Application

To begin navigate to the `www` directory in `/var`

    cd /var/www

This is the directory where all webapps should be hosted. To add a this 
application it will have to be cloned from its repo.

    sudo git clone --depth=1 https://mtoohig@bitbucket.org/mtoohig/moha.git

The `depth` flag will make sure you do not download the history of the 
application's development into your production server. 

Next, you will have to change the permissions of the newly created `moha/` 
directory. In my case I changed the owner to my username `mtoohig` and the 
group was changed to the `webapp` usergroup so that all users of the `webapp` 
group can change the files in the future.

    sudo chown mtoohig:webapp -r moha/

Now that the permissions have been corrected we can move into the `moha` 
directory and setup the application's depencies with `virtualenv`.

    cd moha/
    virtualenv venv
    source venv/bin/activate
    (venv) pip install -r requirements/dev.txt
    (venv) deactivate

If you run into problems during installation of the requirements you may have 
to search the web for what went wrong. All the issues will be simple issues 
such as downloading dependencies to the system required by some packages. You 
may not run into any issues if other applications are already on the machine 
and those dependencies are present.

Lastly, we need to make the application into a service so that it can be set 
to run at boot and managed by the system. Options such as Upstart exist but 
`systemd` is fine.

    sudo nano /etc/systemd/system/moha.service
    
Inside the file we need to specify the details of the service and its 
location.

<code>
[Unit]
Description=Gunicorn daemon for MOHA [MoH Assets]
After=network.target

[Service]
User=mtoohig
Group=www-data
WorkingDirectory=/var/www/moha
ExecStart=/var/www/moha/venv/bin/gunicorn --workers 3 --bind unix:/var/www/moha/moha.sock wsgi

[Install]
wantedBy=multi-user.target
</code>

Use the `systemctl` command to control the application. When you want to edit 
the application, for example when pulling an update from the repo, you 
would first stop the application, pull the update then start the application.

    sudo systemctl stop moha
    cd /var/www/moha
    git pull
    sudo systemctl start moha
    sudo systemctl status moha

Notice the `status` argument to `systemctl`. It can be used to see if the 
application service is running or if it has run into any errors. Also notice 
we use `systemctl` for Nginx and our application and any other applications 
we mean we have one command to control all of our applications, cool?

#### Webserver

##### Fresh Nginx Install

Install Nginx.

    sudo apt-get install nginx

Start the service and then you can view the running webserver by going to the 
machine's IP address to view the default landing page.

    sudo /etc/init.d/nginx start

Finally, add the Nginx service to start at boot time and remove the default 
website configuration.

    sudo update-rc.d nginx default
    sudo rm /etc/nginx/sites-available/default

##### Existing Nginx

Nginx defines all of its different configuration files for each website 
behind it in the `sites-available` and `sites-enabled` directories. So for 
this application we will need to create a file in `sites-available` and make 
a link to the file in `sites-enabled`.

sudo nano /etc/nginx/sites-available/moha

<code>
server {
  listen 80;
  server_name moha.vangov.local;

  location {
    proxy_pass http://unix:/var/www/moha/moha.sock;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

  }

  location ^/static {
    root /var/www/moha/moha;

  }

}
</code>

This sets Nginx to listen on port 80 for requests for `moha.vangov.local` and 
forwards those requests to the unix socket file `moha.sock` located in our 
application.

We also write location for /static so that all content coming from the static 
directory will be served by Nginx and not served by our Flask application. 
This is because our Python application is not nearly as fast at serving 
static content as Nginx (it is built for this task afterall).

The remainder of the `proxy_set_header` directives will set the correct 
values in the head of each request.

To enable our application on Nginx we need to make a symlink into the 
`sites-enabled` directory.

    sudo ln -s /etc/nginx/sites-available/moha /etc/nginx/sites-enabled/moha 

Then restart Nginx to see our changes take effect.

   sudo systemctl restart nginx

If there were no error messages and you've already contracted the appropriate 
staff to add `moha.vangov.local` to the internal DNS then you should see the 
application when visting `moha.vangov.local` from any computer in the VanGov 
network.

Also check that the configuration is good with the following command

    sudo nginx -t




... this part needs how did we get here

Lastly, if you make any changes to yur application code itself, such as 
pulling updates from the repo, then you will need to restart the application 
with the following command.

    sudo systemctl restart moha



....

## U S E R

### Logging In

From the home page click on 'Log in' underneath the title of the application.

Use the govenrment issued email address you used when registering and password that is specific to this website.

### Registering

Click login and then 'Create Account'.

Fill in the proper information such as your name and Governement issued email address. Only a vanuatu.gov.vu email address will be accepted. Once you have registered your account make sure to then contact someone else who has access so that they can grant you full access to MOHA. This prevents random users from joining and spoiling the data.

A current user of MOHA can access the administrative panel by adding `/admin` to the end of the homepage URL. For example `http://moha.vangov.local/admin`.

From the administrative panel select `users` and browse through the users that do not have a check mark in their `confirmed` column. Clicking on the small pencil icon next to a user's row will allow you to edit that user's information including adding a tick mark to the `confirmed` checkbox. Once ticked the user will be able to log in to MOHA with their email and password that they provided during registration.

### How Facilities Work

....

## T E C H N I C A L

### Facilities

has location
has assets
has structures with inspections : has files/pictures
has personnel
has files/pictures

### Personnel

Personnel was the latest request from MoH for this webapp. It takes a simple Excel file output from HR and uploads it to re-sync personnel with facilities. The only real purpose is to identify if a facility is staffed and worth being assessed further for assets, structure repair/expansion, etc.

### Assets

Assets are an individual item found at a facility that is not a structure.  
**Structures have inspections, assets do not.**  Although that is simplifying the difference a bit too much it may help to think of it that way. It was a very business level decision and this whole application could have been written as all items following the model of 'structures' or all items following the model that 'assets' use but this app follows what the customer wants. So we have *structures* and we have *assets*.

>Although the MoH assures me that there is a *important* and *clear* distinction between an 'asset' and an 'inventory' item, I see no distinction from the perspective of writing code, so I will only use the term 'asset'.

Assets are assigned to a functional area which can be assigned to a structure that is connected to a facility. Functional areas may also have defined minimum numbers of assets required which allows us to create gap analysis for assets.

```flow
op0=>operation: Assets
op1=>operation: Functional Area
op2=>operation: Structure
op3=>operation: Facility
op0->op1
op1->op2
op2->op3
```
All assets are of a category. The `CategoryInfo` table contains all categories of assets; this table is a self-referencing [adjacency list] so that category hierarchies can be created. So that when searching for 'beds' you will also be able to return any sub-types of 'beds' such as 'beds / birthing'.

[adjacency list]: http://docs.sqlalchemy.org/en/latest/orm/self_referential.html

### Structures and Inspections

Facilities have structures. Structures are stored using [joined table inheritance] so that calling `StructureInfo` table returns all sub-structures. The `structure_type` column is the type discriminator used by joined table inheritance to indicate the type of structure represented within the row.

[joined table inheritance]: http://docs.sqlalchemy.org/en/latest/orm/inheritance.html

structure_type|Structure
:--:|--
B   |Building
WSR |Water Source
WST |Water Storage
P   |Power
S   |Sanitation
C   |Communication
CST |Cold Storage
T   |Transportation
V   |Vehicle
BO  |Boat

>The `TransportationInfo` table is actually another polymorphic table containing both `VehicleInfo` and `BoatInfo` tables while at the same time being a sub-table of `StructureInfo`.

The benefit of this design choice is all structure types can be called via `StructureInfo` and each sub-table can be called to collect rows of that structure type only. For example, calling `BuildingInfo` returns only buildings and calling `BoatInfo` returns all boats.  
`StructureInfo` contains common information about all structure types such as date of construction, status (which indicates if the structure is operational or not), description, and of course what facility does the structure belong to. The other two columns `structure_type` and `number` together form a facility-wide (not database-wide) unique identifier used by the Asset's unit to identify structures on diagrams and drawings in their other business work.

>A building structure will have the `structure_type = 'B'` and may have its `number = 2` so that its identifier will be ***B-2***.

Unique attributes to for different structure types can be found in that structure type's table. For example, `BuildingInfo` contains all rows from `StructureInfo` where `structure_type` is 'B' and unique attributes of buildings will be kept in `BuildingInfo`. `BuildingInfo` contains additional columns such as `roofType` and `WaterStorageInfo` contains additional columns such as `capacity`.
This way there does not need to be a `description` column for each structure type, instead the `description` column is found only in the `StructureInfo` table.

```python
> structure = models.StructureInfo.query.first()
> structure.structure_type
'B'
> structure.description
'A building'
> structure.area  # this is the floor area in meters
240
```

`StructureInspection` table is essentially identical to the `StructureInfo` table design except that it contains inspections for structure objects. `PowerInspection` table has inspections for `PowerInfo` table objects, etc.

Lastly, the forms for each of these tables follows the same polymorphic principle and fields are inherited so that the entire form can be created.

## L O G G I N G


:DEBUG:		For developers to follow what happens in the code.
:INFO:		For accounting or auditing. Keep records of user interaction.
:WARN:		For messages that indicate a possible future error.
:ERROR:		For errors and exceptions encountered.

Debug must be used *inside* functions to show that we have entered a function.
Debug can be used to show counts of objects iterated or removed/added.
Debug must record changes to cache.

Info must be used *outside* functions to indicate success after exiting a function.
Info must record changes to data that are commited to database.

Warn can be used for exceptions that are handled and expected but a developer could write code to expand functionality for the exception caught

Error must be used for all exceptions that not expected in normal use.
Error logs must contain enough information to help a developer troubleshoot the error.

# Style Guide

## Views

```python
#  A D D
#------
@blueprint.route('/widget/add', methods=['GET', 'POST'])
@login_required
def add_widget():
	"""Add an widget to a thing."""
	logger.debug('Enter add widget')
	whatsitID = request.args.get('whatsitID')
	things = models.ThingInfo.query.filter_by(whatsitID=whatsitID).all()
	form = forms.WidgetForm()
	form.thingID.choices = [(t.id, t.name) for t in things] 
	if form.validate_on_submit():
		newWidget = models.WidgetInfo.create(thingID=form.thingID.data,
						score=form.score.data,
						description=form.description.data)
		logger.info('Created widgetID: {0}'.format(newWidget.id))
		flash('Widget added', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('things/form.html', form=form, title='Add Widget')


```

Views in this project take on a form much like the above example view. 
0. Above the view should be a one word description of the function for to make it simple to find when browsing through the file.
1. `@blueprint` defines the route to access the view; do not use `@app`. 
	* Accepted methods defined and are capitalized such as `GET` and `POST`. 
	* Only put variables related to the route in the route such as `/widget/<int:widgetID>/update` not `/widget/<int:widgetID>/<unrelated_variable>/<True>/<Long_string_of_nonsense_that_looks_ugly>/update`
2. The definition must be consistent and simple and follow a similar naming to the route.
3. Log the entrance to the view/function/method with log level `DEBUG`.
4. Arguments not easily related to the route are collected from the request.
5. Forms are called when needed.
6. Form choices are defined and/or form fields may be removed.
7. Render the template from the blueprint's corresponding directory
8. Then `form.validate_on_submit()` accessed when the user submits the form as a `POST` request.
9. The model should use the `CRUDMixin` so that the `create` method can be called.
	* If there are many fields to in `WidgetInfo` then a `business.py` file can contain the logic for creating the object.
10. Log the creation of the new widget for accounting purposes.
11. Flash the message to the user that the widget was successfully created. 
12. Return with redirect if necessary.
	* The redirect function that works best is found in the form.

## Forms

```python
class WidgetForm(RedirectForm):
	"""Widget Form"""
	thingID = SelectField('Thing ID',
				choices=[],
				coerce=int,
				validators=[DataRequired()])
	score = IntegerField('Score of Widget',
				validators=[NumberRange(min=1, max=5, message='Must enter 1 to 5')]),
	description = StringField('Description of Widget', 
				validators=[Length(max=500, message='500 characters only.')],
				render_kw={'placeholder':'Your value here.'})
	date = DateField('Date',
				default=datetime.date.today(),
				format="%d/%m/%Y",
				validators=[DataRequired()],
				description='Date when the widget was purchased')
	
	def __init__(self, *args, **kwargs):
		"""Create instance of this form"""
		super(WidgetForm, self).__init__(*args, **kwargs)
		self.WidgetForm = None

	def validate(self):
		"""Validate the form"""
		initial_validation = super(WidgetForm, self).validate()
		if not initial_validation:
			return False
		# Custom Validation Example
		# if self.score.data == 3:
		#	self.score.errors.append('Can not accept 3')
		#	return False
		return True
```

1. The class name must be according to what the form does.
	2. The name also should match similar to the Table name it represents such as WidgetInfo and WidgetForm.
	3. In place of `RedirectForm` would normally be `Form`; but we are inheriting the `RedirectForm` characteristics in many forms found in MOHA.
3. The description text must be simple and clear; however, if the form is unusual use this space to describe what is happening.
4. Populate the form with fields. 
	5. Field should match the column name in the database whenever possible.
	6. The title must be clear and short for rendering in HTML.
	7. The description attribute can be used to explain details.
	7. Validators must be used whenever it is appropriate; do not create custom validation unless needed.
8. The `__init__` definition must be placed in all forms.
9. The `__validate__` definition must be placed in all forms.
	10. Initial validation must be performed.
	11. Custom validation should be avoided when possible.
		12. Custom validation must append why the validation failed to the field's errors list so that `utils.flash_errors` on the views will catch these errors and show them to the user.
	13. Always return `True` at the end to validate the form if no validators are triggered.

## Models

```python
class WidgetInfo(Model, SurrogatePK):
	"""A Widget"""
	__tablename__ = 'widgetInfo'
	
	score = Column(db.Integer, nullable=False)
	description = Column(db.Unicode(500), nullable=True)
	date = Column(db.Date, nullable=False)
	
	thingID = reference_col('thingInfo')  # ForeignKey
	thing = relationship('ThingInfo', back_populates='widgets')  # class attribute

	@hybrid_property
	def age(self):
		today = datetime.date.today()
		delta = today - self.date
		return '{0} days ago'.format(delta.days)

	def __unicode__(self):
		return 'widgetID: {0} of thingID: {1}'.format(self.id, self.thingID)
	
	def __repr__(self):
		return '<WidgetInfo(id={0}, score={1}, description={2}, date={3}, thingID={4})>'.format(self.id, self.score, self.description, self.date, self.thingID)
```

1. The class name must be simple and define what it represents and use [PascalCase] .
	2. 'Info' is used for tables that contain details of the resource.
	3. 'Ref' is used on the table name where the table is mostly comprised of foreign keys or is a table that is primarily used to populate a list that describes a attribute of a table named with 'Info'. 
4. `Model` and `SurrogatePK` are mixin classes found in `database.py` that help make consistent primary keys and CRUD operations possible on the classes.
	5. `SurrogatePK` is not used when the primary key can not be an integer.
6. The [documentation string] should follow normal Python conventions.
7. `__tablename__` must be the same as the class name, but [camelCase].
8. Columns of the table must be distinct and clear.
	9. Columns of a similar nature on different tables should use the same column name.
	10. Input allowed for the column should be strictly defined and match that of the validators in the Form.
11. Foreign Keys must use the `reference_col` class to easily join classes.
12. Relationships should use `back_populates` whenever possible as opposed to `backref`.
13. `hybrid_property` and `hybrid_methods` must come before the `__unicode__` definition.
14. `__unicode__` definition must be a user-friendly string that the user can read to identify the widget (row).
15. `__repr__` definition must be a machine readable string that could be used to reconstruct the row.

[PascalCase]: https://en.wikipedia.org/wiki/PascalCase
[documentation string]: https://docs.python.org/2.7/tutorial/controlflow.html#documentation-strings
[camelCase]: https://en.wikipedia.org/wiki/Camel_case
