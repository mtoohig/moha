import os

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
create_app().app_context().push()

from moh_webapp.facility import models as fm
from moh_webapp.structure import models as sm

from dateutil.parser import parse
### Working Demo to update a table in our App
##
#f = db.session.query(fm.FacilityInformation).filter_by(facilityID=u'8888').one()
#f.manager = u'michael'
#db.session.commit()


#fac = sm.BuildingInspectionInformation()
#fac.buildingID = 323
#fac.status = u'closed'
#fac.inspectorName = u'michael'
#fac.beamCond = 3
#fac.beamNote = u'How about that'

#db.session.add(fac)
#db.session.commit()


import pickle
facilities_file = pickle.load( open('facilities_fix.pkl', 'r') )

skip_list = []
with open('skip_list.txt', 'r') as f:
	for line in f:
		if line.startswith('###') or len(line) < 4:
			continue
		else:
			skip_list.append(line[:4])


import HumanHelp
match = HumanHelp.Match()

def cond_note(value):
	# Catches values greater than 500 due to database constraint
	if len(value['comments']) > 499:
		print value['comments']
		value['comments'] = raw_input("please write shorter version: ")
	return int(value['average']), value['comments']

# Loop through the dictionaries in the pickle
for facility in facilities_file:
	log = HumanHelp.log(facility, 'structural_data_migrate_2.txt')
	
	if facility in skip_list:
		log.report_info('Skipped')
		continue

	inspectorName = u'Kramer'
	inspectionDate = facilities_file[facility]['inspection_data']['inspectionDate']
	compilationDate = facilities_file[facility]['inspection_data']['compilationDate']


	# Loop through each building of this facility
	for item in facilities_file[facility]['structural_data']: # list
		print item['building']['name']
		# grab the building from the database so we can get its buildingID
		try:
			bldg = db.session.query(sm.BuildingInformation).filter_by(facilityID=facility).filter_by(name=item['building']['name']).one()
		except:
			facility_bldgs = db.session.query(sm.BuildingInformation).filter_by(facilityID=facility).all()
			bldgs = [b.name for b in facility_bldgs]
			choice = match.Table(item['building']['name'], bldgs)
			# No match; add the building
			if choice is None:
				log.report_failure('Did not match building: ' + item['building']['name'])
				bldg = sm.BuildingInformation()
				bldg.facilityID = facility
				bldg.name = item['building']['name']
				db.session.add(bldg)
				db.session.commit()
				log.report_progress("new building added: " + item['building']['name'])
				bldg = db.session.query(sm.BuildingInformation).filter_by(facilityID=facility).filter_by(name=item['building']['name']).one()
			# Our matched building name will take that building from the list for the next step
			else:
				bldg = db.session.query(sm.BuildingInformation).filter_by(facilityID=facility).filter_by(name=choice).one()
			
		# Make an inspection object for a building
		inspec = sm.BuildingInspectionInformation()	
		inspec.buildingID = bldg.buildingID
		inspec.inspectorName = inspectorName
		# Parse the string date example: "May 23rd 2013" into a standardized date format
		try:
			inspec.inspectionDate = parse(inspectionDate).date()
		# Catch any empty or 0 values and add them to the database as None	
		except:
			if inspec.inspectionDate == 0 or inspec.inspectionDate == None:
				pass
			else:
				print inspectionDate
		# Parse the string date example: "May 23rd 2013" into a standardized date format
		try:
			inspec.compilationDate = parse(compilationDate).date()
		# Catch any empty or 0 values and add them to the database as None		
		except:
			if inspec.compilationDate == 0 or inspec.compilationDate == None:
				pass
			else:
				print compilationDate

		inspec.status = u'open' # this is default because there's no way for me to know

		for key, value in item.iteritems():
			
			if key == 'roofing':
				inspec.roofCond, inspec.roofNote = cond_note(value)

			elif key == 'windows':
				inspec.windowCond, inspec.windowNote  = cond_note(value)

			elif key == 'beams':
				inspec.beamsCond, inspec.beamsNote = cond_note(value)

			elif key == 'walls':
				inspec.inWallCond, inspec.inWallNote = cond_note(value)
				inspec.exWallCond, inspec.exWallNote = cond_note(value)

			elif key == 'roofing framework':
				inspec.roofFrameCond, inspec.roofFrameNote = cond_note(value)

			elif key == 'slab':
				inspec.slabCond, inspec.slabNote = cond_note(value)

			elif key == 'columns':
				inspec.columnCond, inspec.columnNote = cond_note(value)

			elif key == 'footing':
				pass # Tim says drop this data not needed.

		db.session.add(inspec)
		db.session.commit()
		log.report_progress("inspection added to building :: " + str(inspec.buildingID))
