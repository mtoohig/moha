import os

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
create_app().app_context().push()

from moh_webapp.facility import models as fm
from moh_webapp.structure import models as sm
from moh_webapp.personel import models as pm


"""

This script calculates the buildingCond in the BuildingInspectionInformation table.

This is needed when inspections are created via scripts. If inspections are added via the 
webapp form, the form will calculate buildingCond automatically.

This is just to fill in the data where we missed it the first time around in another script.

"""

position_list = ['Medical Officer',
'Medical Officer ',
'Medical Officer  ',
'Medical officer  1',
'Medical Officer  1 Port Olry ',
'Medical Officer  1 Tasmalum ',
'Medical officer 1',
'Medical Officer 1',
'Medical Officer 1 ',
'Medical Officer 1  ',
'Medical Officer 1 Fanafo ',
'Medical Officer 1 LOH',
'Medical Officer 1 Malau',
'Medical Officer 1 Nogugu '


				]

for person in db.session.query(pm.PersonelInformation).all():
	if person.positionTitle:
		if person.positionTitle in position_list and person.positionTitle is not None:
			person.titleID = 3
			pm.PersonelInformation.save(person)
		
db.session.commit()
