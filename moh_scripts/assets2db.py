import HumanHelp
import os
import pickle

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
app = create_app()
app.app_context().push()

from moh_webapp.asset import models as am
from moh_webapp.facility import models as fm
from moh_webapp.structure import models as sm

file_dir = "C:\\project\\moh_scripts\\files\\"

# Walk the directories to read all asset pickle files
for root, dirs, files in os.walk(file_dir):
	for file in files:
		# Check if its one of our Pickle files containing assets
		if file.endswith('completed_assets.pkl'):
			file_name = file.split('completed_assets.pkl')[0]
			f = pickle.load( open( os.path.join(root, file), 'rb') )
			for facilityID, asset_list in f.iteritems():
				for asset in asset_list:
					# Create empty Instance of AssetInformation
					a = am.AssetInformation()
					# Fill in the Information
					a.facilityID = facilityID
					a.building = asset['building']
					a.description = asset['description']
					a.model = asset['model']
					a.dimensions = asset['dimensions']
					a.quantity = asset['quantity']
					a.statusNote = asset['opStatusNote']
					a.status = asset['opStatus']
					a.BMENum = asset['BMENum']
					if isinstance(asset['cost'], int):
						a.cost = asset['cost'] 
					a.location = asset['location']
					a.purchaseTime = asset['purchaseDate']
					a.supplier = asset['supplier']
					a.manufacturer = asset['manufacturer']
					a.serialNum = asset['serialNum']
					a.vNum = asset['vNum']
					# Save the Asset
					am.AssetInformation.save(a)

# Commit the Additions
db.session.commit()
