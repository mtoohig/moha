import os
import pickle
from PIL import Image
from datetime import datetime

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
app = create_app()
app.app_context().push()
### MAKE CONTEXT INTO PROJECTS

from moh_webapp.facility import models

from werkzeug.utils import secure_filename
import base64
import uuid


def get_a_uuid():
	# Generate Unique ID for the picture file name
	r_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
	return r_uuid.replace('=', '')


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in app.config['ALLOWED_PICTURE_EXTENSIONS']

root_dir = "\\\\10.0.5.2\\Assets\\Filing System\\8. Infrastructure Assets\\"

directories = pickle.load( open('facilityID_directory_match.pkl', 'r') )
for directory, facilityID in directories.iteritems():
	#log = HumanHelp.log(directory, "major_picture_link_migration.txt")

	striped_dir = directory.strip('S:')
	count = 0
	for root, dirs, files in os.walk(os.path.join(root_dir, striped_dir)):
		for file in files:
			if allowed_file(os.path.join(root, file)):
				ExFile = models.FacilityExFileInformation()

				ExFile.facilityID = facilityID
				ExFile.fileOriginalName = file
				#ExFile.fileName = file
				ExFile.fileDesc = " - ".join(root.strip(root_dir).split('\\'))
				ExFile.fileDateUploaded = datetime.now()
				ExFile.fileDateCreated = datetime.fromtimestamp(int(os.path.getctime(os.path.join(root, file))))
				ExFile.filePath = os.path.join(root, file)
				ExFile.fileType = file.rsplit('.', 1)[1]
				ExFile.fileSize = os.stat( os.path.join(root, file) ).st_size
				ExFile.thumbID  = unicode(get_a_uuid() + "." + ExFile.fileType)
				
				thumb_directory = os.path.join(app.config['APP_DIR'],'static','files','pictures',str(facilityID),'thumbs')
				thumb = Image.open(os.path.join(root,file))
				thumb.thumbnail((200,200), Image.ANTIALIAS)
				if not os.path.exists(thumb_directory):
					os.makedirs(thumb_directory)
				thumb.save(os.path.join(thumb_directory, ExFile.thumbID))

				models.FacilityExFileInformation.save(ExFile)

				count = count + 1

	print "{} photos added to facility {}".format(count, facilityID)
	db.session.commit()