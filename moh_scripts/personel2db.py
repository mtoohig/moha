import HumanHelp
import os
from openpyxl import load_workbook

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
app = create_app()
app.app_context().push()

from moh_webapp.asset import models as am
from moh_webapp.facility import models as fm
from moh_webapp.structure import models as sm
from moh_webapp.personel import models as pm

file = "C:\\project\\moh_webapp2\\moh_webapp\\moh_scripts\\personnel_data.xlsx"

ap = pm.PersonelInformation.query.all()
for a in ap:
	pm.PersonelInformation.delete(a)
db.session.commit()

wb = load_workbook(file)
ws = wb.worksheets[0]

for row in ws.iter_rows(row_offset=2):
	p = pm.PersonelInformation()
	p.vnpfNumber = row[1].value
	p.pcv = row[2].value
	p.name = row[3].value
	p.pn = row[4].value
	p.positionTitle = row[5].value
	p.fund = row[6].value
	p.costCenterCode = row[7].value
	p.costCenterName = row[8].value
	p.activity = row[9].value
	p.department = row[10].value
	p.jobCode = row[11].value
	p.salaryGrade = row[12].value
	p.percentSalaryCost = row[13].value
	p.baseSalary = row[14].value
	p.cola = row[15].value
	p.childAllowance = row[16].value
	p.houseAllowance = row[17].value
	p.homeIslandtravel = row[18].value
	p.totalSalary = row[19].value
	pm.PersonelInformation.save(p)

