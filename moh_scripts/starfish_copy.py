import os
import glob
import shutil
import time

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
app = create_app()
app.app_context().push()

src = '\\\\10.0.5.2\\Assets\\Filing System\\8. Infrastructure Assets\\'

#dst = 'c:\\project\\moh_webapp2\\moh_webapp\\moh_scripts\\starfish\\'
dst = '\\\\10.255.134.44\\files\\starfish\\MALAMPA\\'
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in app.config['ALLOWED_PICTURE_EXTENSIONS']

def copy(src, dst, found):
	#print " - - " + found
	destination = found.replace(src, dst)
	#print " -+- " + destination
 	try:
		if not os.path.isdir(destination):
			os.makedirs(os.path.dirname(destination))
	except:
		pass
	shutil.copy2(found, destination)
	#time.sleep(.2)

for root, dirs, files in os.walk(src):
	#print "*"*10
	#print root
	#print "*"*10
	for f in files:
		if allowed_file(f):
			found = os.path.join(root, f)
			copy(src, dst, found)
		#if f.endswith('.xls'):
			#found = os.path.join(root, f)
			#copy(src, dst, found)
		#if f.endswith('.xlsx'):
			#found = os.path.join(root, f)
			#copy(src, dst, found)