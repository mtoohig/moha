import os
import sys
from prettytable import PrettyTable


class log(object):
	"""	This log class is best used in a loop where it is initalized at
		the beginning of each loop. The loop item being the subject of
		the log and the logfile not changing.
	"""

	def __init__(self, subject, logfile="log.txt"):
		self.log_subject = subject
		self.file = logfile

	def report_failure(self, report):
		# Write all failures to file for human review
		with open(self.file, "a") as myfile:
			myfile.write("###ALERT###" + self.log_subject + " -=- " + report + "\n")

	def report_progress(self, report):
		with open(self.file, "a") as myfile:
			myfile.write(self.log_subject + " -=- " + str(report) + "\n")

	def report_info(self, report):
		with open(self.file, "a") as myfile:
			myfile.write("===INFO===" + self.log_subject + " -=- " + str(report) + "\n")

	def __call__(self, facility):
		self.__init__(facility)



### TODO fix Table() so that it can handle empty inputs and other oddities

class Match(object):

	def __init__(self):
		pass
		# Add Log support? have logs directly in the Table
		# Also add dictionary of found matches so future matches are automatic
		#
		#
		#
		#

	def _GetSubstrings(self, S, T):
		"""	Locating the longest substring of two strings allows the program
			to suggest a option from the table that may be a good match. 
		"""
		m = len(S)
		n = len(T)
		counter = [[0]*(n+1) for x in range(m+1)]
		longest = 0
		lcs_set = set()
		for i in range(m):
			for j in range(n):
				if S[i] == T[j]:
					c = counter[i][j] + 1
					counter[i+1][j+1] = c
					if c > longest:
						lcs_set = set()
						longest = c
						lcs_set.add(S[i-c+1:i+1])
					elif c == longest:
						lcs_set.add(S[i-c+1:i+1])
		return lcs_set

	def Table(self, match, match_list, title="Match Options", sortList=True):
		"""	Print a table of options to the terminal with a suggested best match.

			match: the item you are looking to match from the match_list
			match_list: the list of options for finding a match
			title: the string at the top of the matching table
			
			TODO
			 - add support for more columns in table
			 - add support for exit with -2 to show a greater list of options 
		"""
		if len(match_list) == 0:
			return None

		t = PrettyTable(['num', title])
		t.align = 'l'
		longest_substring = 0

		best_match = "None (enter -2)"
		if sortList == True:
			match_list = sorted(match_list)
		for num, item in enumerate(match_list):
			substring = self._GetSubstrings(match, item)
			for sub in substring:
				if len(sub) > longest_substring:
					longest_substring = len(sub)
					best_match = item
					best_num = num
			t.add_row([num, item])
		print t

		print "\n -- M A T C H -- \n"
		print "            : ", match
		print "  best match: ", best_match

		while True:	
			try:
				selection = int(raw_input("\nSelect match(-2:exit)[suggest: "+str(best_num)+"]: "))
				if selection in range(len(match_list)):
					break
				elif selection == -1 or selection == -2:
					break
				elif selection == -3:
					sys.exit()
				elif selection == -4:
					break
				else:
					print "Please select a number representing a facility or -2 if no match"
			except ValueError:
				selection = best_num
				break
			except:
				if selection == -3:
					raise
				print "\nFailed to input a number"	
		if selection == -2:
			return None
		elif selection == -4:
			return int(raw_input("enter facilityID: "))
		else:
			f_id = match_list[selection]
		return f_id