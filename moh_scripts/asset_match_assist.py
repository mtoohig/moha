import HumanHelp
import os
import pickle
import openpyxl

from fuzzywuzzy import fuzz

# Declare the working directory and files
file_dir = "C:\\project\\moh_scripts\\files\\"
in_xl_file = "Copy of 170612_Prioritized HCDisp_Medquip (costed).xlsx"

# Load the standards excel file
wb = openpyxl.load_workbook(filename=os.path.join(file_dir, in_xl_file), data_only=True)
ws = wb['Priority facility list']

standards_list = []
for row in ws: 
	standards_list.append({'p': int(row[0].value) if row[0].value else None,	# Priority
							't': row[1].value if row[1].value else None,		# Type, Equipement
							'as': row[2].value if row[2].value else None,		# Additional Specs
							'hc': int(row[3].value) if row[3].value else None,	# Health Centre, minimum requirement of type
							'd': int(row[4].value) if row[4].value else None,	# Dispensary, minimum requirement of type
							'c': int(row[5].value) if row[5].value else None,	# Cost, type
							'hcc': int(row[6].value) if row[6].value else None,	# Health Center Cost
							'dc': int(row[7].value) if row[7].value else None,	# Dispensary Cost
							'n': row[8].value if row[8].value else None})		# Notes

known_assets = pickle.load( open('all_asset_descriptions.pkl', 'rb') )

match = HumanHelp.Match()

asset_assist = {}
for standard in [s['t'] for s in standards_list]:
	asset_assist[standard] = []

	for ka in known_assets:
		if fuzz.token_set_ratio(standard, ka) > 50:
			print "###\nstandard Name: ", standard
			print "        asset: ", ka
			print fuzz.token_set_ratio(standard, ka)
			if raw_input("\ny/n: ") == 'y':
				asset_assist[standard].append(ka)

	while True:
		ans = match.Table(standard, known_assets)
		if ans is None:
			break
		asset_assist[standard].append(ans)

	pickle.dump( asset_assist, open('asset_match_assist.pkl', 'wb') )
	print asset_assist
