import os
import sys # to exit
import pickle
import openpyxl

from fuzzywuzzy import fuzz

# Add our parent directory to PATH (it contains moh_webapp)
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 
# Create a local context
from moh_webapp.app import db, create_app
app = create_app()
app.app_context().push()

# Import facility models
from moh_webapp.facility import models as fm

# Declare the working directory and files
file_dir = "C:\\project\\moh_scripts\\files\\"
in_xl_file = "Copy of 170612_Prioritized HCDisp_Medquip (costed).xlsx"
out_xl_file = "C:\\project\\moh_webapp2\\moh_webapp\\moh_scripts\\test2.xlsx"

known_matches = pickle.load( open('asset_match_assist.pkl', 'rb') )

# Load the standards excel file
wb = openpyxl.load_workbook(filename=os.path.join(file_dir, in_xl_file), data_only=True)
ws = wb['Priority facility list']

standards_list = []
for row in ws: 
	standards_list.append({'p': int(row[0].value) if row[0].value else None,	# Priority
							't': row[1].value if row[1].value else None,		# Type, Equipement
							'as': row[2].value if row[2].value else None,		# Additional Specs
							'hc': int(row[3].value) if row[3].value else None,	# Health Centre, minimum requirement of type
							'd': int(row[4].value) if row[4].value else None,	# Dispensary, minimum requirement of type
							'c': int(row[5].value) if row[5].value else None,	# Cost, type
							'hcc': int(row[6].value) if row[6].value else None,	# Health Center Cost
							'dc': int(row[7].value) if row[7].value else None,	# Dispensary Cost
							'n': row[8].value if row[8].value else None})		# Notes
	

# Create new workbook to save our final results
wb = openpyxl.Workbook()
fp = wb['Sheet'] # frontpage
fp.cell(row=4, column=1, value="Province")
fp.cell(row=4, column=2, value="FacilityID")
fp.cell(row=4, column=3, value="Facility Name")
fp.cell(row=4, column=4, value="Priority 1 Items")
fp.cell(row=4, column=5, value="Priority 2 Items")
fp.cell(row=4, column=6, value="Priority 3 Items")
fp.cell(row=4, column=7, value="Priority 4 Items")
frontrow = 5

# Walk the directories to read all asset pickle files
for root, dirs, files in os.walk(file_dir):
	for file in files:
		# Check if its one of our Pickle files containing assets
		if file.endswith('completed_assets.pkl'):
			file_name = file.split('completed_assets.pkl')[0]
			f = pickle.load( open( os.path.join(root, file), 'rb') )
			for facilityID, asset_list in f.iteritems():
				cost_to_facility = 0 # Collect price to pay for new items

				# Get the facility information so we can find its **Designation**
				facility = db.session.query(fm.FacilityInformation)\
								.filter_by(facilityID=facilityID)\
								.join(fm.DesignationRef)\
								.join(fm.Province)\
								.add_columns(fm.DesignationRef.designation,\
											fm.FacilityInformation.facilityName,\
											fm.FacilityInformation.facilityID,\
											fm.FacilityInformation.altFacilityName,\
											fm.Province.ProvinceName).one()
				# Save the designation
				d = facility.designation
				# Skip facilities that are not either "Dispensary" or "Health Centre"
				if d not in ["Dispensary", "Health Centre"]:
					continue
				
				dispensary = True if d.startswith('D') else False

				# Set the title of the sheet
				title = facility.facilityName + ", " + file_name
				if len(title) > 30 or len(file_name) > 30:
					title = facility.facilityID
				# Create a WorkSheet in the Excel Workbook
				ws = wb.create_sheet(title=title[:30])
				# Fill the basic info at the top of the sheet
				ws['A1'].value = "Facility Name"
				ws['A1'].style = 'Title'
				ws['A2'].value = facility.facilityName
				ws['B1'].value = "Alt Facility Name"
				ws['B2'].value = facility.altFacilityName
				ws['C1'].value = "Designation"
				ws['C2'].value = facility.designation
				ws['D1'].value = "Province"
				ws['D2'].value = facility.ProvinceName

				# Header of the table
				ws.cell(row=4, column=1, value = "Matching Item Available")
				ws.cell(row=4, column=2, value = "priority")
				ws.cell(row=4, column=3, value = "Equipment Type")
				ws.cell(row=4, column=4, value = "Additional Specs")
				ws.cell(row=4, column=5, value = "Dispensary" if dispensary else "Health Centre")
				ws.cell(row=4, column=6, value = "Cost")
				ws.cell(row=4, column=7, value = "Dispensary Cost" if dispensary else "Health Centre Cost")
				ws.cell(row=4, column=8, value = "Notes")

				row = 5
				for standard_item in standards_list:
					ws.cell(row=row, column=2, value = standard_item['p'])
					ws.cell(row=row, column=3, value = standard_item['t'])
					ws.cell(row=row, column=4, value = standard_item['as'])
					ws.cell(row=row, column=5, value = standard_item['d'] if dispensary else standard_item['hc'])
					ws.cell(row=row, column=6, value = standard_item['c'])
					ws.cell(row=row, column=7, value = standard_item['dc'] if dispensary else standard_item['hcc'])
					ws.cell(row=row, column=8, value = standard_item['n'])

					for asset in [a['description'] for a in asset_list if a['description']]:
						if standard_item['t'] in known_matches.keys():
							if asset in known_matches[standard_item['t']]:
								ws.cell(row=row, column=1).value = asset
								break

						#match_ratio = fuzz.token_sort_ratio(standard_item['t'], asset)
						#if match_ratio > 80:
						#	ws.cell(row=row, column=1).value = "Yes"
						#	ws.cell(row=row, column=9).value = asset
						#	break

						# Very uncertain guesses
						#match_ratio = fuzz.token_set_ratio(standard_item['t'], asset)
						#if match_ratio > 50:
						#	ws.cell(row=row, column=1).value = "Maybe"
						#	ws.cell(row=row, column=9).value = asset

					else:
						ws.cell(row=row, column=2).style = 'Bad' 
						ws.cell(row=row, column=3).style = 'Bad'
						ws.cell(row=row, column=4).style = 'Bad'
						ws.cell(row=row, column=5).style = 'Bad'
						ws.cell(row=row, column=6).style = 'Bad'
						ws.cell(row=row, column=7).style = 'Bad'
						ws.cell(row=row, column=8).style = 'Bad'

						if ws.cell(row=row, column=6).value:
							cost_to_facility = cost_to_facility + ws.cell(row=row, column=6).value
							ws.cell(row=2, column=6+ws.cell(row=row, column=2).value, value = cost_to_facility)
							ws.cell(row=1, column=6+ws.cell(row=row, column=2).value, value = "Cost of Items of Priority: "+str(ws.cell(row=row, column=2).value))

					row = row + 1	

				# Configure the page a bit
				# Resize column widths
				for dimension in ws.column_dimensions.values():
					dimension.auto_size = True

			fp.cell(row=frontrow, column=1, value=facility.ProvinceName)		
			fp.cell(row=frontrow, column=2, value=facility.facilityID)
			fp.cell(row=frontrow, column=3, value=facility.facilityName)

			fp.cell(row=frontrow, column=4, value=ws.cell(row=2, column=7).value) # price of cat 1 items
			fp.cell(row=frontrow, column=5, value=ws.cell(row=2, column=8).value) # price of cat 2 items
			fp.cell(row=frontrow, column=6, value=ws.cell(row=2, column=9).value) # price of cat 3 items
			fp.cell(row=frontrow, column=7, value=ws.cell(row=2, column=10).value) # price of cat 4 items

			fp.cell(row=frontrow, column=8, value=ws.cell(row=2, column=10).value) # total price of items

			frontrow = frontrow + 1

wb.save(filename=out_xl_file)
#a = set(all_assets)
#pickle.dump( a, open(os.path.join(file_dir, 'all_asset_descriptions.pkl'), 'wb') )
sys.exit()

