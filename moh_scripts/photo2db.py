import os
import sys
import pickle
from PIL import Image
from datetime import datetime
from prettytable import PrettyTable

import ctypes

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
app = create_app()
app.app_context().push()

from werkzeug.utils import secure_filename
import base64
import uuid

from moh_webapp.facility import models as fm
from moh_webapp.structure import models as sm

import HumanHelp
match = HumanHelp.Match()

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in app.config['ALLOWED_PICTURE_EXTENSIONS']

def get_a_uuid():
	# Generate Unique ID for the picture file name
	r_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
	return r_uuid.replace('=', '')

def print_buildings(facilityID):
	# Print all buildings of the facility that pictures could be associated with
	t = PrettyTable(['Buildings', 'ID'])
	t.align = 'l'
	buildings = db.session.query(sm.BuildingInformation).filter_by(facilityID=facilityID).all()
	for b in buildings:
		t.add_row([b.name, b.buildingID])
	print t
	return buildings

def print_directories(root, dirs):
	# Print directories of current pictures being assigned their placement
	root = root.split(' Assets\\', 1)[1]
	t = PrettyTable(['num', 'pic', root])
	t.align = 'l'
	for count, d in enumerate(dirs):
		if '.' in d and d.rsplit('.')[1].lower() in ['jpg', 'bmp', 'jpeg', 'gif', 'png']:
			t.add_row([count, '***', d])
		else:
			t.add_row([count, '', d])
	return t

def print_commands():
	t = PrettyTable(['cmd', 'meaning'])
	t.add_row([8, 'mark directory as unknown'])
	t.add_row([2, 'look inside directory'])
	t.add_row([5, 'add all pictures in directory to building'])
	t.add_row([4, 'add directory to facility'])
	t.add_row([6, 'add pictures to assets of facility'])
	return t

def get_photo_dir(file_dir, striped_dir):
	# Get the directory of 'photos' for this facility
	print os.path.join(file_dir, striped_dir)
	try:
		if os.path.isdir( os.path.join(file_dir, striped_dir, 'Photos') ):
			photo_dir = 'Photos'
		elif os.path.isdir( os.path.join(file_dir, striped_dir, '3. Photos') ):
			photo_dir = '3. Photos'
		elif os.path.isdir( os.path.join(file_dir, striped_dir, '3 Photos') ):
			photo_dir = '3 Photos'
		elif os.path.isdir( os.path.join(file_dir, striped_dir, '3.Photos') ):
			photo_dir = '3.Photos'
		else:
			log.report_failure("Unknown Directory for 'Photos', here are options: ", str(os.listdir( os.path.join('P:\\', directory.strip('S:')))))
	except WindowsError as e:
		print e

	return photo_dir

def add_photos_to_facility(facilityID, path):
	#kdll = ctypes.windll.LoadLibrary("kernel32.dll")
	count = 0
	for root, dirs, files in os.walk(path):
		for file in files:
			f = os.path.join(root, file)
			if not allowed_file(f):
				continue
			picInfo = fm.FacilityFileInformation()
			picInfo.facilityID = facilityID
			picInfo.fileType = file.rsplit('.', 1)[1]
			picInfo.fileOriginalName = secure_filename(file)
			# create a unique fileName
			picInfo.fileName = get_a_uuid() + "." + picInfo.fileType
			picInfo.fileDesc = root.split('\\')[-1]
			picInfo.fileDateCreated = datetime.strptime(Image.open(f)._getexif()[36867], "%Y:%m:%d %H:%M:%S")
			picInfo.fileDateUploaded = datetime.now()
			picInfo.default = True
			#os.rename((os.path.join(f)), (os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName)))
			#kdll.CreateSymbolicLinkA( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName), os.path.join(f), 0)
			picInfo.fileSize = 0#os.stat( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ).st_size
			fm.FacilityFileInformation.save(picInfo)
			db.session.rollback()
			count = count + 1

	#db.session.commit()
	log.report_progress("added %d pictures to facility :: %s" % (count, facilityID)) 
	print str(count), "pictures added."

def add_photo_to_building(buildingID, path, file):
	picInfo = sm.BuildingFileInformation()
	i = db.session.query(sm.BuildingInspectionInformation).filter_by(buildingID=buildingID).order_by(sm.BuildingInspectionInformation.inspectionDate.desc()).first()
	picInfo.inspectionID = i.inspectionID
	picInfo.buildingID = buildingID
	picInfo.fileType = file.rsplit('.', 1)[1]
	picInfo.fileOriginalName = secure_filename(file)
	# create a unique fileName
	picInfo.fileName = get_a_uuid() + "." + picInfo.fileType
	picInfo.fileDesc = root.split('\\')[-1]
	#picInfo.fileDateCreated = datetime.strptime(Image.open(os.path.join(path, file))._getexif()[36867], "%Y:%m:%d %H:%M:%S")
	picInfo.fileDateUploaded = datetime.now()
	picInfo.default = True
	#print os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName), os.path.join(f)
	#os.rename((os.path.join(path, file)), (os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName)))
	#kdll.CreateHardLinkA( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName), os.path.join(f), 0)
	picInfo.fileSize = os.stat(os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ).st_size #os.stat( os.path.realpath( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ) ).st_size
	sm.BuildingFileInformation.save(picInfo)
	#db.session.rollback()


def add_photos_to_building(buildingID, path):
	#kdll = ctypes.windll.LoadLibrary("kernel32.dll")
	count = 0
	for root, dirs, files in os.walk(path):
		for file in files:
			f = os.path.join(root, file)
			if not allowed_file(f):
				continue
			picInfo = sm.BuildingFileInformation()
			i = db.session.query(sm.BuildingInspectionInformation).filter_by(buildingID=buildingID).order_by(sm.BuildingInspectionInformation.inspectionDate.desc()).first()
			picInfo.inspectionID = i.inspectionID
			picInfo.buildingID = buildingID
			picInfo.fileType = file.rsplit('.', 1)[1]
			picInfo.fileOriginalName = secure_filename(file)
			# create a unique fileName
			picInfo.fileName = get_a_uuid() + "." + picInfo.fileType
			picInfo.fileDesc = root.split('\\')[-1]
			picInfo.fileDateCreated = datetime.strptime(Image.open(f)._getexif()[36867], "%Y:%m:%d %H:%M:%S")
			picInfo.fileDateUploaded = datetime.now()
			picInfo.default = True
			#os.rename((os.path.join(f)), (os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName)))
			print os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName), os.path.join(f)
			kdll.CreateHardLinkA( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName), os.path.join(f), 0)
			picInfo.fileSize = os.stat(os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ).st_size #os.stat( os.path.realpath( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ) ).st_size
			sm.BuildingFileInformation.save(picInfo)
			#db.session.rollback()
			count = count + 1

	db.session.commit()
	log.report_progress("added %d pictures to building :: %s" % (count, buildingID)) 
	print str(count), "pictures added."


def add_photos_to_assets(facilityID, path):
	#kdll = ctypes.windll.LoadLibrary("kernel32.dll")
	count = 0
	for root, dirs, files in os.walk(path):
		for file in files:
			f = os.path.join(root, file)
			if not allowed_file(f):
				continue
			picInfo = fm.FacilityFileInformation()
			picInfo.facilityID = facilityID
			picInfo.fileType = file.rsplit('.', 1)[1]
			picInfo.fileOriginalName = secure_filename(file)
			# create a unique fileName
			picInfo.fileName = get_a_uuid() + "." + picInfo.fileType
			picInfo.fileDesc = root.split('\\')[-1]
			picInfo.fileDateCreated = datetime.strptime(Image.open(f)._getexif()[36867], "%Y:%m:%d %H:%M:%S")
			picInfo.fileDateUploaded = datetime.now()
			picInfo.default = True
			#os.rename((os.path.join(f)), (os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName)))
			#kdll.CreateSymbolicLinkA( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName), os.path.join(f), 0)
			picInfo.fileSize = 0#os.stat( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ).st_size
			log.report_progress("added %d pictures to assets of facility :: %s" % (count, facilityID)) 
			fm.FacilityFileInformation.save(picInfo)
			db.session.rollback()
			count = count + 1

	#db.session.commit()
	print str(count), "pictures added."


"""

file_dir = '\\\\starfish\\Assets\\Filing System\\8. Infrastructure Assets'
#file_dir = 'C:\\project\\moh_webapp2\\moh_webapp\\moh_webapp\\static\\files\\yet_to_add\\'
facilities = db.session.query(fm.FacilityInformation).all()


### This script took the log file and re-placed found photos into the database.
##
#with open('C:\\project\\moh_webapp2\\moh_webapp\\moh_scripts\\picture_log.txt') as f:
#	lines = f.readlines()
#
#for buildingID, pictureName in [(line.strip().split('::')[1], line.split(' -=- ')[0]) for line in lines if not line.strip().startswith('###') and len(line) > 10]:
#	print buildingID, pictureName	
#	add_photo_to_building(buildingID, app.config['TEMP_DEST'], pictureName)




directories = pickle.load( open('facilityID_directory_match.pkl', 'r') )
#
# Pre-matched directories and facilityID dictionary
for directory, facilityID in directories.iteritems():
	log = HumanHelp.log(directory, "major_picture_migration.txt")

	striped_dir = directory.strip('S:')
	photo_dir = get_photo_dir(file_dir, striped_dir)


	# root directory of photos
	root = os.path.join(file_dir, striped_dir, photo_dir)

	while True:
		# all directories inside photos
		dirs = os.listdir(root)
		
		print print_commands()
		buildings = print_buildings(facilityID)
		print print_directories(root, dirs)

		cmd = raw_input("\nwhat to do: ")
		
		if cmd == "n":
			log.report_progress("continue to next facility")
			break
		elif cmd == 'q':
			log.report_failure('Quit command received')
			sys.exit()
		elif cmd == '1':
			# Go up one level
			root = os.path.normpath( os.path.join(root, "..") )
			continue
		elif cmd == '4':
			# add current dir to facility
			add_photos_to_facility(facilityID, root)
		else:
			# split the command between spaces
			parts = cmd.split()


		if len(parts) == 2:
			c_s = int(cmd.split()[0]) # command selection
			d_s = int(cmd.split()[1]) # directory selection

			if c_s == 8:
				# mark unknown picture directory
				log.report_failure('Unkown directory %s' % (dirs[d_s]))
			elif c_s == 2:
				# Look inside directory
				print print_directories( os.path.join(root, dirs[d_s]), os.listdir(os.path.join(root, dirs[d_s])) )
			elif c_s == 3:
				# go down level
				root = os.path.join(root, dirs[d_s])
			elif c_s == 4:
				# add photos to facility
				add_photos_to_facility(facilityID, os.path.join(root, dirs[d_s]))
				log.report_progress('added all photos from :: %s :: to facility :: %s' % (os.path.join(root, dirs[d_s]), facilityID))
			elif c_s == 6:
				# add photos to assets
				add_photos_to_assets(facilityID, os.path.join(root, dirs[d_s]))
				log.report_progress('added all photos from :: %s :: to assets of facility :: %s' % (os.path.join(root, dirs[d_s]), facilityID))
			else:
				pass

		elif len(parts) == 3:
			c_s = int(cmd.split()[0]) # command selection
			d_s = int(cmd.split()[1]) # directory selection
			b_s = int(cmd.split()[2]) # building selection (buildingID)
			
			if c_s == 5:
				# add pictures to building
				add_photos_to_building(b_s, os.path.join(root, dirs[d_s]))
				log.report_progress('added all photos from :: %s :: to building :: %s :: of facilityID :: %s' % (os.path.join(root, dirs[d_s]), b_s, facilityID))
			
		else:
			pass

		print "please select a command from the list or 'n' or 'q'"



		#	if file.rsplit('.')[1].lower() in ['jpg', 'bmp', 'jpeg', 'gif', 'png']:
		#		print root


"""
##### ORIGINAL ######
facilities = db.session.query(fm.FacilityInformation).all()
print facilities
file_dir = 'C:\\project\\moh_webapp2\\moh_webapp\\moh_webapp\\static\\files\\yet_to_add\\'
for root, dirs, files in os.walk(file_dir):
	
	for file in files:
		log = HumanHelp.log(file, "picture_log2.txt")
		# Create picture object
		picInfo = sm.BuildingFileInformation()
		# Choose the facility from the picture name
		print file
		facility = int(raw_input("facilityID: "))  # match.Table(file.rsplit('.',1)[0], [f.facilityName for f in facilities])
		if facility is 0:
			log.report_failure("could not find facility")
			continue
		
		# Collect facility row from database
		f = db.session.query(fm.FacilityInformation).filter_by(facilityName=facility).one()
		# Collect the faciltie's buildings
		buildings = [b.name for b in db.session.query(sm.BuildingInformation).filter_by(facilityID=f.facilityID).all()]
		# If no buildings mark it in logs					
		if not buildings or len(buildings) < 1:
			log.report_failure("no buildgings in :: %s" % (facility))
			continue
		# Choose the building to match the picture
		print buildings 
		building = match.Table(file.rsplit('.',1)[0], buildings)
		b = db.session.query(sm.BuildingInformation).filter_by(facilityID=f.facilityID).filter_by(name=building).one()
		# Collect the most recent inspection from the buildingID
		i = db.session.query(sm.BuildingInspectionInformation).filter_by(buildingID=b.buildingID).order_by(sm.BuildingInspectionInformation.inspectionDate.desc()).first()
		if not i:
			log.report_failure('No inspection yet - can not add photo')
			continue
		picInfo.inspectionID = i.inspectionID
		picInfo.fileType = file.rsplit('.', 1)[1]
		picInfo.fileOriginalName = secure_filename(file)
		# Open the file so we can hash the file to creat-2e a unique fileName
		picInfo.fileName = get_a_uuid() + "." + picInfo.fileType	
		#picInfo.fileDesc = None
		picInfo.fileDateCreated = datetime.now()
		picInfo.fileDateUploaded = datetime.now()
		picInfo.default = True
		os.rename((os.path.join(app.config['TEMP_DEST'], file)), (os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName)))
		picInfo.fileSize = os.stat( os.path.join("\\\\10.255.134.44\\files\\pictures\\", picInfo.fileName) ).st_size
		log.report_progress("added picture to building :: %s" % (b.buildingID)) 

		sm.BuildingFileInformation.save(picInfo)
		db.session.commit()

