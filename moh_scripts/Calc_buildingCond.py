import os

import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0,parentdir) 

from moh_webapp.app import db, create_app
create_app().app_context().push()

from moh_webapp.facility import models as fm
from moh_webapp.structure import models as sm
from moh_webapp.personel import models as pm


"""

This script calculates the buildingCond in the BuildingInspectionInformation table.

This is needed when inspections are created via scripts. If inspections are added via the 
webapp form, the form will calculate buildingCond automatically.

This is just to fill in the data where we missed it the first time around in another script.

"""


for inspection in [bldg for bldg in db.session.query(sm.BuildingInspectionInformation).all()]:
	conditions = []
	#print inspection.buildingCond, inspection.buildingID
	if inspection.buildingCond is None and inspection.inspectorName == "Kramer":
		conditions = [inspection.subCond,
						inspection.inWallCond,
						inspection.exWallCond,
						inspection.windowCond,
						inspection.shutterCond,
						inspection.inDoorCond,
						inspection.exDoorCond,
						inspection.floorCond,
						inspection.verandahCond,
						inspection.roofCond,
						inspection.roofFrameCond,
						inspection.ceilingCond,
						inspection.fasciaCond,
						inspection.gutterCond,
						inspection.columnCond,
						inspection.beamCond]
		conditions = [x for x in conditions if x is not None]
		inspection.buildingCond = int( sum(conditions) / len(conditions) )
		# Uncomment below to make changes to database
		db.session.commit()
	