# -*- coding: utf-8 -*-
"""Facility forms."""
import datetime

from flask_wtf import Form
from wtforms import StringField, SelectField, FloatField, DateField, FileField, TextAreaField, BooleanField, IntegerField
from wtforms.validators import DataRequired, Length, NumberRange

import models as fm


class StarfishForm(Form):
	"""Edit Facility."""

	directory = SelectField('Directory', choices=[])

	def __init__(self, *args, **kwargs):
		"""Create instance."""
		super(StarfishForm, self).__init__(*args, **kwargs)
		self.starfish = None

	def validate(self):
		"""Validate the form."""
		initial_validation = super(StarfishForm, self).validate()
		if not initial_validation:
			return False
		
		return True


class FacilityFileForm(Form):
	"""Add File to Facility"""

	file = FileField('Picture/File', render_kw={'multiple': 'multiple'}, validators=[DataRequired()],
								description='Select multiple files at once using the `CTRL` key to load them all together.')

	description = StringField('Desciption',
								render_kw={'placeholder': 'Description of Picture/File'},
								validators=[Length(min=0, max=500, message='File description may not exceed 500 characters.')])

	dateCreated = DateField('Date of Picture/File',
								default=datetime.datetime.now().date(),
								validators=[DataRequired()],
								format="%d/%m/%Y")

	def __init__(self, *args, **kwargs):
		"""Create instance."""
		super(FacilityFileForm, self).__init__(*args, **kwargs)
		self.FacilityFileForm = None

	def validate(self):
		"""Validate the form."""
		initial_validation = super(FacilityFileForm, self).validate()
		if not initial_validation:
			return False

		return True


class FacilityNoteForm(Form):
	"""Add a Note to Facility"""

	note = TextAreaField('Note', render_kw={'placeholder': 'Type your note here.'},
								validators=[DataRequired(message="You didn't write anything!"), Length(max=2000, message="May not exceed 2000 characters")])

	def __init__(self, *args, **kwargs):
		"""Create instance."""
		super(FacilityNoteForm, self).__init__(*args, **kwargs)
		self.FacilityNoteForm = None

	def validate(self):
		"""Validate the form."""
		initial_validation = super(FacilityNoteForm, self).validate()
		if not initial_validation:
			return False

		return True


class FacilitySearchForm(Form):
	"""Search Facility List"""

	facilityName = StringField('Facility Name')

	designation = SelectField('Designation', choices=[], coerce=int)

	province = SelectField('Province', choices=[], coerce=int)

	healthZone = SelectField('Health Zone', choices=[])

	island = SelectField('Island', choices=[], coerce=int)

	def validate(self):
		"""Validate the form."""
		initial_validation = super(FacilitySearchForm, self).validate()
		if not initial_validation:
			return False

		return True


class FacilityForm(Form):
	"""Edit Facility."""

	facilityID = StringField('Facility ID',
								render_kw={"placeholder": "Facility ID"},
								validators=[DataRequired()])

	facilityName = StringField('Facility Name',
								render_kw={"placeholder": "Facility Name"},
								validators=[DataRequired()])

	altFacilityName = StringField('Local Facility Name',
								render_kw={"placeholder": "Local Facility Name"})

	dataSource = StringField('Data Source', 
								render_kw={"placeholder": "Data Source"})

	designationID = SelectField('Designation', 
								choices=[], 
								coerce=int,
								render_kw={"placeholder": "Designation"},
								validators=[DataRequired()], )

	provinceID = SelectField('Province', 
								choices=[],
								coerce=int,
								render_kw={"placeholder": "Province"},
								validators=[DataRequired()])

	islandID = SelectField('Island', 
								choices=[],
								coerce=int,
								render_kw={"placeholder": "Island"},
								validators=[])#RequiredIf('healthZone')])

	healthZoneID = SelectField('Health Zone', 
								choices=[],
								coerce=int,
								render_kw={"placeholder": "Health Zone"},
								validators=[])#RequiredIf('islandID')])

	areaCouncilID = SelectField('Area Council',
								choices=[],
								coerce=int,
								render_kw={"placeholder": "Area Council"},
								validators=[DataRequired()])
	
	longD = IntegerField('Degrees')
	
	longM = IntegerField('Minutes', 
								validators=[NumberRange(min=0, max=59, message="Must be between 0 and 59")])
	
	longS = FloatField('Seconds')

	latD = IntegerField('Degrees')
	
	latM = IntegerField('Minutes', 
								validators=[NumberRange(min=0, max=59, message="Must be between 0 and 59")])
	
	latS = FloatField('Seconds')

	ownershipID = SelectField('Ownership', 
								choices=[], 
								coerce=int,
								render_kw={"placeholder": "Onwership"},
								validators=[DataRequired()])

	settingID = SelectField('Setting', 
								choices=[], 
								coerce=int,
								render_kw={"placeholder": "Setting"},
								validators=[DataRequired()])

	status = BooleanField('Status', default=True, description="Is the facility Open?")
								
	landOwner = StringField('Land Owner',
								render_kw={"placeholder": "Land Owner"})

	landOwnerContact = StringField('Land Owner Contact',
								render_kw={"placeholder": "Land Owner Contact"})

	landTitle = StringField('Land Title',
								render_kw={"placeholder": "Land Title"})

	manager = StringField('Facility Manager',
								render_kw={"placeholder": "Facility Manager"})

	managerContact = StringField('Manager Contact',
								render_kw={"placeholder": "Manager Contact"})

	def __init__(self, *args, **kwargs):
		"""Create instance."""
		super(FacilityForm, self).__init__(*args, **kwargs)
		self.facility = None

	def validate(self):
		"""Validate the form."""
		initial_validation = super(FacilityForm, self).validate()
		if not initial_validation:
			return False

		if self.latD.data < 13:
			self.latD.errors.append('There are no islands that far north')
			return False

		if self.latD.data > 22:
			self.latD.errors.append('There are no islands that far south')
			return False

		if self.longD.data < 166:
			self.longD.errors.append('There are no islands that far east')
			return False

		if self.longD.data > 171:
			self.longD.errors.append('There are no islands that far west')
			return False

		if self.latS.data < 0 or self.latS.data > 60:
			self.latS.errors.append('must be between 0 and 60')
			return False

		if self.longS.data < 0 or self.longS.data > 60:
			self.latS.errors.append('must be between 0 and 60')
			return False

		return True