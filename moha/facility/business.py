import os
import datetime

from flask import current_app as app
from moha.database import db
from moha import utils

import models as fm
from moha.structure import models as sm
from moha.personnel import models as pm

from sqlalchemy.orm.exc import NoResultFound

from moha import cache_utils

import logging
logger = logging.getLogger("moha.facility.business")

def create_facility(form):
	logger.debug("Try to add facilityID: {0}".format(form.facilityID.data))
	fm.FacilityInfo.create(facilityID=form.facilityID.data,
							facilityName=form.facilityName.data,
							altFacilityName=form.altFacilityName.data,
							status=form.status.data,
							designationID=form.designationID.data,
							provinceID=form.provinceID.data,
							islandID=form.islandID.data,
							healthZoneID=form.healthZoneID.data,
							areaCouncilID=form.areaCouncilID.data,
							latD=form.latD.data,
							latM=form.latM.data,
							latS=form.latS.data,
							longD=form.longD.data,
							longM=form.longM.data,
							longS=form.longS.data,
							longitude= (float(form.longD.data) + (form.longM.data / 60.) + form.longS.data / 3600.),
							latitude= (float(form.latD.data) + (form.latM.data / 60.) + form.latS.data / 3600.) * -1,
							ownershipID=form.ownershipID.data,
							settingID=form.settingID.data,
							landOwner=form.landOwner.data,
							landOwnerContact=form.landOwnerContact.data,
							manager=form.manager.data,
							managerContact=form.managerContact.data,
							landTitle=form.landTitle.data)


def update_facility_info(facility, form):
	logger.debug("Try to update facilityID: {0}".format(facility.facilityID))
	fm.FacilityInfo.update(facility,
							facilityID=form.facilityID.data,
							facilityName=form.facilityName.data,
							altFacilityName=form.altFacilityName.data,
							status=form.status.data,
							designationID=form.designationID.data,
							provinceID=form.provinceID.data,
							islandID=form.islandID.data,
							healthZoneID=form.healthZoneID.data,
							areaCouncilID=form.areaCouncilID.data,
							latD=form.latD.data,
							latM=form.latM.data,
							latS=form.latS.data,
							longD=form.longD.data,
							longM=form.longM.data,
							longS=form.longS.data,
							longitude= (float(form.longD.data) + (form.longM.data / 60.) + form.longS.data / 3600.),
							latitude= (float(form.latD.data) + (form.latM.data / 60.) + form.latS.data / 3600.) * -1,
							ownershipID=form.ownershipID.data,
							settingID=form.settingID.data,
							landOwner=form.landOwner.data,
							landOwnerContact=form.landOwnerContact.data,
							manager=form.manager.data,
							managerContact=form.managerContact.data,
							landTitle=form.landTitle.data)


def add_facility_file(file, form, save_dir, ref_table, facilityID):
	fileType = file.filename.rsplit('.', 1)[1]
	fileName = utils.get_a_uuid() + "." + fileType
	fileOriginalName = utils.make_secure_filename(file.filename)
	fileDesc = form.description.data
	fileDateCreated = form.dateCreated.data
	fileDateUploaded = datetime.datetime.now()
	file.seek(0) # Return the read position of the file to the beginning 
	file.save(os.path.join(save_dir, fileName))
	fileSize = os.stat( os.path.join(save_dir, fileName) ).st_size
	newFile = fm.FileInfo.create(fileName = fileName,
									fileOriginalName = fileOriginalName,
									fileType = fileType,
									fileDesc = fileDesc,
									fileSize = fileSize,
									fileDateUploaded = datetime.datetime.now(),
									fileDateCreated = fileDateCreated)
	ref_table.create(fileID=newFile.id, facilityID=facilityID)