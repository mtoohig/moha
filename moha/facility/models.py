# -*- coding: utf-8 -*-
"""Facility models."""
import datetime

from flask import url_for, render_template_string
from flask_login import UserMixin

from moha.database import CRUDMixin, Column, Model, SurrogatePK, db, reference_col, relationship, hybrid_property, hybrid_method, aggregated, func, select, synonym
from moha.extensions import bcrypt


# =========== #
#  F I L E S  #
# =========== #

class FileInfo(Model, SurrogatePK):
	__tablename__ = 'fileInfo'
	
	fileName = db.Column(db.String, nullable=False)   
	fileType = db.Column(db.String(15), nullable=False)
	fileSize = db.Column(db.Integer, nullable=False)
	fileDesc = db.Column(db.String)
	fileOriginalName = db.Column(db.String, nullable=False)
	fileDateUploaded = db.Column(db.Date, nullable=False)
	fileDateCreated = db.Column(db.Date, nullable=True)

	def __repr__(self):
		return '<FileInfo(fileName={0}, fileType={1}, fileSize={2}, fileDesc={3}, fileOriginalName={4})>'.format(self.fileName, self.fileType, self.fileSize, self.fileDesc, self.fileOriginalName)

class FacilityFileRef(Model):
	__tablename__ = 'facilityFileRef'
	fileID = reference_col('fileInfo', primary_key=True)
	file = relationship('FileInfo', backref='facilityFiles')

	id = synonym('facilityID')
	facilityID = reference_col('facilityInfo', pk_name='facilityID', primary_key=True)
	facility = relationship('FacilityInfo', backref='files')

	default = Column(db.Boolean, nullable=True, default=False)  # TODO make default False 

	def __repr__(self):
		return '<FacilityFileRef(fileID={0}, facilityID={1})>'.format(self.fileID, self.facilityID)

class FacilityOtherFileRef(Model):
	__tablename__ = 'facilityOtherFileRef'
	fileID = reference_col('fileInfo', primary_key=True)
	file = relationship('FileInfo', backref='facilityOtherFiles')

	id = synonym('facilityID')
	facilityID = reference_col('facilityInfo', pk_name='facilityID', primary_key=True)
	facility = relationship('FacilityInfo', backref='otherFiles')

	default = Column(db.Boolean, nullable=True, default=False)  # TODO make default False

	def __repr__(self):
		return '<FacilityOtherFileRef(fileID={0}, facilityID={1})>'.format(self.fileID, self.facilityID)

class FacilityPicRef(Model):
	__tablename__ = 'facilityPicRef'
	fileID = reference_col('fileInfo', primary_key=True)
	file = relationship('FileInfo', backref='facilityPics')

	id = synonym('facilityID')
	facilityID = reference_col('facilityInfo', pk_name='facilityID', primary_key=True)
	facility = relationship('FacilityInfo', backref='pics')

	default = Column(db.Boolean, nullable=False, default=False)

	def __repr__(self):
		return '<FacilityPicRef(fileID={0}, facilityID={1}, default={2})>'.format(self.fileID, self.facilityID, self.default)


# =================== #
#  G E O G R A P H Y  #
# =================== #

class ProvinceInfo(Model, SurrogatePK):
	__tablename__ = 'provinceInfo'
	name = db.Column(db.Unicode(50), nullable=False)

	healthZones = relationship('HealthZoneInfo', order_by=('HealthZoneInfo.name'), back_populates='province')
	islands = relationship('IslandInfo', order_by=('IslandInfo.name'), back_populates='province')

	@hybrid_property
	def total_facilities(self):
		return len(self.facilities)

	# calculate the average condition of all facilities of the province
	@hybrid_property
	def condition_facilities(self):
		values = [f.condition() for f in self.facilities]
		try:
			return sum(values) / float(len(values))
		except ZeroDivisionError:
			return 0

	@hybrid_property
	def total_positions(self):
		return sum([len(f.positions) for f in self.facilities])

	@hybrid_property
	def total_positions_filled(self):
		return sum([len(f.positions_filled) for f in self.facilities])

	@hybrid_property
	def total_positions_unfilled(self):
		return sum([len(f.positions_unfilled) for f in self.facilities])

	@hybrid_property
	def total_positions_filled_percent(self):
		try:
			return self.total_positions_filled / float(self.total_positions)
		except ZeroDivisionError:
			return 0

	@hybrid_method
	def total_positions_of_cadre(self, cadreID):
		return sum([len(f.positions_by_cadre(cadreID)) for f in self.facilities])

	@hybrid_property
	def facilities_open(self):
		return len([f for f in self.facilities if f.status and not f.damaged])

	@hybrid_property
	def facilities_closed(self):
		return len([f for f in self.facilities if not f.status and not f.damaged])

	@hybrid_property
	def facilities_damaged_open(self):
		return len([f for f in self.facilities if f.status and f.damaged])

	@hybrid_property
	def facilities_damaged_closed(self):
		return len([f for f in self.facilities if not f.status and f.damaged])

	@hybrid_method
	def total_structure_type(self, structure_type):
		return [f for f in self.facilities if f.structures]

	def __unicode__(self):
		return self.name

	def __repr__(self):
		return '<ProvinceRef(name={0})>'.format(self.name)


class IslandInfo(Model, SurrogatePK):
	__tablename__ = 'islandInfo'
	name = Column(db.Unicode, nullable=False)

	provinceID = reference_col('provinceInfo')
	province = relationship('ProvinceInfo', back_populates='islands')

	def __unicode__(self):
		return self.name

	def __repr__(self):
		return '<IslandInfo(name={0}, provinceID={1})>'.format(self.name, self.provinceID)
		

class AreaCouncilInfo(Model, SurrogatePK):
	__tablename__ = 'areaCouncilInfo'

	name = db.Column(db.Unicode, nullable=False)
	population = db.Column(db.Integer, nullable=True)

	provinceID = reference_col('provinceInfo')
	province = relationship('ProvinceInfo', backref='areaCouncils')

	def __unicode__(self):
		return self.name

	def __repr__(self):
		return '<AreaCouncilInfo(name={0}, population={1}, provinceID={2})>'.format(self.name, self.population, self.provinceID)


class VillageInfo(Model, SurrogatePK):
	__tablename__ = 'villageInfo'

	name = db.Column(db.Unicode, nullable=False)
	population = db.Column(db.Integer)
	latitude = db.Column(db.Float(53))
	longitude = db.Column(db.Float(53))
	
	islandID = reference_col('islandInfo')
	island = relationship('IslandInfo', backref='villages')

	areaCouncilID = reference_col('areaCouncilInfo')
	areaCouncil = relationship('AreaCouncilInfo', backref='villages')


class HealthZoneInfo(Model, SurrogatePK):
	__tablename__ = 'healthZoneInfo'

	# TODO add geo data
	name = db.Column(db.Unicode, nullable=False)
	number = db.Column(db.Unicode)

	provinceID = reference_col('provinceInfo')
	province = relationship('ProvinceInfo', back_populates='healthZones')

	areaCouncilID = reference_col('areaCouncilInfo', nullable=True)
	areaCouncil = relationship('AreaCouncilInfo', backref='healthZones')

	def __unicode__(self):
		return self.name

	def __repr__(self):
		return '<HealthZoneInfo(name={0}, number={1})>'.format(self.name, self.number)


# ================= #
#  F A C I L I T Y  #
# ================= #

class DesignationRef(Model, SurrogatePK):
	__tablename__ = 'designationRef'
	name = db.Column(db.Unicode(150), nullable=False)
	criticality = db.Column(db.Integer, nullable=False)

	asset_requirements = relationship('AssetRequirementInfo', back_populates='designation')

	@hybrid_method
	def type_list(self, type, designation='all'):
		"""Returns list of structures belonging to a type with optional and facility designation"""
		type_list = []
		for f in self.facilities:
			if designation == 'all':
				type_list += [s for s in f.structures if s.type == type]
			else:
				type_list += [s for s in f.structures if (s.type == type and f.designation == designation)]
		return type_list

	@hybrid_method
	def total_designations(self, provinceID):
		return len([f for f in self.facilities if f.provinceID == provinceID])

	@hybrid_method
	def province_list(self, type, designation, province):
		province_list = []	
		type_list = self.type_list(type, designation)
		for s in type_list:
			if s.facility.province == province:
				province_list.append(s)
		return province_list

	@hybrid_method
	def required_asset_categories(self, functionalAreaID=None):
		if functionalAreaID:
			return [a.category for a in self.asset_requirements if a.functionalAreaID==functionalAreaID]
		else:
			return [a.category for a in self.asset_requirements]

	def __unicode__(self):
		return '{0} (criticality: {1})'.format(self.name, self.criticality)

	def __repr__(self):
		return '<DesignationRef(name={0}, criticality={1})>'.format(self.name, self.criticality)

class OwnershipRef(Model, SurrogatePK):
	__tablename__ = 'ownershipRef'
	name = db.Column(db.Unicode(150), nullable=False)

	def __unicode__(self):
		return self.name

	def __repr__(self):
		return '<OwnershipRef(name={0})>'.format(self.name)

class SettingRef(Model, SurrogatePK):
	__tablename__ = 'settingRef'
	name = db.Column(db.Unicode(150), nullable=False)

	def __unicode__(self):
		return self.name

	def __repr__(self):
		return '<SettingRef(name={0})>'.format(self.name)

class FacilityInfo(Model):
	__tablename__ = 'facilityInfo'

	facilityID = db.Column(db.Unicode(50), primary_key=True)
	facilityName = db.Column(db.Unicode(150))
	altFacilityName = db.Column(db.Unicode(150))
	dataSource = db.Column(db.Unicode(250))
	longD = db.Column(db.Integer)
	longM = db.Column(db.Integer)
	longS = db.Column(db.Float)
	longitude = db.Column(db.Float(53))
	latD = db.Column(db.Integer)
	latM = db.Column(db.Integer)
	latS = db.Column(db.Float)
	latitude = db.Column(db.Float(53))
	landOwner = db.Column(db.Unicode(150))
	landOwnerContact = db.Column(db.Unicode(50))
	landTitle = db.Column(db.Unicode(50))
	manager = db.Column(db.Unicode(150))
	managerContact = db.Column(db.Unicode(50))

	status = Column(db.Boolean, nullable=False, default=True)
	damaged = Column(db.Boolean, nullable=True, default=False)

	designationID = reference_col('designationRef')
	designation = relationship(u'DesignationRef', backref=u'facilities')
	
	ownershipID = reference_col('ownershipRef')
	ownership = relationship(u'OwnershipRef', backref=u'facilities')
	
	provinceID = reference_col('provinceInfo')
	province = relationship(u'ProvinceInfo', backref=u'facilities')
	
	healthZoneID = reference_col('healthZoneInfo')
	healthZone = relationship('HealthZoneInfo', backref=u'facilities')

	settingID = reference_col('settingRef')
	setting = relationship(u'SettingRef', backref=u'facilities')
	
	islandID = reference_col('islandInfo')
	island = relationship(u'IslandInfo', backref=u'facilities')
 
	areaCouncilID = reference_col('areaCouncilInfo', nullable=True)  # TODO remove nullable
	areaCouncil = relationship(u'AreaCouncilInfo', backref=u'facilities')

	structures = relationship(u'StructureInfo', cascade="save-update, merge, delete", order_by='StructureInfo.structure_type', back_populates='facility')  # structures are deleted with the facility
	notes = relationship(u'NoteInfo', cascade="save-update, merge, delete", order_by='desc(NoteInfo.date)', back_populates='facility')  # notes are deleted with the facility
	positions = relationship(u'PositionInfo', cascade="save-update, merge", back_populates='facility')  # Will not remove positions if facility is deleted
	required_functional_areas = relationship(u'FunctionalAreaRequirementsRef', cascade="save-update, merge, delete", back_populates='facility')  # functional area requirements are deleted with facility


	@hybrid_property
	def identifier(self):
		return '{0} - {1}'.format(self.facilityID, self.facilityName)

	@hybrid_property
	def latitude_format(self):
		if self.latD and self.latM and self.latS:
			return 'S {0} {1} {2}'.format(self.latD, self.latM, self.latS)
		else:
			return ''

	@hybrid_property
	def longitude_format(self):
		if self.longD and self.longM and self.longS:
			return 'E {0} {1} {2}'.format(self.longD, self.longM, self.longS)
		else:
			return ''
			
	# Count of structures without an inspection or the inspection is older than 180 days
	@hybrid_method
	def overdue(self, structure_type='all'):
		if structure_type == 'all':
			return len([s for s in self.structures if (s.inspection and s.inspection.age > 180) or not s.inspection])
		else:
			return len([s for s in self.structures if (s.inspection and s.inspection.age > 180 and s.structure_type==structure_type) or (not s.inspection and s.structure_type==structure_type)])

	@hybrid_method
	def overdue_percent(self, structure_type='all'):
		overdue = self.overdue(structure_type)
		if structure_type == 'all':
			total = len(self.structures)
		else:
			total = len([s for s in self.structures if s.structure_type==structure_type])
		try:
			return overdue / float(total)
		except ZeroDivisionError:
			return 0

	# Average condition of structures that have an inspection
	@hybrid_method
	def condition(self, structure_type='all', make_float=False):
		if structure_type == 'all':
			values = [s.inspection.condition for s in self.structures if s.inspection]
		else:
			values = [s.inspection.condition for s in self.structures if s.inspection and s.structure_type==structure_type]
		try:
			if make_float:
				return sum(values) / float(len(values))
			else:
				return sum(values) / len(values)
		except ZeroDivisionError:
			return 0

	@hybrid_method
	def has_structure_type(self, structure_type):
		return True if len([s for s in self.structures if s.structure_type == structure_type]) > 0 else False

	# list of structures that are operational
	@hybrid_method
	def operational_list(self, structure_type='all'):
		if structure_type == 'all':
			return [s for s in self.structures if s.status]
		else:
			return [s for s in self.structures if s.status and s.structure_type==structure_type]

	def nonoperational_list(self, structure_type='all'):
		if structure_type == 'all':
			return [s for s in self.structures if not s.status]
		else:
			return [s for s in self.structures if not s.status and s.structure_type==structure_type]

	@hybrid_method
	def operational_percent(self, structure_type='all'):
		operational = self.operational_list(structure_type)
		if structure_type == 'all':
			total = len(self.structures)
		else:
			total = len([s for s in self.structures if s.structure_type==structure_type])
		try:
			return len(operational) / float(total)
		except ZeroDivisionError:
			return 0

	@hybrid_property
	def positions_unfilled(self):
		return [p for p in self.positions if p.vacant]

	@hybrid_property
	def positions_filled(self):
		return [p for p in self.positions if not p.vacant]

	@hybrid_property
	def positions_filled_percent(self):
		try:
			return len(self.positions_filled) / float(len(self.positions))
		except ZeroDivisionError:
			return 0

	@hybrid_method
	def positions_by_cadre(self, cadreID):
		return [p for p in self.positions if p.title.cadreID == cadreID]

	@hybrid_property
	def functionalAreas(self):
		return [fa for s in self.structures if s.functional_areas for fa in s.functional_areas]

	@hybrid_property
	def requiredFunctionalAreas(self):
		return [fa.functionalAreaID for fa in self.required_functional_areas]

	@hybrid_property
	def assets(self):
		"""Advanced list comphrension equivelant to:

		assets = []
		for s in self.structures:					# for each structure in this facility
			if s.functional_areas:					# if the structure has functional areas
				for fa in s.functional_areas:		# for each funtional area in this structure
					if fa.assets:					# if the functional area has assets
						assets = assets + fa.assets	# add assets to list of all assets in this facility
		return assets

		Learn more here:
		https://spapas.github.io/2016/04/27/python-nested-list-comprehensions/  advanced list comphrension
		"""
		return [assets for s in self.structures if s.functional_areas for fa in s.functional_areas if fa.assets for assets in fa.assets]

	@hybrid_property
	def default_pic(self):
		try:
			return [pic for pic in self.pics if pic.default==True][0]  # only return one pic
		except IndexError:  # except no pictures yet
			return None


	def __init__(self, **kwargs):
		"""Create instance."""
		db.Model.__init__(self, **kwargs)


	def _empty_popup_html(self):

		template = """<div style="min-width: 200px;" id="popup-id-{{facility.facilityID}}">
						<h5><a href="{{link}}">{{facility.identifier}}</a></h5>
						  <div class="thumbnail">
                            <img src="{{ url_for('static', filename='img/loading.gif') }}" class="img-rounded img-responsive">
                          </div>
					   </div>
				   """
		return render_template_string(template, linke=url_for('facility.facility', facilityID=self.facilityID), facility=self)

	def as_dict(self, categoryID=None):
		"""Produces the dictionary that is turned into JSON.
		Used to make markers for maps.
		"""
		data = {c.name: getattr(self, c.name) for c in self.__table__.columns}
		data['identifier'] = self.identifier
		data['facilityID'] = self.facilityID
		data['facilityLink'] = url_for('facility.facility', facilityID=self.facilityID)
		data['designation'] = self.designation.name
		data['condition'] = self.condition()
		data['categoryID'] = categoryID  # if a categoryID is supplied in jquery we will call for data about it and rewrite the popup on the client side
		data['empty_popup'] = self._empty_popup_html()
		data['second_line'] = data['designation']
		return data

	def __unicode__(self):
		return 'ID: {0} - {1} in {2}'.format(self.facilityID, self.facilityName, self.province.name)

	def __repr__(self):
		return '<FacilityInfo(facilityID={0}, facilityName={1}, provinceID={2})>'.format(self.facilityID, self.facilityName, self.provinceID)


class NoteInfo(Model, SurrogatePK):
	__tablename__ = "noteInfo"

	note = Column(db.Unicode, nullable=False)
	date = Column(db.DateTime, nullable=False)

	facilityID = reference_col('facilityInfo', pk_name='facilityID')
	facility = relationship(u'FacilityInfo', back_populates='notes')

	userID = reference_col('users', nullable=True)
	user = relationship('User', backref='notes')

"""

class AssetMaintenanceInformation(db.Model):
	__tablename__ = 'assetMaintenanceInformation'

	assetMaintenanceID = db.Column(db.Integer, primary_key=True)
	assetID = db.Column(db.ForeignKey(u'assetInformation.assetID'), nullable=False)
	date = db.Column(db.Date, nullable=False)
	description = db.Column(db.Unicode(500), nullable=False)
	performedBy = db.Column(db.Unicode(150))
	cost = db.Column(db.Integer, nullable=False)

	assetInformation = db.relationship(u'AssetInformation', primaryjoin='AssetMaintenanceInformation.assetID == AssetInformation.assetID', backref=u'asset_maintenance_informations')


#class AssetInformation(db.Model):
#    __tablename__ = 'assetInformation'

#    assetID = db.Column(db.Integer, primary_key=True, autoincrement=True)
#    vNum = db.Column(db.Unicode(50))
#    opStatus = db.Column(db.Integer)
#    opStatusNote = db.Column(db.Unicode)
#    lastSeenDate = db.Column(db.Date, nullable=False)
#    itemID = db.Column(db.ForeignKey(u'itemRef.itemID'), nullable=False)
#    buildingFunctionID = db.Column(db.ForeignKey(u'buildingFunctionRef.buildingFunctionID'))

#    itemRef = db.relationship(u'ItemRef', primaryjoin='AssetInformation.itemID == ItemRef.itemID', backref=u'asset_informations')
#    buildingFunctionRef = db.relationship(u'BuildingFunctionRef', primaryjoin='AssetInformation.buildingFunctionID == BuildingFunctionRef.buildingFunctionID', backref=u'asset_informations')


class ItemRef(db.Model):
	__tablename__ = 'itemRef'

	itemID = db.Column(db.Integer, primary_key=True, autoincrement=True)
	catalogID = db.Column(db.ForeignKey(u'catalogInformation.catalogID'), nullable=False)
	baseItemID = db.Column(db.ForeignKey(u'baseItemInformation.baseItemID'), nullable=False)

	catalogInformation = db.relationship(u'CatalogInformation', primaryjoin='ItemRef.catalogID == CatalogInformation.catalogID', backref=u'item_refs')
	baseItemInformation = db.relationship(u'BaseItemInformation', primaryjoin='ItemRef.baseItemID == BaseItemInformation.baseItemID', backref=u'item_refs')


class CatalogInformation(db.Model):
	__tablename__ = 'catalogInformation'

	catalogID = db.Column(db.Integer, primary_key=True, autoincrement=True)
	baseItemID = db.Column(db.ForeignKey(u'baseItemInformation.baseItemID'), nullable=False)
	manufacturer = db.Column(db.Unicode(150))
	model = db.Column(db.Unicode(150))
	supplier = db.Column(db.Unicode(150))
	cost = db.Column(db.Integer)
	dimensions = db.Column(db.Unicode(50))

	baseItemInformaiton = db.relationship(u'BaseItemInformation', primaryjoin='CatalogInformation.baseItemID == BaseItemInformation.baseItemID', backref=u'catalog_informations')


class BaseItemInformation(db.Model):
	__tablename__ = 'baseItemInformation'

	baseItemID = db.Column(db.Integer, primary_key=True, autoincrement=True)
	description = db.Column(db.Unicode(150), nullable=False)


class RequirementRef(db.Model):
	__tablename__ = 'requirementRef'

	requirementID = db.Column(db.Integer, primary_key=True, autoincrement=True)
	functionalSpaceID = db.Column(db.ForeignKey(u'functionalSpaceInformation.functionalSpaceID'), nullable=False)
	baseItemID = db.Column(db.ForeignKey(u'baseItemInformation.baseItemID'), nullable=False)
	quantity = db.Column(db.Integer, nullable=False)

	functionalSpaceInformation = db.relationship(u'FunctionalSpaceInformation', primaryjoin='RequirementRef.functionalSpaceID == FunctionalSpaceInformation.functionalSpaceID', backref=u'requirement_refs')
	baseItemInformation = db.relationship(u'BaseItemInformation', primaryjoin='RequirementRef.baseItemID == BaseItemInformation.baseItemID', backref=u'requirement_refs')


class BuildingFunctionRef(db.Model):
	__tablename__ = 'buildingFunctionRef'

	buildingFunctionID = db.Column(db.Integer, primary_key=True, autoincrement=True)
	#buildingID = db.Column(db.ForeignKey(u'buildingInformation.buildingID'), nullable=False)
	functionalSpaceID = db.Column(db.ForeignKey(u'functionalSpaceInformation.functionalSpaceID'), nullable=False)

	#buildingInformation = db.relationship(u'BuildingInformation', primaryjoin='BuildingFunctionRef.buildingID == BuildingInformation.buildingID', backref=u'building_function_refs')
	functionalSpaceInformation = db.relationship(u'FunctionalSpaceInformation', primaryjoin='BuildingFunctionRef.functionalSpaceID == FunctionalSpaceInformation.functionalSpaceID', backref=u'building_function_refs')


class FunctionalSpaceInformation(db.Model):
	__tablename__ = 'functionalSpaceInformation'

	functionalSpaceID = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.Unicode(150), nullable=False)
	description = db.Column(db.Unicode(500))

"""