# -*- coding: utf-8 -*-
"""Facility views"""
import os
import datetime
import pygal

from flask import Blueprint, render_template, request, flash, url_for, redirect, send_from_directory, jsonify, current_app
from flask import current_app as app
from flask_login import login_required, current_user
from flask_paginate import Pagination, get_page_parameter, get_per_page_parameter

from moha.extensions import csrf_protect, cache

import forms
import models as fm
import business as bus
from moha import utils
import moha.cache_utils as cacheDel

from sqlalchemy import or_

from moha.asset import models as am
from moha.personnel import models as pm
from moha.structure import models as sm

import structlog
logger = structlog.getLogger(__name__)

blueprint = Blueprint('facility', __name__, url_prefix='/facility', static_folder='../static')

# ============= #
#  C H A R T S  #
# ============= #
"""This section contains charts and graphs related to facilities.

They are called from the _charts() function via its route.
This will be a jQuery call that requests the charts to be created and sent to the browser.
This is because these graphs are relatively resource intensive to create and would
slow down the browser too much when loading the page.

Charts themselves are made with PyGal which you can learn about here: http://pygal.org/en/stable/
They are quite simple to create and have good aesthetic quality in my opinion. They come included
with a minimal javascipt component that makes them somewhat interactive. The charts are svg 
which means they will scale to any size and can be imported into other documents.

Each chart appears to be made in 10 lines of code or less but that is not true. 
The charts *Heavily* use SQLAlchemy hybrid_method and hybrid_property functions to help build the 
lists required to populate the chart. In the models you can find these hybrid_property and 
hybrid_method functions that are doing the work to build the data for the charts. These
functions can follow relationships between models and therefore can travel many models before 
returning with the data for the chart. But following the functions will show you how a 
chart gathers its data and you will have no problem debugging it.

Lastly, the actual charts are shown on screen with jQuery (as mentioned above), so the 
code that calls for these functions will be found in the templates. The return 
statements are dictionaries so that they can be encoded into JSON for jQuery.
"""

#  S E A R C H 
#------------
@cache.memoize(timeout=300)
def _chart_designation_by_province(disable_xml_declaration=False):
	provinces = fm.ProvinceInfo.query.all()
	designations = fm.DesignationRef.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Designations by Province'
	chart.x_labels = [p.name for p in provinces]
	for d in designations:
		chart.add(d.name, [d.total_designations(p.id) for p in provinces])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@cache.memoize(timeout=300)
def _chart_percent_designation_by_province(disable_xml_declaration=False):
	provinces = fm.ProvinceInfo.query.all()
	designations = fm.DesignationRef.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Designations by Province (%)'
	chart.value_formatter = lambda x: '{}%'.format(x)
	chart.x_labels = [p.name for p in provinces]
	for d in designations:
		chart.add(d.name, [(d.total_designations(p.id)/float(p.total_facilities))*100 if p.total_facilities > 0 else None for p in provinces])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@cache.memoize(timeout=300)
def _chart_facility_condition(disable_xml_declaration=False):
	facilities = fm.FacilityInfo.query.all()
	chart = pygal.Pie(style=pygal.style.BlueStyle)
	chart.title = 'Facility Status'
	chart.show_legend = False
	chart.add('Status', [{'value': len([f for f in facilities if f.status and not f.damaged]), 'label': 'Open', 'color': 'green'},
						{'value': len([f for f in facilities if not f.status and not f.damaged]), 'label': 'Closed', 'color': 'black'},
						{'value': len([f for f in facilities if f.status and f.damaged]), 'label': 'Open/Damaged', 'color': 'orange'},
						{'value': len([f for f in facilities if not f.status and f.damaged]), 'label': 'Closed/Damaged', 'color': 'red'}])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@cache.memoize(timeout=300)
def _chart_percent_facility_condition_by_province(disable_xml_declaration=False):
	provinces = fm.ProvinceInfo.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Facility Status by Province (%)'
	chart.value_formatter = lambda x: '{}%'.format(x)
	chart.show_legend = False
	chart.x_labels = [p.name for p in provinces]
	chart.add('', [{'value': p.facilities_open/float(len(p.facilities))*100, 'label': 'Open', 'color': 'green'} if p.total_facilities > 0 else None for p in provinces])
	chart.add('', [{'value': p.facilities_closed/float(len(p.facilities))*100, 'label': 'Closed', 'color': 'black'} if p.total_facilities > 0 else None for p in provinces])
	chart.add('', [{'value': p.facilities_damaged_open/float(len(p.facilities))*100, 'label': 'Damaged/Open', 'color': 'orange'} if p.total_facilities > 0 else None for p in provinces])
	chart.add('', [{'value': p.facilities_damaged_closed/float(len(p.facilities))*100, 'label': 'Damaged/Closed', 'color': 'red'} if p.total_facilities > 0 else None for p in provinces])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@cache.memoize(timeout=300)
def _chart_facility_rank():
	"""This is a test and not a complete ranking or report"""
	provinces = fm.ProvinceInfo.query.all()
	facilities = fm.FacilityInfo.query.all()
	chart = pygal.XY(style=pygal.style.BlueStyle)
	chart.stroke = False
	chart.title = 'Facility Condition'
	for f in facilities:
		chart.add(f.facilityName, [])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@blueprint.route('/data/charts')
def _charts():
	logger.debug('Enter _charts')
	chart1 = _chart_designation_by_province()
	chart2 = _chart_percent_designation_by_province()
	chart3 = _chart_facility_condition()
	chart4 = _chart_percent_facility_condition_by_province()
	chart5 = _chart_facility_rank()
	logger.debug('Exit _charts')
	return jsonify({'charts': [chart1, chart2, chart3, chart4, chart5]})


# ===================== #
#  F A C I L I T I E S  #
# ===================== #

#  S E A R C H 
#------------
@blueprint.route('/search/')
@csrf_protect.exempt
def search():
	"""List Facilities."""
	logger.debug('Enter facility search')
	form = forms.FacilitySearchForm()
	# Create the base query for the facilities
	query = fm.FacilityInfo.query.order_by(fm.FacilityInfo.facilityName)
	# Create base queries for different columns of search page
	province_query = fm.ProvinceInfo.query.order_by(fm.ProvinceInfo.id)
	island_query = fm.IslandInfo.query.order_by(fm.IslandInfo.name)
	healthZone_query = fm.HealthZoneInfo.query.order_by(fm.HealthZoneInfo.name)
	designation_query = fm.DesignationRef.query.order_by(fm.DesignationRef.name)
	# Collect the argumetns from the Form's GET submit and set defaults
	try:
		# Set defaults on form so that between page refreshes the selected variables remain
		facilityName = request.args.get('facilityName')
		provinceID = int(request.args.get('province'))
		form.province.default = provinceID
		islandID = int(request.args.get('island'))
		form.island.default = islandID
		healthZoneID = request.args.get('healthZone')
		form.healthZone.default = healthZoneID
		designationID = int(request.args.get('designation'))
		form.designation.default = designationID
		if facilityName is not None:
			query = query.filter(or_(fm.FacilityInfo.facilityName.contains(facilityName), fm.FacilityInfo.altFacilityName.contains(facilityName)))
			form.facilityName.default = facilityName
		if provinceID >= 0:
			query = query.filter(fm.FacilityInfo.provinceID==provinceID)
			island_query = island_query.filter(fm.IslandInfo.provinceID==provinceID)
		if islandID >= 0:
			query = query.filter(fm.FacilityInfo.islandID==islandID)
		if healthZoneID >= 0 and healthZoneID != '-1':
			query = query.filter(fm.FacilityInfo.healthZoneID==healthZoneID)
		if designationID >= 0:
			query = query.filter(fm.FacilityInfo.designationID==designationID)
	except TypeError:
		pass  # user has not yet filtered so above int() fails

	# Finalize the queries and add an 'all' option to the SelectFields in the form
	form.province.choices = [(-1,'all')] + [(p.id, p.name.strip()) for p in province_query.all()]
	form.island.choices = [(-1,'all')] + [(i.id, i.name.strip()) for i in island_query.all()]
	form.healthZone.choices = [('-1','all')] + [(h.id, h.name.strip()) for h in healthZone_query.all()]
	form.designation.choices = [(-1,'all')] + [(d.id, d.name.strip()) for d in designation_query.all()]
	form.process()  # We have to call this to 're-build' the form with our new choices and defaults selected
	# Set up pagination
	page = request.args.get(get_page_parameter(), type=int, default=1)
	per_page = request.args.get(get_per_page_parameter(), type=int, default=10)	
	pagination = Pagination(page=page, per_page=per_page, total=query.count(), search=False, record_name='facilities', bs_version=3)
	facilities = query.limit(per_page).offset((pagination.page - 1) * per_page)
	return render_template('facilities/facilities.html', form=form, facilities=facilities, pagination=pagination)#, charts=_facility_charts())

#  V I E W
#--------
@blueprint.route('/<facilityID>')
@login_required
def facility(facilityID):
	""" Main page blo wan Facility. 
		Includes Buildings, inspections, Files, Pictures, and Data about facility."""
	logger.debug("Enter view facilityID: {0}".format(facilityID), facilityID=facilityID, action='view')
	noteForm = forms.FacilityNoteForm()
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	return render_template('facilities/facility.html', facility=facility, noteForm=noteForm)

#  A D D
#------
@blueprint.route('/create', methods=['GET', 'POST'])
@login_required
def add_facility():
	"""Create New Facility."""
	logger.debug('Enter add facility')
	form = _populate_facility_form(forms.FacilityForm(request.form))
	if form.validate_on_submit():
		logger.debug('Validated form')
		if not fm.FacilityInfo.query.filter_by(facilityID=form.facilityID.data).first():
			logger.debug('facilityID "{0}" is unique will be added to database'.format(form.facilityID.data))
			facility = fm.FacilityInfo.create(facilityID=form.facilityID.data,
							facilityName=form.facilityName.data,
							altFacilityName=form.altFacilityName.data,
							status=form.status.data,
							designationID=form.designationID.data,
							provinceID=form.provinceID.data,
							islandID=form.islandID.data,
							healthZoneID=form.healthZoneID.data,
							areaCouncilID=form.areaCouncilID.data,
							latD=form.latD.data,
							latM=form.latM.data,
							latS=form.latS.data,
							longD=form.longD.data,
							longM=form.longM.data,
							longS=form.longS.data,
							longitude= (float(form.longD.data) + (form.longM.data / 60.) + form.longS.data / 3600.),
							latitude= (float(form.latD.data) + (form.latM.data / 60.) + form.latS.data / 3600.) * -1,
							ownershipID=form.ownershipID.data,
							settingID=form.settingID.data,
							landOwner=form.landOwner.data,
							landOwnerContact=form.landOwnerContact.data,
							manager=form.manager.data,
							managerContact=form.managerContact.data,
							landTitle=form.landTitle.data)
			logger.info('Created facilityID: {0}'.format(form.facilityID.data), facility=facility, action='create')
			cacheDel.force_update_facility_cache()
			FunctionalAreaRequirements(form.facilityID.data, form.designationID.data)
			logger.info('FunctionalAreaRequirements created for facilityID: {0}'.format(form.facilityID.data))
			flash('Facility Created', 'success')
			redirect_url = request.args.get('next') or url_for('.facility', facilityID=form.facilityID.data)
			return redirect(redirect_url)
		else:
			flash('Please choose an unique facility ID', 'info')
	else:
		utils.flash_errors(form)
	data = _dropdown_from_province()
	return render_template('facilities/form_facility.html', form=form, dropdowns=data, form_title='Create New Facility')

#  U P D A T E
#------------
@blueprint.route('/<facilityID>/update', methods=['GET', 'POST'])
@login_required
def update_facility(facilityID):
	"""Update Facility Information."""
	logger.debug('Enter update facilityID: {0}'.format(facilityID))
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	originalDesignation = facility.designationID
	form = _populate_facility_form(forms.FacilityForm(obj=facility))
	if form.validate_on_submit():
		logger.debug('Validated form')
		logger.info('Enter update facilityID: {0}'.format(facilityID))
		fm.FacilityInfo.update(facility,
							facilityID=form.facilityID.data,
							facilityName=form.facilityName.data,
							altFacilityName=form.altFacilityName.data,
							status=form.status.data,
							designationID=form.designationID.data,
							provinceID=form.provinceID.data,
							islandID=form.islandID.data,
							healthZoneID=form.healthZoneID.data,
							areaCouncilID=form.areaCouncilID.data,
							latD=form.latD.data,
							latM=form.latM.data,
							latS=form.latS.data,
							longD=form.longD.data,
							longM=form.longM.data,
							longS=form.longS.data,
							longitude= (float(form.longD.data) + (form.longM.data / 60.) + form.longS.data / 3600.),
							latitude= (float(form.latD.data) + (form.latM.data / 60.) + form.latS.data / 3600.) * -1,
							ownershipID=form.ownershipID.data,
							settingID=form.settingID.data,
							landOwner=form.landOwner.data,
							landOwnerContact=form.landOwnerContact.data,
							manager=form.manager.data,
							managerContact=form.managerContact.data,
							landTitle=form.landTitle.data)
		logger.info('Updated facilityID: {0}'.format(facility.facilityID), facility=facility, action='update')
		cacheDel.force_update_facility_cache()
		if originalDesignation != form.designationID.data:
			logger.info('Facility has new designation facilityID: {0} oldDesignation: {1} newDesignation: {2}'.format(facilityID, originalDesignation, form.designationID.data))
			FunctionalAreaRequirements(form.facilityID.data, form.designationID.data)
			logger.info('FunctionalAreaRequirements updated for facilityID: {0}'.format(facilityID))
		flash('Facility Updated', 'success')
		redirect_url = request.args.get('next') or url_for('.facility', facilityID=facility.facilityID)
		return redirect(redirect_url)
	else:
		utils.flash_errors(form)
	data = _dropdown_from_province()
	return render_template('facilities/form_facility.html', form=form, dropdowns=data, form_title="Update Facility Information")

#  D E L E T E 
#------------
@blueprint.route('/<facilityID>/delete', methods=['GET', 'POST'])
@login_required
def delete_facility(facilityID):
	""" Delet this."""
	logger.debug("Enter delete facilityID: {0}".format(facilityID))
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	if request.method == 'POST':
		## TODO delete buildings and all of the attached data together
		fm.FacilityInfo.delete(facility)
		logger.info("Deleted facilityID: {0}".format(facilityID), facility=facility, action='delete')
		cacheDel.force_update_facility_cache()
		flash('Facility Deleted', 'warning')
		redirect_url = request.args.get('next') or url_for('.facilities')
		return redirect(redirect_url)
	return render_template('facilities/delete.html', facility=facility)


# =========== #
#  F I L E S  #
# =========== #

#  D O W N L O A D
#----------------
@blueprint.route('<facilityID>/download_files')
@login_required
def download_facility_files(facilityID):
	"""Returns all images or files of a facility in a ZIP file."""
	file_type = request.args.get('file_type')
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).one()
	if file_type == 'file':
		files = fm.FacilityFileRef.query.filter_by(facilityID=facilityID).all()
		attachment = utils.download_multiple(facility, files, file_type=file_type, file_name="{0} Files.zip".format(facility.identifier))
	elif file_type == 'picture':
		files = fm.FacilityPicRef.query.filter_by(facilityID=facilityID).all()
		attachment = utils.download_multiple(facility, files, file_type=file_type, file_name="{0} Pictures.zip".format(facility.identifier))	
	else:
		logger.warn('File type not supported: {0}'.format(file_type))
		flash('{0} - file type not supported for {1}s.'.format(file.filename, file_type), 'warning')
	return attachment

#  D O W N L O A D
#----------------
@blueprint.route('/<facilityID>/download/<filename>')
@login_required
@cache.memoize(timeout=604800)  # 1 week storage in cache
def download_file(facilityID, filename):
	"""Returns an image or a file."""
	file_type = request.args.get('file_type')
	original_name = request.args.get('original_name')
	if file_type == 'picture':
		#logger.info('Download picture of facilityID: {0} filename: {1}'.format(facilityID, filename))
		return send_from_directory(os.path.join(app.config['PICTURE_FOLDER'], facilityID), filename, as_attachment=True, attachment_filename=original_name)
	elif file_type == 'file':
		#logger.info('Download file of facilityID: {0} filename: "{1}"'.format(facilityID, filename))
		return send_from_directory(os.path.join(app.config['FILE_FOLDER'], facilityID), filename, as_attachment=True, attachment_filename=original_name)
	else:
		logger.warn('Unknown file type requested for download: facilityID: {0} filename: {1} file_type: {2}'.format(facilityID, filename, file_type))
		flash('The file type "{0}" is malformed or unknown, the action you requested can not be performed'.format(file_type))

#  V I E W 
#--------
@blueprint.route('/<facilityID>/view/<filename>')
@login_required
@cache.memoize(timeout=15552000)  # 6 months storage in cache
def view_file(facilityID, filename):
	"""Returns the thumbnail of an image or a file."""
	file_type = request.args.get('file_type')
	original_name = request.args.get('original_name')
	# TODO attache original name to request so that it downloads with proper name
	if file_type == 'picture':
		#logger.info('View picture of facilityID: {0} filename: {1}'.format(facilityID, filename))
		return send_from_directory(os.path.join(app.config['PICTURE_FOLDER'], facilityID, 'thumbs'), filename)
	elif file_type == 'file':
		#logger.info('View file of facilityID: {0} filename: {1}'.format(facilityID, filename))
		return send_from_directory(os.path.join(app.config['FILE_FOLDER'], facilityID), filename)
	else:
		logger.warn('Unknown file type requested for viewing: facilityID: {0} filename: {1} file_type: {2}'.format(facilityID, filename, file_type))
		flash('The file type "{0}" is malformed or unknown, the action you requested can not be performed'.format(file_type))

#  R E V I E W
#------------
@blueprint.route('/<facilityID>/files')
@login_required
def facility_files(facilityID):
	"""View and edit photos"""
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).one()
	files = facility.files
	pics = facility.pics
	return render_template('facilities/facility_files.html', facility=facility, files=files, pics=pics)

#  D E F A U L T
#--------------
@blueprint.route('/<facilityID>/files/<int:fileID>/make_default')
@login_required
def make_default_file(facilityID, fileID):
	logger.debug('Enter make default fileID: {0}'.format(fileID))
	file_type = request.args.get('file_type')
	if file_type == 'picture':
		for f in fm.FacilityPicRef.query.filter_by(facilityID=facilityID).filter_by(default=True).all():
			fm.FacilityPicRef.update(f, default=False)
		file = fm.FacilityPicRef.query.filter_by(fileID=fileID).filter_by(facilityID=facilityID).one()
		fm.FacilityPicRef.update(file, default=True)
		flash('Default Picture Updated', 'success')
	else:
		logger.warn('File type not supported: {0}'.format(file_type))
		flash('This file type is not supported for setting a default', 'warning')
	return redirect(url_for('facility.facility', facilityID=facilityID))

#  A D D
#------
@blueprint.route('/<facilityID>/files/add', methods=['GET', 'POST'])
@login_required
def add_facility_file(facilityID):
	"""Add picture to facility. """
	logger.debug('Enter add file facilityID: {0}'.format(facilityID))
	form = forms.FacilityFileForm()
	file_type = request.args.get('file_type')
	default_selector = True if file_type == 'picture' else False
	title = 'Upload {0}'.format(file_type.capitalize())
	if form.validate_on_submit():
		logger.debug('Form validated')
		default = True if request.form.get('default') == 'default' else False
		files_uploaded = request.files.getlist('file')
		logger.debug('{0} files received'.format(len(files_uploaded)))
		if files_uploaded:
			pic_count = 0
			file_count = 0
			for file in files_uploaded:
				if file_type == 'picture' and utils.allowed_picture(file.filename):
					newFile = utils.add_file(file, form, app.config['PICTURE_FOLDER'], facilityID, fm.FileInfo)
					if default:
						for p in fm.FacilityPicRef.query.filter_by(default=True).filter_by(facilityID=facilityID).all():
							fm.FacilityPicRef.update(p, default=False)
							logger.debug('Removed default fileID: {0}'.format(p.id))
					fm.FacilityPicRef.create(fileID=newFile.id, id=facilityID, default=default)
					logger.info('Created association fileID: {0} facilityID: {1}'.format(newFile.id, facilityID), file=newFile, action='create')
					pic_count += 1
				elif file_type == 'file' and utils.allowed_file(file.filename):
					newFile = utils.add_file(file, form, app.config['FILE_FOLDER'], facilityID, fm.FileInfo)
					if default:
						for f in fm.FacilityFileRef.query.filter_by(default=True).filter_by(facilityID=facilityID).all():
							fm.FacilityFileRef.update(f, default=False)
							logger.debug('Removed default fileID: {0}'.format(f.id))
					fm.FacilityFileRef.create(fileID=newFile.id, id=facilityID, default=default)
					logger.info('Created association fileID: {0} facilityID: {1}'.format(newFile.id, facilityID), file=newFile, action='create')
					file_count += 1
				else:
					logger.warn('File type not supported: {0}'.format(file_type))
					flash('{0} - file type not supported for {1}s.'.format(file.filename, file_type), 'warning')
			if pic_count > 0:
				logger.debug('{0} pictures added to facilityID: {1}'.format(pic_count, facilityID))
				flash('{0} Pictures Added'.format(pic_count), 'success')
			if file_count > 0:
				logger.debug('{0} files added to facilityID: {1}'.format(file_count, facilityID))
				flash('{0} Files Added'.format(file_count), 'success')
			redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
			return redirect(redirect_url)
		else:
			flash('No files were uploaded, please try again.', 'warning')
	return render_template('facilities/form_file.html', form=form, title=title, default=default_selector)

#  U P D A T E
#------------
@blueprint.route('/<facilityID>/files/<int:fileID>/update', methods=['GET', 'POST'])
@login_required
def update_facility_file(facilityID, fileID):
	logger.debug('Enter update facility fileID: {0}'.format(fileID))
	file = fm.FileInfo.query.filter_by(id=fileID).one()
	form = forms.FacilityFileForm(dateCreated=file.fileDateCreated, description=file.fileDesc)
	del form.file
	if form.validate_on_submit():
		logger.debug('Form validated')
		fm.FileInfo.update(file, 
							fileDateCreated=form.dateCreated.data,
							fileDesc=form.description.data)
		logger.info('Updated fileID: {0}'.format(fileID))
		return redirect(url_for('facility.facility', facilityID=facilityID))
	else:
		utils.flash_errors(form)
	return render_template('structures/form.html', form=form, title='Edit File')

#  D E L E T E
#------------
@blueprint.route('/<facilityID>/files/<int:fileID>/delete/')
@login_required
def delete_facility_file(facilityID, fileID):
	"""Delete a file from a facility.

	:param::file_type: required :: either 'picture' or 'file'

	:param::inspectionID: optional :: if present will remove the file from the inspection
	"""
	logger.debug('Enter delete fileID: {0}'.format(fileID))
	file_type = request.args.get('file_type')
	if file_type == 'picture':
		file = fm.FacilityPicRef.query.filter_by(fileID=fileID, id=facilityID).one()
		utils.delete_file(file.file.fileName, file_type, facilityID)
		fm.FacilityPicRef.delete(file)
		flash('Picture removed', 'success')
	elif file_type == 'file':
		file = fm.FacilityFileRef.query.filter_by(fileID=fileID, id=facilityID).one()
		fm.FacilityFileRef.delete(file)
		utils.delete_file(file.file.fileName, file_type, facilityID)
		flash('File removed', 'success')
	else:
		logger.warn('File type not supported: {0}'.format(file_type))
		flash('File type not supported -- no file type specified', 'warning')
	redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
	return redirect(redirect_url)


# =================== #
#  B U I L D I N G S  #
# =================== #
# Move to structures
# Make dropdowns and have a like 'search for anything' instead of just buildings
def _building_charts():
	return []

@blueprint.route('/buildings/<int:building_type>')
@blueprint.route('/buildings/')
@cache.memoize(timeout=300)  # TODO make this longer or idefinite if cache can be properly cleared when needed
def buildings(building_type=0):
	"""Explore buildings"""
	provinces = fm.ProvinceInfo.query.all()
	designations = fm.DesignationRef.query.all()
	building_types = sm.BuildingRef.query.all()
	if building_type:
		building_type = sm.BuildingRef.query.filter_by(id=building_type).one()
	return render_template('facilities/buildings.html', designations=designations, building_types=building_types, provinces=provinces, building_type=building_type, charts=_building_charts())


# =========== #
#  N O T E S  #
# =========== #

#  A D D
#------
@blueprint.route('/<facilityID>/note/add', methods=['POST'])
@login_required
def add_note(facilityID):
	"""Add note to facility."""
	form = forms.FacilityNoteForm()	
	if form.validate_on_submit():
		note = fm.NoteInfo.create(note=form.note.data, facilityID=facilityID, userID=current_user.id, date=datetime.datetime.now())
		logger.info('Created noteID: {0}'.format(note.id), note=note, action='create')
		flash('New note created', 'success')
	else:
		utils.flash_errors(form)
	return redirect(url_for('facility.facility', facilityID=facilityID))

#  D E L E T E
#------------
@blueprint.route('/<facilityID>/note/delete')
@login_required
def delete_note(facilityID):
	"""Deletes a note from a facility."""
	noteID = request.args.get('id')
	note = fm.NoteInfo.query.filter_by(id=noteID).first_or_404()
	fm.NoteInfo.delete(note)
	logger.info('Removed noteID: {0}'.format(note.id), note=note, action='delete')
	flash('Note Removed', 'success')
	return redirect(url_for('facility.facility', facilityID=facilityID))


# =============== #
#  N E T W O R K  #
# =============== #
"""Prepares data for Sigma.js to render network map"""

def _image_url(s):
	if s.default_pic:
		return url_for('facility.view_file', facilityID=s.facilityID, file_type='picture', filename=s.default_pic.file.fileName)
	else:
		return False

def _edge_color(s):
	if s.structure_type == 'B':
		return '#ccc'
	elif s.structure_type == 'WSR' or s.structure_type == 'WST':
		return 'blue'
	elif s.structure_type == 'P':
		return 'yellow'
	elif s.structure_type == 'C':
		return 'purple'
	elif s.structure_type == 'CST':
		return 'lightblue'
	else:
		return 'black'

@blueprint.route('/<facilityID>/get_image_urls')
@login_required
def structure_network(facilityID):
	logger.debug('Enter structure_network for {0}'.format(facilityID))
	structures = sm.StructureInfo.query.filter_by(facilityID=facilityID).all()
	data = dict(nodes=[],edges=[],urls=[])
	for s in structures:
		node = {}
		node['identifier'] = s.identifier
		node['id'] = s.id
		node['structure_type'] = s.structure_type
		node['type'] = 'image' if s.default_pic else 'def'  # 'def' is sigma.js default type
		node['image'] = True if s.default_pic else False
		if node['image']:
			data['urls'].append(_image_url(s))
		node['url'] = _image_url(s)
		node['color'] = 'green' if s.status else 'red'
		node['size'] = 16 + len(s.services)*2 + len(s.servicing)*2
		data['nodes'].append(node)

	for s in structures:
		for srvc in s.servicing:
			data['edges'].append(dict(id='e{0}-{1}'.format(s.id, srvc.id), 
									  source=s.id, 
									  target=srvc.id,
									  size=10,
									  type='line' if s.status else 'dotted',
									  color=_edge_color(s)))

	return jsonify({"data": data})


# =========== #
#  U T I L S  #
# =========== #

@blueprint.route('/<facilityID>/status_switch')
@login_required
def status_switch(facilityID):
	"""Switches the facility status Opened/Closed """
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).one()
	if facility.status:
		status = False
	else:
		status = True
	fm.FacilityInfo.update(facility, status=status)
	logger.info('facilityID: {0} status: {1}'.format(facilityID, status))
	cacheDel.force_update_facility_cache()
	return redirect(url_for('facility.facility', facilityID=facilityID))

@blueprint.route('/<facilityID>/damaged_switch')
@login_required
def damaged_switch(facilityID):
	"""Switches the facility damaged flag"""
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).one()
	if facility.damaged:
		damaged = False
	else:
		damaged = True
	fm.FacilityInfo.update(facility, damaged=damaged)
	logger.info('facilityID: {0} damaged: {1}'.format(facilityID, damaged))
	cacheDel.force_update_facility_cache()
	redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
	return redirect(redirect_url)

def _populate_facility_form(form):
	logger.debug('Enter _populate_facility_form')
	form.provinceID.choices = [(p.id, p.name.strip()) for p in fm.ProvinceInfo.query.all()]
	form.settingID.choices = [(s.id, s.name) for s in fm.SettingRef.query.all()]
	form.designationID.choices = [(d.id, d.name) for d in fm.DesignationRef.query.order_by(fm.DesignationRef.name).all()]
	form.ownershipID.choices = [(o.id, o.name) for o in fm.OwnershipRef.query.order_by(fm.OwnershipRef.id).all()]
	form.islandID.choices = [(i.id, i.name.strip()) for i in fm.IslandInfo.query.order_by(fm.IslandInfo.name).all()]
	form.healthZoneID.choices = [(h.id, h.name) for h in fm.HealthZoneInfo.query.order_by(fm.HealthZoneInfo.name).all()]
	form.areaCouncilID.choices = [(a.id, a.name) for a in fm.AreaCouncilInfo.query.order_by(fm.AreaCouncilInfo.name).all()]
	logger.debug('Exit _populate_facility_form')
	return form

def _dropdown_from_province():
	"""Creating dropdowns with this instead of WTForms to follow this pattern using Jquery to change the contents of dropdown selection
	https://stackoverflow.com/questions/26159665/dynamic-2nd-dropdown-list-based-on-the-1st-dropdown
	"""
	data = {}
	healthZones = {}
	islands = {}
	areaCouncils = {}
	for province in fm.ProvinceInfo.query.all():
		healthZones[province] = [(h.id, h.name) for h in province.healthZones]
		islands[province] = [(i.id, i.name) for i in province.islands]
		areaCouncils[province] = [(a.id, a.name) for a in province.areaCouncils]
	data['healthZones'] = healthZones
	data['islands'] = islands
	data['areaCouncils'] = areaCouncils
	return data

def FunctionalAreaRequirements(facilityID, designationID):
	logger.debug('Enter FunctionAreaRequirements for facilityID: {0} designationID: {1}'.format(facilityID, designationID))
	default_functional_areas = [fa.functionalAreaID for fa in am.DefaultFunctionalAreaRequirementRef.query.filter_by(designationID=designationID).all()]
	removed_fa = 0
	for fa in am.FunctionalAreaRequirementsRef.query.filter_by(facilityID=facilityID).all():
		if fa.functionalAreaID not in default_functional_areas:
			am.FunctionalAreaRequirementsRef.delete(fa)
			removed_fa += 1
		else:
			default_functional_areas.remove(fa.functionalAreaID)  # If we already have the functionalArea we do not need to keep it in our list
	logger.debug('Removed {0} functional area requirements for facilityID: {1}'.format(removed_fa, facilityID))
	added_fa = 0
	for functionalAreaID in default_functional_areas:
		am.FunctionalAreaRequirementsRef.create(facilityID=facilityID, functionalAreaID=functionalAreaID)
		added_fa += 1
	logger.debug('Created {0} functional area requirements for facilityID: {1}'.format(added_fa, facilityID))
	flash('Functional Area Requirements Adjusted: {0} added and {1} removed'.format(added_fa, removed_fa), 'info')
