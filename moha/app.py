# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
from flask import Flask, render_template, request, g
import datetime


from moha import commands, public, user, facility, structure, personnel, asset, map, report
from moha import admin
from moha.extensions import bcrypt, cache, csrf_protect, CSRFError, db, debug_toolbar, login_manager, current_user, migrate, mail, admin
from moha.assets import assets
from moha.settings import ProdConfig


def create_app(config_object=ProdConfig):
    """An application factory, as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.

    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_logging(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)
    register_template_filters(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""

    assets.init_app(app)
    bcrypt.init_app(app)
    cache.init_app(app)
    db.init_app(app)
    csrf_protect.init_app(app)
    login_manager.init_app(app)
    debug_toolbar.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)
    admin.init_app(app)
    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(public.views.blueprint)
    app.register_blueprint(user.views.blueprint)
    app.register_blueprint(facility.views.blueprint)
    app.register_blueprint(structure.views.blueprint)
    app.register_blueprint(personnel.views.blueprint)
    app.register_blueprint(asset.views.blueprint)
    app.register_blueprint(map.views.blueprint)
    app.register_blueprint(report.views.blueprint)
    return None


def register_logging(app):
    """Register Flask logging."""
    # https://blog.sneawo.com/blog/2017/07/28/json-logging-in-python/
    import logging
    import logging.config
    from structlog import configure, processors, stdlib, threadlocal

    werkzeugLogger = logging.getLogger('werkzeug')
    werkzeugLogger.disabled = True

    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': True,
        'filters': {
            'request_id': {
                '()': 'moha.logger.RequestIdFilter',
            },
            'user_id': {
                '()': 'moha.logger.UserIdFilter',
            },
        },
        'formatters': {
            'json': {
                'format': '%(message)s %(lineno)d %(pathname)s',
                '()': 'pythonjsonlogger.jsonlogger.JsonFormatter'
            }
        },
        'handlers': {
            'json': {
                'class': 'logging.StreamHandler',
                'formatter': 'json',
                'filters': ['request_id', 'user_id'],
            },
            'file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'json',
                'filters': ['request_id', 'user_id'],
                'filename': app.config['LOG_FILENAME'],
                'maxBytes': app.config['LOG_FILESIZE'],
                'backupCount': app.config['LOG_BACKUPCOUNT'],
            },
        },
        'loggers': {
            '': {
                'handlers': ['json', 'file'],
                'level': logging.DEBUG
            }
        }
    })
    # Confiture structlog to structure our logs into JSON
    configure(
        context_class=threadlocal.wrap_dict(dict),
        logger_factory=stdlib.LoggerFactory(),
        wrapper_class=stdlib.BoundLogger,
        processors=[
            stdlib.filter_by_level,
            stdlib.add_logger_name,
            stdlib.add_log_level,
            stdlib.PositionalArgumentsFormatter(),
            processors.TimeStamper(fmt="iso"),
            processors.StackInfoRenderer(),
            processors.format_exc_info,
            processors.UnicodeDecoder(),
            stdlib.render_to_log_kwargs]
    )
    return None

def register_errorhandlers(app):
    """Register error handlers."""
    def render_error(error):
        """Render error template."""
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        import structlog
        logger = structlog.get_logger(__name__)
        logger.error(error)
        return render_template('{0}.html'.format(error_code), request_id=g.request_id if g.request_id else ''), error_code
   
    @app.errorhandler(CSRFError)
    def handle_csrf_error(e):
        return render_template('csrf_error.html', reason=e)
   
    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)


def register_shellcontext(app):
    """Register shell context objects."""
    def shell_context():
        """Shell context objects."""
        return {
            'db': db,
            'fm': facility.models,
            'sm': structure.models,
            'pm': personnel.models,
            'am': asset.models,
            'cache': cache}

    app.shell_context_processor(shell_context)


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.test)
    app.cli.add_command(commands.lint)
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.urls)
    app.cli.add_command(commands.thumbs)
    app.cli.add_command(commands.tupia)  # temp command to move tupia data into database
    app.cli.add_command(commands.pull_personnel)
    app.cli.add_command(commands.update_personnel)
    

def register_template_filters(app):
    """Register Filtes."""

    def format_days_ago(value):
        now = datetime.datetime.now()
        try:
            return (now - value).days
        except TypeError:
            now = datetime.date.today()
            return (now - value).days

    def color_code(facility):
        status = facility.status
        damaged = facility.damaged
        if status and damaged:
            return 'yellow'
        if status and not damaged:
            return 'green'
        if not status and damaged:
            return 'red'
        if not status and not damaged:
            return 'black'
        else:
            return 'white'

    def condition_color(structure):
        if structure.condition:
            pass

    def none_filter(value):
        if value is not None:
            return value
        else:
            return ''

    app.jinja_env.filters['daysAgo'] = format_days_ago
    app.jinja_env.filters['colorCode'] = color_code
    app.jinja_env.filters['NoneFilter'] = none_filter