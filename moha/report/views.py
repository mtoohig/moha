# -*- coding: utf-8 -*-
"""Report Views"""
import os
import datetime

from flask import Blueprint, request, url_for, send_from_directory, flash, render_template
from moha.extensions import login_required

from sqlalchemy.exc import InvalidRequestError

from moha.facility import models as fm
from moha.facility import forms as ff
from moha.structure import models as sm
from moha.personnel import models as pm
from moha.asset import models as am

from flask import current_app as app

import openpyxl

#import structlog
#logger = structlog.getLogger(__name__)
# WILL NEED TO LOG EVENTUALL?

blueprint = Blueprint('report', __name__, url_prefix='/report', static_folder='../static')


@blueprint.after_request
def add_header(response):
	"""Makes changes to the response header before being passed to the user.
	These changes will force the browser not to store files in cache so
	that reports will be generated eache time with up-to-date data.

	source :: https://stackoverflow.com/questions/13768007/browser-caching-issues-in-flask
	"""
	response.headers['X-UA-Compatible'] = 'IE=Edge,Chrome=1'
	response.headers['Cache-Control'] = 'public, max-age=0'
	response.headers['Pragma'] = 'no-cache'
	return response


# ====================== #
#  E X C E L  U T I L S  #
# ====================== #

def adjust_column_widths(ws):
	for col in ws.columns:
		max_length = 0
		column = col[0].column # Get the column name
		for cell in col:
			try: # Necessary to avoid error on empty cells
				if len(str(cell.value)) > max_length:
					max_length = len(cell.value)
			except:
				pass
		adjusted_width = (max_length + 0) * 1
		ws.column_dimensions[column].width = adjusted_width

def style_range(ws, min_row, max_row, min_col, max_col, border=openpyxl.styles.Border(), fill=None, font=None, alignment=None, vertical=None):
	"""
	Apply styles to a range of cells as if they were a single cell.

	:param ws:  Excel worksheet instance
	:param range: An excel range to style (e.g. A1:F20)
	:param border: An openpyxl Border
	:param fill: An openpyxl PatternFill or GradientFill
	:param font: An openpyxl Font object
	:param alignment: An openpyxl Alignment object
	"""

	top = openpyxl.styles.Border(top=border.top)
	left = openpyxl.styles.Border(left=border.left)
	right = openpyxl.styles.Border(right=border.right)
	bottom = openpyxl.styles.Border(bottom=border.bottom)

	rows = ws.iter_rows(min_row=min_row, max_row=max_row, min_col=min_col, max_col=max_col)

	for row in rows:
		for cell in row:
			if alignment:
				cell.alignment = alignment
			if font:
				cell.font = font
			if vertical:
				cell.border = cell.border + right
			if fill:
				cell.fill = fill

	for row in ws.iter_rows(min_row=min_row, max_row=min_row, min_col=min_col, max_col=max_col):
		for cell in row:
			cell.border = cell.border + top
	for row in ws.iter_rows(min_row=max_row, max_row=max_row, min_col=min_col, max_col=max_col):
		for cell in row:
			cell.border = cell.border + bottom
	for row in ws.iter_rows(min_row=min_row, max_row=max_row, min_col=min_col, max_col=min_col):
		for cell in row:
			cell.border = cell.border + left
	for row in ws.iter_rows(min_row=min_row, max_row=max_row, min_col=max_col, max_col=max_col):
		for cell in row:
			try:
				cell.border = cell.border + right
			except TypeError:
				pass

def facility_color(facility):
	if facility.status and not facility.damaged:
		return 'c6ffba'  # green
	elif not facility.status and not facility.damaged:
		return 'd3d3d3'  # black/gray
	elif not facility.status and facility.damaged:
		return 'ff8282'  # red
	else:
		return 'ffde91'  # orange


def color_row(ws, min_row, max_row=None):
	if not max_row: max_row = min_row
	for rows in ws.iter_rows(min_row=min_row, max_row=max_row, min_col=1, max_col=1):
		for cell in rows:
			cell.fill = openpyxl.styles.PatternFill(bgColor="00D70D", fill_type="lightDown")

# ================= #
#  F A C I L I T Y  #
# ================= #


@blueprint.route('/facility/<facilityID>')
@login_required
def facility(facilityID):
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	
	wb = openpyxl.Workbook()
	ws = wb.create_sheet(facility.identifier, 0)
	
	row = 1
	ws.cell(row=row, column=1, value=facility.identifier)
	ws.cell(row=row, column=2, value=datetime.date.today())
	font = openpyxl.styles.Font(name='Helvetica', size=18, bold=True)
	alignment = openpyxl.styles.Alignment(vertical='center', horizontal='center')	
	style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, font=font, alignment=alignment)
	ws.row_dimensions[row].height = 24
	row += 1
	
	ws = facility_cover(facility, ws)

	# Make columns wide enough to show their contents
	adjust_column_widths(ws)

	ws.page_setup.paperSize = ws.PAPERSIZE_A4
	#ws.page_setup.fitToHeight = 0
	#ws.page_setup.fitToWidth = 1
	ws.print_title_cols = 'A:B'
	ws.print_title_rows = '1:2'

	wb.save(os.path.join(app.config['TEMP_FOLDER'], 'facility_report.xlsx'))
	return send_from_directory(app.config['TEMP_FOLDER'], filename='facility_report.xlsx')


def facility_basics_row(ws, row, data):
	desc_font = openpyxl.styles.Font(bold=True)
	data_font = openpyxl.styles.Font(color="FFBB00") 

	ws.cell(row=row, column=1, value=data[0]).font = desc_font
	ws.cell(row=row, column=2, value=data[1]).font = data_font
	ws.merge_cells(start_row=row, start_column=2, end_row=row, end_column=3)

def facility_basic_structure_rows(ws, row, data):
	row_start = row
	structure_heading = ws.cell(row=row, column=1, value=data[0])	
	structure_heading.font = openpyxl.styles.Font(bold=True)
	structure_heading.alignment= openpyxl.styles.Alignment(horizontal='general', vertical='center')
	
	if not data[1]:
		ws.cell(row=row, column=1, value='None Defined')
		row_end = row_start
		row += 1
	else:
		for s in data[1]:
			ws.cell(row=row, column=2, value=s.identifier)
			ws.cell(row=row, column=3, value='Operational' if s.status else 'Non-Operational')
			ws.cell(row=row, column=4, value=s.description)
			if s.inspection:
				ws.cell(row=row, column=5, value=s.inspection.avg)
				ws.cell(row=row, column=6, value=s.inspection.note)
				ws.cell(row=row, column=7, value=s.inspection.inspectionDate)
			else:
				ws.cell(row=row, column=5, value='No Inspection')
				ws.merge_cells(start_row=row, start_column=5, end_row=row, end_column=7)
			row_end = row
			row += 1
	ws.merge_cells(start_row=row_start, start_column=1, end_row=row_end, end_column=1)

def facility_cover(facility, ws):
	row = 2
	# Primary header
	ws.cell(row=row, column=1, value='{0} Province'.format(facility.province.name))
	row += 1
	ws.cell(row=row, column=1, value=facility.island.name)
	row += 1
	ws.cell(row=row, column=1, value='Information Summary of Site'.upper())
	row += 1
	ws.merge_cells(start_row=3, start_column=1, end_row=3, end_column=3)
	# Data to include in border
	basic_information = [('FacilityID', facility.facilityID),
						 ('Facility Name', facility.facilityName),
						 ('Alternative Name', facility.altFacilityName),
						 ('Area Council', facility.areaCouncil.name),
						 ('Health Zone', facility.healthZone.name),
						 ('Latitude', facility.latitude_format),
						 ('Longitude', facility.longitude_format),
						 ('Land Owner', facility.landOwner),
						 ('Land Owner Contact', facility.landOwnerContact),
						 ('Land Title', facility.landTitle),
						 ('Manager', facility.manager),
						 ('Manager Contact', facility.managerContact)]
	for data in basic_information:
		facility_basics_row(ws, row, data)
		row += 1
	row += 1  # Spacer

	# S T R U C T U R E S
	ws.cell(row=row, column=1, value='Structures and Services'.upper())
	ws.cell(row=row, column=2, value='Facility-wide ID')
	ws.cell(row=row, column=3, value='Status')
	ws.cell(row=row, column=4, value='Description')
	# merge above cells down vertically one row
	ws.merge_cells(start_row=row, start_column=1, end_row=row + 1, end_column=1)
	ws.merge_cells(start_row=row, start_column=2, end_row=row + 1, end_column=2)
	ws.merge_cells(start_row=row, start_column=3, end_row=row + 1, end_column=3)
	ws.merge_cells(start_row=row, start_column=4, end_row=row + 1, end_column=4)
	# create inspection merged cell
	ws.cell(row=row, column=5, value='Inspection')
	ws.merge_cells(start_row=row, start_column=5, end_row=row, end_column=7)
	row += 1
	ws.cell(row=row, column=5, value='Condition')
	ws.cell(row=row, column=6, value='Note')
	ws.cell(row=row, column=7, value='Date')
	row += 1
	basic_structures_information = [('Communications', facility.communications),
									('Water Sources', facility.waterSources),
									('Water Storages', facility.waterStorages),
									('Power', facility.powers)]
	for data in basic_structures_information:
		facility_basic_structure_rows(ws, row, data)
		row += 1

	# Box the data with a thick border
	thick = openpyxl.styles.Side(border_style="thick", color="000000")
	border = openpyxl.styles.Border(top=thick, left=thick, right=thick, bottom=thick)
	#style_range(ws, 'A4:C{0}'.format(row), border=border)

	return ws


# ============= #
#  A S S E T S  #
# ============= #

def _write_file():
	# create a workbook in memory that we will save and send to user
	wb = openpyxl.Workbook()
	

	ws = wb.create_sheet('My Sheet')
	# ws.sheet_properties.tabColor = "1072BA"  # change sheet tab color
	ws = _write_facility_header(ws, facility)


def _asset_attributes_labels():
	return [
			'Category',
			'V#',
			'Status',
			'Manufacturer',
			'Model',
			'Dimensions',
			'Supplier',
			'Cost',
			'Date Purchased',
			'BME #',
			'Serial #']

def _asset_attributes_values(asset):
	return [
			asset.category.type,
			asset.vNum,
			'Operational' if asset.status else 'NonOperational',
			asset.manufacturer,
			asset.model,
			asset.dimensions,
			asset.supplier,
			asset.cost,
			asset.datePurchased, 
			asset.BMENum,
			asset.serialNum]


def _get_location_assets(location, facility):
	known_assets = location.assets
	required_assets = am.AssetRequirementInfo.query.filter_by(designationID=facility.designationID).filter_by(functionalAreaID=location.functionalArea.id).all()

	asset_list = []
	assets_matched = []

	total_required = 0
	total_known = 0
	for ra in required_assets:
		total_required += ra.quantity
		found = []
		required_asset_category_ids = ra.category.category_ids
		for ka in known_assets:
			if ka in assets_matched:
				continue
			if ka.categoryID in required_asset_category_ids:
				total_known += 1
				found.append(ka)
		asset_list.append({'category': ra.category.type, 'quantity': ra.quantity, 'found': found})
		assets_matched += found
	return asset_list


	
@blueprint.route('/assets/facility/<facilityID>')
@login_required
def assets_facility(facilityID):
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	required_areas = am.FunctionalAreaRequirementsRef.query.filter_by(facilityID=facilityID).all()
	required_areas = [ra.functionalAreaID for ra in required_areas]

	missing = request.args.get('missing')
	required = request.args.get('required')
	known = request.args.get('known')
	gap = request.args.get('gap')

	wb = openpyxl.Workbook()
	ws = wb.create_sheet('Assets', 0)
	row = 1

	#  F A C I L I T Y 
	#----------------
	ws.cell(row=row, column=1, value=facility.identifier)
	font = openpyxl.styles.Font(name='Helvetica', size=18, bold=True)
	alignment = openpyxl.styles.Alignment(vertical='center', horizontal='center')	
	style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, font=font, alignment=alignment)
	ws.row_dimensions[row].height = 24
	row += 1

	#  S T R U C T U R E 
	#------------------
	for structure in (s for s in facility.structures if s.functional_areas):
		ws.cell(row=row, column=1, value=structure.identifier)
		thick = openpyxl.styles.Side(border_style="thick", color="000000")
		border = openpyxl.styles.Border(top=thick, left=thick, right=thick, bottom=thick)
		fill = openpyxl.styles.PatternFill(bgColor='d3d3d3', fill_type='lightDown')
		font = openpyxl.styles.Font(size=16, bold=True)
		style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, fill=fill, alignment=alignment, font=font, vertical=True)
		ws.row_dimensions[row].height = 24
		row += 1

		#  L O C A T I O N
		#----------------
		for location in structure.functional_areas:
			print location.functionalAreaID, required_areas
			if location.functionalAreaID in required_areas:
				print location
				required_areas.remove(location.functionalAreaID)
			ws.cell(row=row, column=1, value=location.identifier)
			thin = openpyxl.styles.Side(border_style="thin", color="000000")
			border = openpyxl.styles.Border(top=thick, left=thin, right=thin, bottom=thick)
			fill = openpyxl.styles.PatternFill(bgColor='00e233', fill_type='lightDown')
			font = openpyxl.styles.Font(size=12)
			style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, alignment=alignment, fill=fill, font=font)
			ws.row_dimensions[row].hidden = False
			ws.row_dimensions[row].outlineLevel = 1
			row += 1
			
			data = _get_location_assets(location, facility)
			
			#  R E Q U I R E D  A S S E T S
			#-----------------------------
			if required:
				if known or gap:
					ws.cell(row=row, column=1, value='Required Asset')
					border = openpyxl.styles.Border(top=thin, left=thin, right=thin, bottom=thin)
					fill = openpyxl.styles.PatternFill(bgColor='87e5ff', fill_type='lightDown')
					style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, fill=fill)
					ws.row_dimensions[row].outlineLevel = 1
					ws.row_dimensions[row].outlineLevel = 2
					ws.row_dimensions[row].height = 18
					row += 1
							
				ws.cell(row=row, column=1, value='Asset Category')
				ws.cell(row=row, column=2, value='Quantity Required')
				ws.cell(row=row, column=3, value='Quantity Known')
				ws.row_dimensions[row].hidden = False
				ws.row_dimensions[row].outlineLevel = 1
				ws.row_dimensions[row].outlineLevel = 2
				if known or gap:
					ws.row_dimensions[row].outlineLevel = 3
				
				row += 1
				for asset in data:
					ws.cell(row=row, column=1, value=asset['category'])
					ws.cell(row=row, column=2, value=asset['quantity'])
					ws.cell(row=row, column=3, value=len(asset['found']))
					ws.row_dimensions[row].hidden = False
					ws.row_dimensions[row].outlineLevel = 1
					ws.row_dimensions[row].outlineLevel = 2
					if known or gap:
						ws.row_dimensions[row].outlineLevel = 3
					row += 1

			#  K N O W N  A S S E T S
			#-----------------------
			if known:
				if required or gap:
					ws.cell(row=row, column=1, value='Known Assets')
					style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, fill=fill)
					ws.row_dimensions[row].outlineLevel = 1
					ws.row_dimensions[row].outlineLevel = 2
					ws.row_dimensions[row].height = 18
					row += 1
				
				col = 1
				for attr in _asset_attributes_labels():
					ws.cell(row=row, column=col, value=attr)
					col += 1
				ws.row_dimensions[row].hidden = False
				ws.row_dimensions[row].outlineLevel = 1
				ws.row_dimensions[row].outlineLevel = 2
				if required or gap:
					ws.row_dimensions[row].outlineLevel = 3

				row += 1
				for asset in location.assets:
					col = 1 #asset_col
					for attr in _asset_attributes_values(asset):
						ws.cell(row=row, column=col, value=attr)
						col += 1
					ws.row_dimensions[row].hidden = False
					ws.row_dimensions[row].outlineLevel = 1
					ws.row_dimensions[row].outlineLevel = 2
					if required or gap:
						ws.row_dimensions[row].outlineLevel = 3
					row += 1

			#  M I S S I N G  A S S E T S
			#---------------------------
			if gap:
				if required or known:
					ws.cell(row=row, column=1, value='Asset Gap Analysis')
					style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, fill=fill)
					ws.row_dimensions[row].outlineLevel = 1
					ws.row_dimensions[row].outlineLevel = 2
					ws.row_dimensions[row].height = 18
					row += 1
								
				ws.cell(row=row, column=1, value='Asset Category')
				ws.cell(row=row, column=2, value='Quantity Needed')
				ws.row_dimensions[row].hidden = False
				ws.row_dimensions[row].outlineLevel = 1
				ws.row_dimensions[row].outlineLevel = 2
				if required or known:
					ws.row_dimensions[row].outlineLevel = 3
				
				row += 1
				for asset in data:
					if asset['quantity'] > len(asset['found']):
						ws.cell(row=row, column=1, value=asset['category'])
						ws.cell(row=row, column=2, value=(asset['quantity'] - len(asset['found'])))
						ws.row_dimensions[row].hidden = False
						ws.row_dimensions[row].outlineLevel = 1
						ws.row_dimensions[row].outlineLevel = 2
						if required or known:
							ws.row_dimensions[row].outlineLevel = 3
						row += 1

	if missing:
		ws.cell(row=row, column=1, value='Missing Areas')
		thick = openpyxl.styles.Side(border_style="thick", color="000000")
		border = openpyxl.styles.Border(top=thick, left=thick, right=thick, bottom=thick)
		fill = openpyxl.styles.PatternFill(bgColor='d3d3d3', fill_type='lightDown')
		font = openpyxl.styles.Font(size=16, bold=True)
		style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, fill=fill, alignment=alignment, font=font, vertical=True)
		ws.row_dimensions[row].height = 24
		row += 1

		for missing_area in required_areas:
			functionalArea = am.FunctionalAreaRef.query.filter_by(id=missing_area).one()
			ws.cell(row=row, column=1, value=functionalArea.identifier)
			thin = openpyxl.styles.Side(border_style="thin", color="000000")
			border = openpyxl.styles.Border(top=thick, left=thin, right=thin, bottom=thick)
			fill = openpyxl.styles.PatternFill(bgColor='00e233', fill_type='lightDown')
			font = openpyxl.styles.Font(size=12)
			style_range(ws, min_row=row, max_row=row, min_col=1, max_col=1, border=border, alignment=alignment, fill=fill, font=font)
			ws.row_dimensions[row].hidden = False
			ws.row_dimensions[row].outlineLevel = 1
			row += 1
			
			data = am.AssetRequirementInfo.query.filter_by(functionalAreaID=missing_area, designationID=facility.designationID).all()

			ws.cell(row=row, column=1, value='Asset Category')
			ws.cell(row=row, column=2, value='Quantity Required')
			ws.row_dimensions[row].hidden = False
			ws.row_dimensions[row].outlineLevel = 1
			ws.row_dimensions[row].outlineLevel = 2
			
			row += 1
			for asset in data:
				ws.cell(row=row, column=1, value=asset.category.type)
				ws.cell(row=row, column=2, value=asset.quantity)
				ws.row_dimensions[row].hidden = False
				ws.row_dimensions[row].outlineLevel = 1
				ws.row_dimensions[row].outlineLevel = 2
				row += 1

	adjust_column_widths(ws)
	wb.save(os.path.join(app.config['TEMP_FOLDER'], 'assets_facility.xlsx'))
	return send_from_directory(app.config['TEMP_FOLDER'], filename='assets_facility.xlsx')


@blueprint.route('/assets/location/<int:locationID>')
@login_required
def assets_location(locationID):
	"""
	All assets of one location

	- facility header
	- structure
	- location name etc

	Sections fold over
	=expected assets and how many found
	=known assets (mark extra assets)
	=assets requirements to meet standard

	"""
	location = am.FunctionalAreaLocationRef.query.filter_by(id=locationID).first_or_404()
	facilityID = location.structure.facility.facilityID
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()

	known_assets = location.assets
	required_assets = am.AssetRequirementInfo.query.filter_by(designationID=facility.designationID).filter_by(functionalAreaID=location.functionalArea.id).all()

	asset_list = []
	assets_matched = []

	total_required = 0
	total_known = 0
	for ra in required_assets:
		total_required += ra.quantity
		found = []
		required_asset_category_ids = ra.category.category_ids
		for ka in known_assets:
			if ka in assets_matched:
				continue
			if ka.categoryID in required_asset_category_ids:
				total_known += 1
				found.append(ka)
		asset_list.append({'category': ra.category.type, 'quantity': ra.quantity, 'found': found})
		assets_matched += found

	wb = openpyxl.Workbook()
	ws = wb.create_sheet(location.identifier, 0)
	#asset_col used to place assets a few columns right of required assets
	#asset_col = 4

	ws.cell(row=1, column=1, value=facility.identifier)
	ws.cell(row=2, column=1, value=location.structure.identifier)
	ws.cell(row=3, column=1, value=location.identifier)
	ws.cell(row=4, column=1, value='Compiled on {0}'.format(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")))

	row = 5
	# Required Assets
	ws.cell(row=row, column=1, value='Required Asset')
	ws.row_dimensions[row].height = 24
	color_row(ws, row)
	row += 1
	ws.cell(row=row, column=1, value='Asset Category')
	ws.cell(row=row, column=2, value='Quantity Required')
	ws.cell(row=row, column=3, value='Quantity Known')
	ws.row_dimensions[row].hidden = True
	ws.row_dimensions[row].outlineLevel = 1
	row += 1
	for asset in asset_list:
		ws.cell(row=row, column=1, value=asset['category'])
		ws.cell(row=row, column=2, value=asset['quantity'])
		ws.cell(row=row, column=3, value=len(asset['found']))
		# Not going to show found assets yet
		#for found in asset['found']:
		#	row += 1
		#	col = asset_col
		#	for attr in _asset_attributes_values(found):
		#		ws.cell(row=row, column=col, value=attr)
		#		col += 1
		ws.row_dimensions[row].hidden = True
		ws.row_dimensions[row].outlineLevel = 1
		row += 1

	# Known Assets
	ws.cell(row=row, column=1, value='Known Assets')
	ws.row_dimensions[row].height = 24
	color_row(ws, row)
	row += 1
	col = 1 #asset_col
	for attr in _asset_attributes_labels():
		ws.cell(row=row, column=col, value=attr)
		col += 1
	ws.row_dimensions[row].hidden = True
	ws.row_dimensions[row].outlineLevel = 1
	row += 1

	for asset in known_assets:
		col = 1 #asset_col
		for attr in _asset_attributes_values(asset):
			ws.cell(row=row, column=col, value=attr)
			col += 1
		ws.row_dimensions[row].hidden = True
		ws.row_dimensions[row].outlineLevel = 1
		row += 1

	# Missing Assets
	ws.cell(row=row, column=1, value='Asset Gap Analysis')
	ws.row_dimensions[row].height = 24
	color_row(ws, row)
	row += 1
	ws.cell(row=row, column=1, value='Asset Category')
	ws.cell(row=row, column=2, value='Quantity Needed')
	ws.row_dimensions[row].hidden = True
	ws.row_dimensions[row].outlineLevel = 1
	row += 1
	for asset in asset_list:
		if asset['quantity'] > len(asset['found']):
			ws.cell(row=row, column=1, value=asset['category'])
			ws.cell(row=row, column=2, value=(asset['quantity'] - len(asset['found'])))
			ws.row_dimensions[row].hidden = True
			ws.row_dimensions[row].outlineLevel = 1
			row += 1

	adjust_column_widths(ws)
	wb.save(os.path.join(app.config['TEMP_FOLDER'], 'assets_location.xlsx'))
	return send_from_directory(app.config['TEMP_FOLDER'], filename='assets_location.xlsx')


"""
get assets in simple rows
get extra in rows

return with location

have a larger function wrap the code in a nice presentation for xlsx

"""


"""
def asset_row(ws, row, col, asset):
	ws.cell(row=row, column=col, value=asset.location.structure.identifier)
	col += 1
	ws.cell(row=row, column=col, value=asset.location.identifier)
	col += 1
	ws.cell(row=row, column=col, value=asset.category.type)
	col += 1
	ws.cell(row=row, column=col, value=asset.status)
	col += 1
	ws.cell(row=row, column=col, value=asset.note)
	col += 1
	ws.cell(row=row, column=col, value=asset.dimensions)
	col += 1
	ws.cell(row=row, column=col, value=asset.datePurchased)
	col += 1
	ws.cell(row=row, column=col, value=asset.cost)
	col += 1
	ws.cell(row=row, column=col, value=asset.vNum)
	col += 1
	ws.cell(row=row, column=col, value=asset.serialNum)
	col += 1
	ws.cell(row=row, column=col, value=asset.BMENum)

@blueprint.route('/assets/')
@login_required
def assets():

	wb = openpyxl.Workbook()
	ws = wb.create_sheet('Assets', 0)

	row = 1
	for p in fm.ProvinceInfo.query.all():
		ws.cell(row=row, column=1, value=p.name)
		province_assets = am.AssetInfo.query.join(am.FunctionalAreaLocationRef)\
											.join(sm.StructureInfo)\
											.join(fm.FacilityInfo)\
											.join(am.AssetCategoryInfo)\
											.order_by(fm.FacilityInfo.islandID)\
											.order_by(fm.FacilityInfo.facilityID)\
											.order_by(sm.StructureInfo.id)\
											.order_by(am.FunctionalAreaLocationRef.functionalAreaID)\
											.order_by(am.AssetCategoryInfo.type)\
											.filter(fm.FacilityInfo.provinceID==p.id).all()
		ws.cell(row=row, column=2, value=len(province_assets))
		ws.cell(row=row, column=2, value=len(province_assets))
		row += 1
		if province_assets:
			for asset in province_assets:
				asset_col = 3
				asset_row(ws, row, asset_col, asset)
				ws.row_dimensions[row].hidden = True
				ws.row_dimensions[row].outlineLevel = 1
				row += 1  # row for next asset
		else:
			ws.cell(row=row, column=1, value='None')
			ws.row_dimensions[row].hidden = True
			ws.row_dimensions[row].outlineLevel = 1
			row += 1  # row for next province
	row += 1

	adjust_column_widths(ws)
	wb.save('assets_test.xlsx')
	return '2'



##
## perhaps remove this in place of generic assets search and optional filter
##
## Or remove the duplicate code to some function
##
@blueprint.route('/assets/category/<categoryID>')
@login_required
def assets_category(categoryID):
	category = am.AssetCategoryInfo.query.filter_by(categoryID=categoryID).first_or_404()
	cat_ids = category.category_ids()

	wb = openpyxl.Workbook()
	ws = wb.create_sheet(category.type, 0)

	row = 1
	ws.cell(row=row, column=1, value=category.type)
	row += 1
	for p in fm.ProvinceInfo.query.all():
		ws.cell(row=row, column=1, value=p.name)
		province_assets = am.AssetInfo.query.filter(am.AssetInfo.categoryID.in_(cat_ids))\
											.join(am.FunctionalAreaLocationRef)\
											.join(sm.StructureInfo)\
											.join(fm.FacilityInfo)\
											.order_by(fm.FacilityInfo.islandID)\
											.order_by(fm.FacilityInfo.facilityID)\
											.filter_by(provinceID=p.id).all()
		ws.cell(row=row, column=2, value=len(province_assets))
		row += 1
		if province_assets:
			for asset in province_assets:
				asset_row(ws, row, 3, asset)
				ws.row_dimensions[row].hidden = True
				ws.row_dimensions[row].outlineLevel = 1
				row += 1  # row for next asset
		else:
			ws.cell(row=row, column=1, value='None')
			ws.row_dimensions[row].hidden = True
			ws.row_dimensions[row].outlineLevel = 1
			row += 1  # row for next province
	row += 1

	adjust_column_widths(ws)
	wb.save('assets_test.xlsx')
	return '8'
"""

@blueprint.route('/assets')
@login_required
def assets():
	categoryID = request.args.get('categoryID')
	if categoryID:
		category = am.AssetCategoryInfo.query.filter_by(categoryID=categoryID).first_or_404()
		cat_ids = category.category_ids
		assets = am.AssetInfo.query.filter(am.AssetInfo.categoryID.in_(cat_ids))\
										.join(am.FunctionalAreaLocationRef)\
										.join(sm.StructureInfo)\
										.join(fm.FacilityInfo)\
										.join(am.AssetCategoryInfo)\
										.order_by(fm.FacilityInfo.islandID)\
										.order_by(fm.FacilityInfo.facilityID)\
										.order_by(sm.StructureInfo.id)\
										.order_by(am.FunctionalAreaLocationRef.functionalAreaID)\
										.order_by(am.AssetCategoryInfo.type)\
										.all()
	else:
		assets = am.AssetInfo.query\
									.join(am.FunctionalAreaLocationRef)\
									.join(sm.StructureInfo)\
									.join(fm.FacilityInfo)\
									.join(am.AssetCategoryInfo)\
									.order_by(fm.FacilityInfo.islandID)\
									.order_by(fm.FacilityInfo.facilityID)\
									.order_by(sm.StructureInfo.id)\
									.order_by(am.FunctionalAreaLocationRef.functionalAreaID)\
									.order_by(am.AssetCategoryInfo.type)\
									.all()


	wb = openpyxl.Workbook()
	ws = wb.create_sheet('Assets', 0)

	row = 1
	col = 1
	ws.cell(row=row, column=col, value='Facility ID')
	col += 1
	ws.cell(row=row, column=col, value='Facility Name')
	col += 1	
	ws.cell(row=row, column=col, value='Designation')
	col += 1
	col_group_location_start = col
	ws.cell(row=row, column=col, value='Province')
	col += 1	
	ws.cell(row=row, column=col, value='Island')
	col += 1	
	ws.cell(row=row, column=col, value='Health Zone')
	col += 1	
	ws.cell(row=row, column=col, value='Area Council')
	col += 1	
	ws.cell(row=row, column=col, value='Latitude')
	col += 1	
	ws.cell(row=row, column=col, value='Longitude')
	col += 1
	ws.cell(row=row, column=col, value='Structure')
	col += 1
	ws.cell(row=row, column=col, value='Location')
	col_group_location_end = col
	col += 1
	ws.cell(row=row, column=col, value='Category')
	col += 1
	ws.cell(row=row, column=col, value='Status')
	col += 1
	ws.cell(row=row, column=col, value='Note')
	col += 1
	ws.cell(row=row, column=col, value='Dimensions')
	col += 1
	ws.cell(row=row, column=col, value='Date Purchased/Installed')
	col += 1
	ws.cell(row=row, column=col, value='Cost')
	col += 1
	ws.cell(row=row, column=col, value='V#')
	col += 1
	ws.cell(row=row, column=col, value='Serial#')
	col += 1
	ws.cell(row=row, column=col, value='BME#')

	thick = openpyxl.styles.Side(border_style="thick", color="000000")
	border = openpyxl.styles.Border(top=thick, left=thick, right=thick, bottom=thick)
	fill = openpyxl.styles.PatternFill(bgColor='d3d3d3', fill_type='lightDown')
	alignment = openpyxl.styles.Alignment(vertical='center', horizontal='center')
	style_range(ws, min_row=row, max_row=row, min_col=1, max_col=col, border=border, fill=fill, alignment=alignment, vertical=True)
	ws.row_dimensions[row].height = 24

	row += 1
	for asset in assets:
		col = 1
		ws.cell(row=row, column=col, value=asset.location.structure.facility.facilityID)
		col += 1
		ws.cell(row=row, column=col, value=asset.location.structure.facility.facilityName)
		col += 1	
		ws.cell(row=row, column=col, value=asset.location.structure.facility.designation.name)
		col += 1
		ws.cell(row=row, column=col, value=asset.location.structure.facility.province.name)
		col += 1	
		ws.cell(row=row, column=col, value=asset.location.structure.facility.island.name)
		col += 1	
		ws.cell(row=row, column=col, value=asset.location.structure.facility.healthZone.name)
		col += 1	
		ws.cell(row=row, column=col, value=asset.location.structure.facility.areaCouncil.name if asset.location.structure.facility.areaCouncil else '')
		col += 1	
		ws.cell(row=row, column=col, value=asset.location.structure.facility.latitude_format)
		col += 1	
		ws.cell(row=row, column=col, value=asset.location.structure.facility.longitude_format)
		col += 1
		ws.cell(row=row, column=col, value=asset.location.structure.identifier)
		col += 1
		ws.cell(row=row, column=col, value=asset.location.identifier)
		col += 1
		ws.cell(row=row, column=col, value=asset.category.type)
		col += 1
		ws.cell(row=row, column=col, value='Operational' if asset.status else 'Non-Operational')
		col += 1
		ws.cell(row=row, column=col, value=asset.note)
		col += 1
		ws.cell(row=row, column=col, value=asset.dimensions)
		col += 1
		ws.cell(row=row, column=col, value=asset.datePurchased)
		col += 1
		ws.cell(row=row, column=col, value=asset.cost)
		col += 1
		ws.cell(row=row, column=col, value=asset.vNum)
		col += 1
		ws.cell(row=row, column=col, value=asset.serialNum)
		col += 1
		ws.cell(row=row, column=col, value=asset.BMENum)
		row += 1

	row += 1

	adjust_column_widths(ws)
	ws.column_dimensions.group(start=openpyxl.utils.get_column_letter(col_group_location_start), end=openpyxl.utils.get_column_letter(col_group_location_end), hidden=True)
	wb.save(os.path.join(app.config['TEMP_FOLDER'], 'assets.xlsx'))
	return send_from_directory(app.config['TEMP_FOLDER'], filename='assets.xlsx')	


@blueprint.route('/facilities')
@login_required
def facilities():
	kwargs = {a: int(request.args.get(a)) for a in request.args if int(request.args.get(a)) >= 0}
	print kwargs
	try:
		facilities = fm.FacilityInfo.query.filter_by(**kwargs).all()
	except InvalidRequestError:
		flash('Fail request', 'danger')
		return 
	wb = openpyxl.Workbook()
	ws = wb.create_sheet('Facilities', 0)

	row = 1
	col = 1
	ws.cell(row=row, column=col, value='Facility ID')
	col += 1
	ws.cell(row=row, column=col, value='Facility Name')
	col += 1	
	ws.cell(row=row, column=col, value='Designation')
	col += 1
	ws.cell(row=row, column=col, value='Province')
	col += 1	
	ws.cell(row=row, column=col, value='Island')
	col += 1	
	ws.cell(row=row, column=col, value='Health Zone')
	col += 1	
	ws.cell(row=row, column=col, value='Area Council')
	col += 1	
	ws.cell(row=row, column=col, value='Latitude')
	col += 1	
	ws.cell(row=row, column=col, value='Longitude')
	# All Structures
	col += 1
	ws.cell(row=row, column=col, value='Condition')
	col += 1
	ws.cell(row=row, column=col, value='% Overdue Inspections')
	col += 1
	ws.cell(row=row, column=col, value='Operational Percent')
	# Buildings
	col += 1
	ws.cell(row=row, column=col, value='Buildings Condition')
	col += 1
	ws.cell(row=row, column=col, value='Buildings Overdue Inspections')
	col += 1
	ws.cell(row=row, column=col, value='Buildings Operational Percent')

	col += 1
	ws.cell(row=row, column=col, value='% Positions Filled')
	col += 1
	ws.cell(row=row, column=col, value='Positions Filled')
	col += 1
	ws.cell(row=row, column=col, value='Total Positions')

	thick = openpyxl.styles.Side(border_style="thick", color="000000")
	border = openpyxl.styles.Border(top=thick, left=thick, right=thick, bottom=thick)
	fill = openpyxl.styles.PatternFill(bgColor='d3d3d3', fill_type='lightDown')
	alignment = openpyxl.styles.Alignment(vertical='center', horizontal='center')
	style_range(ws, min_row=row, max_row=row, min_col=1, max_col=col, border=border, fill=fill, alignment=alignment, vertical=True)
	ws.row_dimensions[row].height = 24

	row += 1
	for facility in facilities:
		col = 1
		ws.cell(row=row, column=col, value=facility.facilityID)
		col += 1
		ws.cell(row=row, column=col, value=facility.facilityName)
		col += 1	
		ws.cell(row=row, column=col, value=facility.designation.name)
		col += 1
		ws.cell(row=row, column=col, value=facility.province.name)
		col += 1	
		ws.cell(row=row, column=col, value=facility.island.name)
		col += 1	
		ws.cell(row=row, column=col, value=facility.healthZone.name)
		col += 1	
		ws.cell(row=row, column=col, value=facility.areaCouncil.name if facility.areaCouncil else '')
		col += 1	
		ws.cell(row=row, column=col, value=facility.latitude_format)
		col += 1	
		ws.cell(row=row, column=col, value=facility.longitude_format)

		# All structures
		col += 1
		ws.cell(row=row, column=col, value=facility.condition())
		col += 1
		ws.cell(row=row, column=col, value=facility.overdue_percent())
		col += 1
		ws.cell(row=row, column=col, value=facility.operational_percent()*100)
		# Buildings
		col += 1
		ws.cell(row=row, column=col, value=facility.condition('B'))
		col += 1
		ws.cell(row=row, column=col, value=facility.overdue_percent('B'))
		col += 1
		ws.cell(row=row, column=col, value=facility.operational_percent('B')*100)

		col += 1
		ws.cell(row=row, column=col).value = facility.positions_filled_percent
		col += 1
		filled_positions = ws.cell(row=row, column=col, value=len(facility.positions_filled))
		col += 1
		total_positions = ws.cell(row=row, column=col, value=len(facility.positions))



		dotted = openpyxl.styles.Side(border_style="dotted", color="d3d3d3")
		hair = openpyxl.styles.Side(border_style="hair", color="000000")
		fill = openpyxl.styles.PatternFill(bgColor=facility_color(facility), fill_type='lightDown')
		border = openpyxl.styles.Border(top=hair, left=hair, right=hair, bottom=hair)
		style_range(ws, min_row=row, max_row=row, min_col=1, max_col=col, border=border, fill=fill, vertical=True)

		row += 1

	# Complete
	adjust_column_widths(ws)
	wb.save(os.path.join(app.config['TEMP_FOLDER'], 'facilities.xlsx'))
	return send_from_directory(app.config['TEMP_FOLDER'], filename='facilities.xlsx')