from flask import url_for, redirect, request, flash
from redis import Redis
from flask_admin.contrib import rediscli
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user
from wtforms import SelectField, PasswordField

# required for creating custom filters
from flask_admin.contrib.sqla.filters import BaseSQLAFilter, FilterEqual

#from flask_admin.menu import MenuLink

from moha.extensions import admin, db
from moha.user import models as um
from moha.facility import models as fm
from moha.structure import models as sm
from moha.personnel import models as pm
from moha.asset import models as am


from moha.structure.forms import BuildingInspectionForm
from moha.asset.forms import AssetCategoryForm
from moha.personnel.forms import PositionForm



def FunctionalAreaRequirements(facilityID, designationID):
    default_functional_areas = [fa.functionalAreaID for fa in am.DefaultFunctionalAreaRequirementRef.query.filter_by(designationID=designationID).all()]
    removed_fa = 0
    for fa in am.FunctionalAreaRequirementsRef.query.filter_by(facilityID=facilityID).all():
        if fa.functionalAreaID not in default_functional_areas:
            am.FunctionalAreaRequirementsRef.delete(fa)
            removed_fa += 1
        else:
            default_functional_areas.remove(fa.functionalAreaID)  # If we already have the functionalArea we do not need to keep it in our list
    added_fa = 0
    for functionalAreaID in default_functional_areas:
        am.FunctionalAreaRequirementsRef.create(facilityID=facilityID, functionalAreaID=functionalAreaID)
        added_fa += 1
    flash('Functional Area Requirements Adjusted: {0} added and {1} removed'.format(added_fa, removed_fa), 'info')


class MyModelView(ModelView):

    column_default_sort = 'id'

    details_modal = True
    create_modal = True
    edit_modal = True

    column_exclude_list = ('icon')

    column_labels = {'dateConstructed': 'Date',
                     'vin': 'Vin Number',
                     'plate': 'Plate Number',
                     'engineSize': 'Engine Size',
                     'engineNum': 'Engine Number',
                     'avg': 'Average Condition',
#                     'condition': 'Condition Score',
                     'inspectorName': 'Inspector',
                     'inspectionDate': 'Inspection Date',
                     'compilationDate': 'Compilation Date',
                     'inspectionDate': 'Inspection Date',
                     'cycloneTies': 'Cyclone Ties',
                     'cycloneScrews': 'Cyclone Screws',
                     'visibleSub': 'Visible Sub-Structure',
                     'facilityName': 'Name',
                     'altFacilityName': 'Alterative Name',
                     'healthZone': 'Health Zone',
                     'landOwner': 'Land Owner',
                     'landOwnerContact': 'Land Owner Contact',
                     'landTitle': 'Land Title',
                     'managerContact': 'Manger Contact',
                     'facility.facilityID': 'Facility ID',
                     'facility.facilityName': 'Facility Name',
                     'type.type': 'Type',
                     'facilityID': 'Facility ID',
                     }

    column_descriptions = {'icon': 'The icon that is shown on the webpage',
                           'structure_type': 'The short code that represents this thing in the database',
                           'services': 'The other things that provide their service to this thing',
                           'servicing': 'The other things that depend on this thing for its service',
                           'type': 'The list of possible types that define this thing',
                           'dateConstructed': 'When this thing was delivered/installed/opened',
                           'avg': 'The average condition of this thing',
#                           'condition': 'The calculated condition of this thing, usually with criticality taken into consideration',
                           'note': "The comments you would like to make about this",
                           'inspectionDate': 'When was this thing inspected',
                           'compilationDate': "The date you submit the form about this thing's inspection",
                           'inspectorName': 'The person who inspected this thing',
                           'damaged': 'True/False if the facility is currently damaged',
                           'altFacilityName': 'A different different or local name that facility is known by',
                           'criticality': 'A measure of how critical something is for calculating facility condition scores',

                           }

    def is_accessible(self):
        return current_user.is_authenticated

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return redirect(url_for('public.home', next=request.url))


# https://github.com/flask-admin/flask-admin/blob/master/examples/sqla-custom-filter/app.py
class FilterProvince(BaseSQLAFilter):
    def apply(self, query, value, alias=None):
        return query.filter(self.column == value)

    def operation(self):
        return 'Province Choice'

provinceChoices = ((1,'TORBA'),(2,'PENAMA'),(3,'SANMA'),(4,'MALAMPA'),(5,'SHEFA'),(6,'TAFEA'))

class FacilityView(MyModelView):

    can_create = False  # Form causes other calculations and changes to the database which are built into the app already
    can_edit = False

    column_display_pk = True
    column_default_sort = 'facilityID'

    columns = ['facilityID',
                 'facilityName',
                 'altFacilityName',
                 'designation',
                 'ownership',
                 'province',
                 'island',
                 'healthZone',
                 'areaCouncil',
                 'landOwner',
                 'landOwnerContact',
                 'landTitle',
                 'manager',
                 'managerContact',
                 ]

    column_filters = [
                    'facilityID',
                    'facilityName',
                    FilterProvince(column=fm.FacilityInfo.provinceID, name='Province', options=provinceChoices),
                    ]
    
    column_labels = {'province.name': 'Province',
                     'facilityID': 'Facility ID',
                     'facilityName': 'Facility Name'}


    def __init__(self, *args, **kwargs):
        super(FacilityView, self).__init__(*args, **kwargs)

admin.add_view(FacilityView(fm.FacilityInfo, db.session, name='Facilities'))


# ============= #
#  T H I N G S  # (structures and services)
# ============= #

class ThingView(MyModelView):

    # I do not want users to think they have to select an inspection when creating a thing.
    # More than likely there will not be an inspection before a thing exists.
    form_excluded_columns = ('inspections')

    # This overrides the default Field type for each of the following fields in the form.
    # Then we can can define the arguments allowed with 'form_args'
    form_overrides = {'structure_type': SelectField,
                      'icon': SelectField}

    column_filters = ['facility.facilityID', 'facility.facilityName', 'dateConstructed', 'type.type']
                     
    def __init__(self, *args, **kwargs):
        super(ThingView, self).__init__(category='Things', *args, **kwargs)


class StructureView(ThingView):
    """This view is strictly for viewing only"""
    can_edit = False
    #can_delete = False
    can_create = False
    details_view = True
    column_display_actions = False

    column_filters = ['facility.facilityID', 'dateConstructed']

admin.add_view(StructureView(sm.StructureInfo, db.session, name='All'))

class BuildingView(ThingView):
    # This sets the 'structure_type' choices in this class to only be 'B' for buildings.
    form_args = {'structure_type': {'choices': [('B', 'B')]},
                 'icon': {'choices': [('home', 'home')]}
                 }


class WaterSourceView(ThingView):
    form_args = {'structure_type': {'choices': [('WSR', 'WSR')]},
                 'icon': {'choices': [('tint', 'tint')]}
                 }

class WaterStorageView(ThingView):
    form_args = {'structure_type': {'choices': [('WST', 'WST')]},
                 'icon': {'choices': [('tint', 'tint')]}
                 }    

class PowerView(ThingView):
    form_args = {'structure_type': {'choices': [('P', 'P')]},
                 'icon': {'choices': [('bolt', 'bolt')]}
                 }    

class SanitationView(ThingView):
    form_args = {'structure_type': {'choices': [('S', 'S')]},
                 'icon': {'choices': [('shower', 'shower')]}
                 }

class CommunicationView(ThingView):
    form_args = {'structure_type': {'choices': [('C', 'C')]},
                 'icon': {'choices': [('phone', 'phone')]}
                 }    

class ColdStorageView(ThingView):
    form_args = {'structure_type': {'choices': [('CST', 'CST')]},
                 'icon': {'choices': [('snowflake-o', 'snowflake-o')]}
                 }

class VehicleView(ThingView):
    form_args = {'structure_type': {'choices': [('V', 'V')]},
                 'icon': {'choices': [('car', 'car')]}
                 }   

class BoatView(ThingView):
    form_args = {'structure_type': {'choices': [('BO', 'BO')]},
                 'icon': {'choices': [('ship', 'ship')]}
                 }  

admin.add_view(BuildingView(sm.BuildingInfo, db.session, name='Buildings'))
admin.add_view(WaterSourceView(sm.WaterSourceInfo, db.session, name='Water Sources'))
admin.add_view(WaterStorageView(sm.WaterStorageInfo, db.session, name='Water Storage'))
admin.add_view(PowerView(sm.PowerInfo, db.session, name='Power'))
admin.add_view(SanitationView(sm.SanitationInfo, db.session, name='Sanitation'))
admin.add_view(CommunicationView(sm.CommunicationInfo, db.session, name='Communication'))
admin.add_view(ColdStorageView(sm.ColdStorageInfo, db.session, name='Cold Storage'))
admin.add_view(VehicleView(sm.VehicleInfo, db.session, name='Vehicle'))
admin.add_view(BoatView(sm.BoatInfo, db.session, name='Boat'))



# ======================= #
#  I N S P E C T I O N S  #
# ======================= #

class InspectionView(MyModelView):

    form_excluded_columns = ('compilationDate')

    # This overrides the default Field type for each of the following fields in the form.
    # Then we can can define the arguments allowed with 'form_args'
    form_overrides = {'structure_type': SelectField}

    column_filters = ['inspectionDate', 'compilationDate', 'inspectorName']

    def __init__(self, *args, **kwargs):
        super(InspectionView, self).__init__(category='Inspections', *args, **kwargs)


class StructureInspectionView(InspectionView):
    """This view is strictly for viewing only"""
    can_edit = False
    can_delete = False
    can_create = False
    details_view = True
    column_display_actions = False

admin.add_view(StructureInspectionView(sm.StructureInspection, db.session, name='All'))


class BuildingInspectionView(InspectionView):
    # This sets the 'structure_type' choices in this class to only be 'B' for buildings.
    form_args = {'structure_type': {'choices': [('B', 'B')]}}

    form = BuildingInspectionForm

class WaterSourceInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('WSR', 'WSR')]}}


class WaterStorageInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('WST', 'WST')]}}


class PowerInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('P', 'P')]}}


class SanitationInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('S', 'S')]}}


class CommunicationInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('C', 'C')]}}


class ColdStorageInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('CST', 'CST')]}}


class VehicleInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('V', 'V')]}}


class BoatInspectionView(InspectionView):
    form_args = {'structure_type': {'choices': [('BO', 'BO')]}}


admin.add_view(BuildingInspectionView(sm.BuildingInspection, db.session, name='Building Inspections'))
admin.add_view(WaterSourceInspectionView(sm.WaterSourceInspection, db.session, name='Water Source Inspections'))
admin.add_view(WaterStorageInspectionView(sm.WaterStorageInspection, db.session, name='Water Storage Inspections'))
admin.add_view(PowerInspectionView(sm.PowerInspection, db.session, name='Power Inspections'))
admin.add_view(SanitationInspectionView(sm.SanitationInspection, db.session, name='Sanitation Inspections'))
admin.add_view(CommunicationInspectionView(sm.CommunicationInspection, db.session, name='Communication Inspections'))
admin.add_view(ColdStorageInspectionView(sm.ColdStorageInspection, db.session, name='Cold Storage Inspections'))
admin.add_view(VehicleInspectionView(sm.VehicleInspection, db.session, name='Vehicle Inspections'))
admin.add_view(BoatInspectionView(sm.BoatInspection, db.session, name='Boat Inspections'))


# =========== #
#  T Y P E S  #
# =========== #

class TypeView(MyModelView):
    """Reference tables that describe basic 'types' of different structures/services.
    These should not be changed ever in theory but items will be added. We do not
    want any to be deleted.
    """

    can_delete = False

    column_display_pk = True

    form_excluded_columns = ('designation_personel_refs', 'facilities', 'buildings', 'waterSources', 'waterStorages', 'powers', 'sanitations', 'communications', 'coldStorages', 'vehicles', 'boats')

    def __init__(self, *args, **kwargs):
        super(TypeView, self).__init__(category='Types', *args, **kwargs)


admin.add_view(TypeView(fm.DesignationRef, db.session, name='Facility Types/Criticality'))
admin.add_view(TypeView(sm.BuildingRef, db.session, name='Building Types/Criticality'))
admin.add_view(TypeView(sm.WaterSourceRef, db.session, name='Water Source Types'))
admin.add_view(TypeView(sm.WaterStorageRef, db.session, name='Water Storage Types'))
admin.add_view(TypeView(sm.PowerRef, db.session, name='Power Types'))
admin.add_view(TypeView(sm.SanitationRef, db.session, name='Sanitation Types'))
admin.add_view(TypeView(sm.CommunicationRef, db.session, name='Communication Types'))
admin.add_view(TypeView(sm.ColdStorageRef, db.session, name='Cold Storage Types'))
admin.add_view(TypeView(sm.VehicleRef, db.session, name='Vehicle Types'))
#admin.add_view(TypeView(sm.EngineTypeRef, db.session, name='Engine Types'))
#admin.add_view(TypeView(sm.GearboxTypeRef, db.session, name='Gearbox Types'))
admin.add_view(TypeView(sm.BoatRef, db.session, name='Boat Types'))
admin.add_view(TypeView(sm.RoofRef, db.session, name='Roof Types'))
admin.add_view(TypeView(fm.OwnershipRef, db.session, name='Ownership Types'))
admin.add_view(TypeView(fm.SettingRef, db.session, name='Setting Types'))
admin.add_view(TypeView(sm.ConstructionRef, db.session, name='Construction Types'))



# =========== #
#  F I L E S  #
# =========== #

class FileView(MyModelView):

    def __init__(self, *args, **kwargs):
        super(FileView, self).__init__(category='Files', *args, **kwargs)

admin.add_view(FileView(fm.FacilityPicRef, db.session, name='Facility Pictures'))
admin.add_view(FileView(fm.FacilityFileRef, db.session, name='Facility Files'))
admin.add_view(FileView(fm.FacilityOtherFileRef, db.session, name='Facility Other Files'))
admin.add_view(FileView(sm.StructureFileRef, db.session, name='Structure Files'))
admin.add_view(FileView(sm.InspectionPicRef, db.session, name='Inspection Pictures'))



# =================== #
#  L O C A T I O N S  #
# =================== #

class LocationView(MyModelView):

    column_default_sort = ('id')

    column_display_pk = True
    can_delete = False

    def __init__(self, *args, **kwargs):
        super(LocationView, self).__init__(category='Locations', *args, **kwargs)

class ProvinceView(LocationView):

    form_excluded_columns = ['facilities', 'areaCouncils', 'islands', 'healthZones']

class IslandView(LocationView):

    form_excluded_columns = ['facilities', 'villages']

class AreaCouncilView(LocationView):

    form_excluded_columns = ['facilities', 'healthZones', 'villages']

class HealthZoneView(LocationView):

    column_exclude_list = ['areaCouncil']
    form_excluded_columns = ['facilities', 'areaCouncil']

admin.add_view(ProvinceView(fm.ProvinceInfo, db.session, name='Provinces'))
admin.add_view(IslandView(fm.IslandInfo, db.session, name='Islands'))
admin.add_view(AreaCouncilView(fm.AreaCouncilInfo, db.session, name='Area Councils'))
admin.add_view(LocationView(fm.VillageInfo, db.session, name='Villages'))
admin.add_view(HealthZoneView(fm.HealthZoneInfo, db.session, name='Health Zones'))



# =================== #
#  P E R S O N N E L  #
# =================== #

class PersonnelView(MyModelView):
    """Create a parent class for personnel so that all views will belong to the same category"""

    def __init__(self, *args, **kwargs):
        super(PersonnelView, self).__init__(category='Personnel', *args, **kwargs)


class PositionView(PersonnelView):

    column_labels = {'pn': 'PN',
                     'pcv': 'PCV',
                     'costCenterCode': 'Cost Centre Code',
                     'costCenterName': 'Cost Centre Name',
                     'jobCode': 'Job Code',
                     'salaryGrade': 'Salary Grade',
                     'title': 'Position Title'}
    column_descriptions = {'pn': 'The unique Position Number',
                           'pcv': 'Permanent / Contract / Vacant'}

    column_list = ['pn',
                   'title',
                   'jobCode',
                   'facility',
                   'personnel',
                   'pcv',
                   'department',
                   'fund',
                   'costCenterCode',
                   'costCenterName',
                   'activity',
                   'salaryGrade']

    column_filters = ['pn', 'title', 'jobCode', 'facility.province.name', 'facility.island.name', 'personnel.vnpfNumber', 'pcv', 'department', 'fund', 'activity', 'costCenterCode', 'costCenterName', 'salaryGrade']
    form_overrides = {'pcv': SelectField}
    form_args = {'pcv': {'choices': [('P','P'),('C','C'),('V','V')]}}

    def __init__(self, *args, **kwargs):
        super(PositionView, self).__init__(*args, **kwargs)

class CadreView(PersonnelView):
    
    column_default_sort = 'type'
    column_list = ['type', 'parent', 'children']

    column_descriptions = {'children': 'Other cadre that are a subcategory of this cadre',
                            'parent': 'The one cadre that is the greater defining cadre of this cadre',
                            'titles': 'The position titles that are defined by this cadre and not a child cadre',
                            'type': 'The name of this cadre'}

    # Define the order of the form inputs
    form_columns = ['type', 'titles', 'parent', 'children']

    def __init__(self, *args, **kwargs):
        super(CadreView, self).__init__(*args, **kwargs)

class TitleView(PersonnelView):
    
    column_filters = ['type', 'cadre.type']

    column_labels = {'cadre.type': 'Cadre',
                     'type': 'Position Title'}
    column_descriptions = {'cadre': 'The one cadre that defines this position title',
                           'clinical': 'Marks the position title as a clinical position or not'}

    form_excluded_columns = ['positions']

    def __init__(self, *args, **kwargs):
        super(TitleView, self).__init__(*args, **kwargs)

admin.add_view(PersonnelView(pm.PersonnelInfo, db.session, name='Personnel'))
admin.add_view(CadreView(pm.CadreRef, db.session, name='Cadre'))
admin.add_view(TitleView(pm.TitleRef, db.session, name='Titles'))
admin.add_view(PositionView(pm.PositionInfo, db.session, name='Positions'))



# ============= #
#  A S S E T S  #
# ============= #

class AssetView(MyModelView):
    """This is the parent class for all Asset views created so that they
    all will belong to the same category on the Admin Panel."""

    column_labels = {'functionalArea': 'Functional Area'}

    def __init__(self, *args, **kwargs):
        super(AssetView, self).__init__(category='Assets', *args, **kwargs)

class AssetCategoryView(AssetView):

    column_default_sort = 'categoryID'
    column_list = ['categoryID', 'type', 'parent', 'children']

    column_labels = {'type': 'Asset Category'}
    column_descriptions = {'type': 'The name of this asset category',
                           'parent': 'The asset category one step higher on the heirarchy than this asset category',
                           'children': 'The asset categories that are direct sub-types of this asset category'}

    form_excluded_columns = ['category_assets']

    column_filters = ['type']

    column_display_pk = True

class FunctionalAreaView(AssetView):

    form_excluded_columns = ['location']

    column_labels = {'type': 'Functional Area'}
    column_descriptions = {'type': 'The name of a distinct functional area'}

class DefaultFunctionalAreaRequirementView(AssetView):
    
    can_edit = False  # Don't want to deal with editing data then propogating the changes to all facilities. Easier to force delete and recreate

    def after_model_change(self, form, model, is_created):
        designationID = model.designationID
        functionalAreaID = model.functionalAreaID
        area = model.area
        facility_count = 0
        designation_functional_areas = [fa.functionalAreaID for fa in am.DefaultFunctionalAreaRequirementRef.query.filter_by(designationID=designationID).all()]
        for facility in fm.FacilityInfo.query.filter_by(designationID=designationID).all():                                             # For each facility
            if functionalAreaID not in facility.required_functional_areas:                                                              #   If the functional area is not in the facility
                am.FunctionalAreaRequirementsRef.create(facilityID=facility.facilityID, functionalAreaID=functionalAreaID, area=area)   #       Add it to the facility's required functional areas 
                facility_count += 1
        flash('{0} facilities have had their required functional areas updated'.format(facility_count), 'success')

    def after_model_delete(self, model):
        designationID = model.designationID
        functionalAreaID = model.functionalAreaID
        facility_count = 0
        removed_count = 0
        for facility in fm.FacilityInfo.query.filter_by(designationID=designationID).all():
            facility_count += 1
            for fa in facility.required_functional_areas:
                if fa.functionalAreaID == functionalAreaID:
                    removed_count += 1
                    am.FunctionalAreaRequirementsRef.delete(fa)
        flash('The functional area requirement was removed from {0} facilities'.format(removed_count), 'success')

class FunctionalAreaLocationRef(AssetView):

    form_excluded_columns = ['assets']

    column_filters = ['structure.facility.facilityID']

    column_labels = {'structure.facility.facilityID': 'Facility ID'}

class AssetRequirementInfoView(AssetView):

    column_filters = ['designation', 'functionalArea', 'category']

    

admin.add_view(AssetCategoryView(am.AssetCategoryInfo, db.session, name='Categories'))
admin.add_view(AssetView(am.AssetInfo, db.session, name='Assets'))
admin.add_view(FunctionalAreaView(am.FunctionalAreaRef, db.session, name='Functional Areas'))
admin.add_view(DefaultFunctionalAreaRequirementView(am.DefaultFunctionalAreaRequirementRef, db.session, name='Default Functional Area Requirements'))
admin.add_view(AssetView(am.FunctionalAreaRequirementsRef, db.session, name='Functional Area Requirements'))
admin.add_view(FunctionalAreaLocationRef(am.FunctionalAreaLocationRef, db.session, name='Asset Locations'))
admin.add_view(AssetRequirementInfoView(am.AssetRequirementInfo, db.session, name='Asset Requirements'))


# =========== #
#  R E D I S  #
# =========== #
# Causes a CSRF error for some reason so skip for now
# admin.add_view(rediscli.RedisCli(Redis()))


# ========= #
#  U S E R  #
# ========= #


class UserView(MyModelView):

    column_default_sort = ('id')

    column_list = ['email', 'first_name', 'last_name', 'confirmed']

    # New password code from https://stackoverflow.com/questions/39185230/flask-admin-overrides-password-when-user-model-is-changed
    form_excluded_columns = ['password', 'is_anonymous', 'is_new', 'confirmed_on', 'active', 'is_admin', 'notes', 'roles', 'created_at', 'username']

    form_extra_fields = {'password2': PasswordField('New password')}

    def on_model_change(self, form, User, is_created):
        if form.password2.data is not None:
            User.set_password(form.password2.data)        

    def __init__(self, *args, **kwargs):
        super(UserView, self).__init__(*args, **kwargs)

admin.add_view(UserView(um.User, db.session, name='users', endpoint='users'))