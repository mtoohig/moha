# -*- coding: utf-8 -*-
"""Application assets."""
from flask_assets import Bundle, Environment

# CSS and JS, local to the developed app

css = Bundle(
    'libs/font-awesome4/css/font-awesome.css',
    'libs/bootstrap/dist/css/bootstrap.min.css',
    'bb/bootflat/css/bootflat.css',
    'css/bootstrap-jasny.min.css',
    'css/bootstrap-toggle.min.css',
    'css/bootstrap-slider.css',
    'css/datatables.min.css',
    'css/blueimp-gallery.min.css',
    'css/animate.css',
    'css/style.css',
    filters='cssmin',
    output='public/css/common.css'
)

js_head = Bundle(
    'js/jquery3-2-1.min.js',
    filters='jsmin',
    output='public/js/js_head.js'
)

js = Bundle(
    'libs/bootstrap/dist/js/bootstrap.js',
    'js/bootstrap-tooltip.js',
    'js/bootstrap-notify.min.js',
    'js/bootstrap-jasny.min.js',
    'js/bootstrap-confirmation.js',
    'js/bootstrap-toggle.min.js',
    'js/bootstrap-slider.js',
    'js/bootstrap-flat.js',
    'js/datatables.min.js',
    'js/blueimp-gallery.min.js',
    'js/pygal-tooltips.min.js',
    'js/plugins.js',
    filters='jsmin',
    output='public/js/common.js'
)

# Leaflet Assets
css_leaflet = Bundle(
    'css/leaflet.css',
    'css/leaflet-awesome-markers.css',
    'css/leaflet-mousePosition.css',
    'css/leaflet-measure.css',
    'css/leaflet-fullscreen.css',
    #'css/leaflet-control-layers.css',
    'css/leaflet-sidebar.css',
    #filters='cssmin',
    #output='public/css/common_leaflet.css'
)

js_leaflet = Bundle(
    'js/leaflet.js',
    'js/leaflet-awesome-markers.js',
    'js/leaflet-mousePosition.js',
    'js/leaflet-measure.js',
    'js/leaflet-fullscreen.js',
    #'js/leaflet-control-layers.js',
    'js/leaflet-spiderfy.js',
    'js/leaflet-sidebar.js',
    'js/oms.js',
    'js/list.js',
    #filters='jsmin',
    #output='public/js/common_leaflet.js'
)

# Sigma Assets
js_sigma = Bundle(
    'js/sigma.js/src/sigma.core.js',
    'js/sigma.js/src/conrad.js',
    'js/sigma.js/src/utils/sigma.utils.js',
    'js/sigma.js/src/utils/sigma.polyfills.js',
    'js/sigma.js/src/sigma.settings.js',
    'js/sigma.js/src/classes/sigma.classes.dispatcher.js',
    'js/sigma.js/src/classes/sigma.classes.configurable.js',
    'js/sigma.js/src/classes/sigma.classes.graph.js',
    'js/sigma.js/src/classes/sigma.classes.camera.js',
    'js/sigma.js/src/classes/sigma.classes.quad.js',
    'js/sigma.js/src/classes/sigma.classes.edgequad.js',
    'js/sigma.js/src/captors/sigma.captors.mouse.js',
    'js/sigma.js/src/captors/sigma.captors.touch.js',
    'js/sigma.js/src/renderers/sigma.renderers.canvas.js',
    'js/sigma.js/src/renderers/sigma.renderers.webgl.js',
    'js/sigma.js/src/renderers/sigma.renderers.svg.js',
    'js/sigma.js/src/renderers/sigma.renderers.def.js',
    'js/sigma.js/src/renderers/webgl/sigma.webgl.nodes.def.js',
    'js/sigma.js/src/renderers/webgl/sigma.webgl.nodes.fast.js',
    'js/sigma.js/src/renderers/webgl/sigma.webgl.edges.def.js',
    'js/sigma.js/src/renderers/webgl/sigma.webgl.edges.fast.js',
    'js/sigma.js/src/renderers/webgl/sigma.webgl.edges.arrow.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.labels.def.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.hovers.def.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.nodes.def.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edges.def.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edges.curve.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edges.arrow.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edgehovers.def.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edgehovers.curve.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edgehovers.arrow.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js',
    'js/sigma.js/src/renderers/canvas/sigma.canvas.extremities.def.js',
    'js/sigma.js/src/renderers/svg/sigma.svg.utils.js',
    'js/sigma.js/src/renderers/svg/sigma.svg.nodes.def.js',
    'js/sigma.js/src/renderers/svg/sigma.svg.edges.def.js',
    'js/sigma.js/src/renderers/svg/sigma.svg.edges.curve.js',
    'js/sigma.js/src/renderers/svg/sigma.svg.labels.def.js',
    'js/sigma.js/src/renderers/svg/sigma.svg.hovers.def.js',
    'js/sigma.js/src/middlewares/sigma.middlewares.rescale.js',
    'js/sigma.js/src/middlewares/sigma.middlewares.copy.js',
    'js/sigma.js/src/misc/sigma.misc.animation.js',
    'js/sigma.js/src/misc/sigma.misc.bindEvents.js',
    'js/sigma.js/src/misc/sigma.misc.bindDOMEvents.js',
    'js/sigma.js/src/misc/sigma.misc.drawHovers.js',
    'js/sigma.js/plugins/sigma.layout.forceAtlas2/worker.js',
    'js/sigma.js/plugins/sigma.layout.forceAtlas2/supervisor.js',
    'js/sigma.js/plugins/sigma.plugins.neighborhoods/sigma.plugins.neighborhoods.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edges.dashed.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edges.dotted.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edges.parallel.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edges.tapered.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edgehovers.dashed.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edgehovers.dotted.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edgehovers.parallel.js',
    'js/sigma.js/plugins/sigma.renderers.customEdgeShapes/sigma.canvas.edgehovers.tapered.js',
    'js/sigma.js/plugins/sigma.renderers.customShapes/shape-library.js',
    'js/sigma.js/plugins/sigma.renderers.customShapes/sigma.renderers.customShapes.js',
    filters='jsmin',
    output='public/js/common_sigma.js'
)


assets = Environment()
assets.register("css", css)
assets.register("js_head", js_head)
assets.register("js", js)
assets.register("css_leaflet", css_leaflet)
assets.register("js_leaflet", js_leaflet)
assets.register("js_sigma", js_sigma)