# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from flask import flash, flash, send_from_directory, request
from flask import current_app as app

import os
import uuid
import base64
import datetime
from PIL import Image
from werkzeug.utils import secure_filename

import zipfile

import structlog
logger = structlog.getLogger(__name__)

def flash_errors(form, category='warning'):
	"""Flash all errors for a form."""
	for field, errors in form.errors.items():
		for error in errors:
			flash('{0} - {1}'.format(getattr(form, field).label.text, error), category)


from pygal.style import Style
def pygal_style():
	return Style(background='transparent',
					plot_background='transparent',
					foreground='#53E89B',
					foreground_strong='#53A0E8',
					foreground_subtle='#630C0D',
					opacity='.6',
					opacity_hover='.9',
					transition='400ms ease-in',
					colors=('#E853A0', '#E8537A', '#E95355', '#E87653', '#E89B53')
				)

	
# =========== #
#  F I L E S  #
# =========== #

def allowed_picture(filename):
	return '.' in filename and filename.rsplit('.',1)[1].lower() in app.config['ALLOWED_PICTURE_EXTENSIONS']


def allowed_file(filename):
	return '.' in filename and filename.rsplit('.',1)[1].lower() in app.config['ALLOWED_FILE_EXTENSIONS']


def get_a_uuid():
	r_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
	return r_uuid.replace('=', '')
						

def make_secure_filename(filename):
	return secure_filename(filename)


def check_directory(save_dir):
	if not os.path.exists(save_dir):
		os.makedirs(save_dir)
		logger.debug('New directory: "{0}"'.format(save_dir))


def make_thumbnail(save_dir, file, fileName):
	# Pass to PIL to make a thumbnail
	file = Image.open(file)
	file.thumbnail((200,200), Image.ANTIALIAS)
	file.save(os.path.join(save_dir, fileName))


def add_file(file, form, save_dir, facilityID, file_table):
	logger.debug('Enter add file', file=file, form=form, save_dir=save_dir, facilityID=facilityID, file_table=file_table)
	fileType = file.filename.rsplit('.', 1)[1]
	fileName = get_a_uuid() + "." + fileType
	fileOriginalName = make_secure_filename(file.filename)
	fileDesc = form.description.data
	fileDateCreated = form.dateCreated.data
	fileDateUploaded = datetime.datetime.now()
	file.seek(0)  # Return the read position of the file to the beginning 
	# Save file to disk
	directory = os.path.join(save_dir, facilityID)
	check_directory(directory)
	file.save(os.path.join(directory, fileName))
	logger.debug('filename "{0}" saved to "{1}"'.format(fileName, directory))
	fileSize = os.stat( os.path.join(directory, fileName) ).st_size
	# Save thumbnail
	if allowed_picture(file.filename):
		thumbs = os.path.join(save_dir, facilityID, 'thumbs')
		check_directory(thumbs)
		make_thumbnail(thumbs, file, fileName)
	# Add file to database
	newFile = file_table.create(fileType=fileType,
								fileName=fileName,
								fileOriginalName=fileOriginalName,
								fileDesc=fileDesc,
								fileDateCreated=fileDateCreated,
								fileDateUploaded=fileDateUploaded,
								fileSize=fileSize)
	logger.info('Created fileID: {0}'.format(newFile.id), file=newFile, action='create')
	return newFile


def download_multiple(facility, files, file_type='all', file_name='files.zip'):
	logger.debug('Enter download multiple files', file_type=file_type)
	directory = os.path.join(app.config['TEMP_FOLDER'], facility.facilityID)
	check_directory(directory)
	zipp = zipfile.ZipFile(os.path.join(directory, '{0}.zip'.format(facility.identifier)), 'w')
	for file in files:
		if file_type=='picture':
			try:
				zipp.write(os.path.join(app.config['PICTURE_FOLDER'], facility.facilityID, file.file.fileName), file.file.fileOriginalName)
			except Exception as e:
				logger.error('Failed to write picture to zip file. facilityID: {0} filename: {1}'.format(facility.facilityID, file.file.fileName))
		elif file_type=='file':
			try:
				zipp.write(os.path.join(app.config['FILE_FOLDER'], facility.facilityID, file.file.fileName), file.file.fileOriginalName)
			except Exception as e:
				logger.error('Failed to write file to zip file. facilityID: {0} filename: {1}'.format(facility.facilityID, file.file.fileName))
		elif file_type=='all':
			try:
				zipp.write(os.path.join(app.config['PICTURE_FOLDER'], facility.facilityID, file.file.fileName), file.file.fileOriginalName)
				zipp.write(os.path.join(app.config['FILE_FOLDER'], facility.facilityID, file.file.fileName), file.file.fileOriginalName)
			except Exception as e:
				logger.error('Failed to write picture to zip file. facilityID: {0} filename: {1}'.format(facility.facilityID, file.file.fileName))
		else:
			logger.error('Unknown file_type given: {0}'.format(file_type))
			flash('The file type given "{0}" is malformed or unknown and your request can not be completed', 'danger')
	zipp.close()
	logger.info('Delivered zip of all {0} of facilityID: {1}'.format(file_type, facility.facilityID))
	return send_from_directory(directory, '{0}.zip'.format(facility.identifier), as_attachment=True, attachment_filename=file_name)


def delete_file(file, file_type, facilityID):
	logger.debug('Enter delete file: {0}'.format(file))
	if file_type == 'picture':
		try:
			os.remove(os.path.join(app.config['PICTURE_FOLDER'], facilityID, file))
			logger.info('Deleted file: {0}'.format(file), action='delete')
		except OSError:
			logger.warning('Failed to find file: {0}'.format(file))
		try:
			os.remove(os.path.join(app.config['PICTURE_FOLDER'], facilityID, 'thumbs', file))
			logger.info('Deleted thumb file: {0}'.format(file), action='delete')
		except OSError:
			logger.warning('Failed to find thumb file: {0}'.format(file))
	elif file_type == 'file':
		try:
			os.remove(os.path.join(app.config['FILE_FOLDER'], facilityID, file))
			logger.info('Deleted file: {0}'.format(file), action='delete')
		except OSError:
			logger.warning('Failed to find file: {0}'.format(file))
	else:
		logger.error('The file_type {0} is not recognized for deletion'.format(file_type))
		flash('The file type given "{0}" is malformed or unknown and your request can not be completed', 'danger')



# =========== #
#  O T H E R  #
# =========== #

def make_cache_key(*args, **kwargs):
	"""makes unique keys for the cache including any parameters

	source :: https://stackoverflow.com/questions/9413566/flask-cache-memoize-url-query-string-parameters-as-well
	"""
	path = request.path
	args = str(hash(frozenset(request.args.items())))
	#lang = get_locale()
	return (path + args).encode('utf-8')
