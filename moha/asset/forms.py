import datetime

from flask_wtf import Form
from wtforms import StringField, SelectField, IntegerField, DateField, BooleanField, TextAreaField, HiddenField
from wtforms.validators import DataRequired, Length, Optional

from moha.public.forms import RedirectForm


class AssetCategoryForm(Form):
	"""Asset Category Form"""

	type = StringField('Type')

	parentID = SelectField('Parent Category',
							choices=[(1, 'Bed')],
							coerce=int,
							validators=[])

	def __init__(self, *args, **kwargs):
		"""Create instance."""
		super(AssetCategoryForm, self).__init__(*args, **kwargs)
		self.AssetCategoryForm = None

	def validate(self):
		"""Validate the form."""
		initial_validation = super(AssetCategoryForm, self).validate()
		if not initial_validation:
			return False

		return True


class AssetForm(RedirectForm):
	"""Asset Form"""

	categoryID = SelectField('Asset Type',
							choices=[],
							coerce=int,
							validators=[DataRequired()])

	status = BooleanField('Status')

	note = TextAreaField('Note', validators=[Length(max=500, message="May not exceed 500 characters")])

	manufacturer = StringField('Manufacturer')
	model = StringField('Model')
	supplier = StringField('Supplier')

	height = IntegerField('Height', validators=[Optional()])
	width = IntegerField('Width', validators=[Optional()])
	depth = IntegerField('Depth', validators=[Optional()])
	units = StringField('Unit of Measure')

	datePurchased = DateField('Purchased Date', format="%d/%m/%Y")
	cost = IntegerField('Cost', validators=[Optional()])

	BMENum = StringField('BME Number')
	serialNum = StringField('Serial Number')
	vNum = StringField('V# (reference)')

	locationID = SelectField('Location',
							choices=[],
							coerce=int,
							validators=[DataRequired()])

	def validate(self):
		"""Validate the form."""
		initial_validation = super(AssetForm, self).validate()
		if not initial_validation:
			return False

		if not self.height.data and not self.width.data and not self.depth.data:
			self.units.data = ''

		if self.height.data or self.width.data or self.depth.data:
			if self.units.data == '':
				self.units.errors.append('Required if you define any of the dimensions')
				return False

		return True


class AssetSearchForm(Form):
	"""Search Personnel List."""

	vNum = StringField('V#')

	categoryID = SelectField('Category', choices=[], coerce=int)

	status = SelectField('Status', choices=[(-1,'all'),(1,'Operational'),(0,'Non-Op')], coerce=int)

	facilityID = StringField('Facility ID')

	def __init__(self, *args, **kwargs):
		"""Create instance."""
		super(AssetSearchForm, self).__init__(*args, **kwargs)
		self.AssetSearchForm = None

	def validate(self):
		"""Validate the form."""
		initial_validation = super(AssetSearchForm, self).validate()
		if not initial_validation:
			return False

		return True


class LocationForm(RedirectForm):
	"""Functional Area Location Form"""

	structureID = SelectField('Structure',
							choices=[],
							coerce=int,
							validators=[])

	functionalAreaID = SelectField('Asset Location',
							choices=[],
							coerce=int,
							validators=[])

	def validate(self):
		"""Validate the form."""
		initial_validation = super(LocationForm, self).validate()
		if not initial_validation:
			return False

		return True


class TransferForm(RedirectForm):
	"""Transfer Location to new structure"""

	structureID = SelectField('Structure',
							choices=[],
							coerce=int,
							validators=[])

	def validate(self):
		"""Validate the form."""
		initial_validation = super(TransferForm, self).validate()
		if not initial_validation:
			return False

		return True