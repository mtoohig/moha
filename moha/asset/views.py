# -*- coding: utf-8 -*-
"""Asset views."""

from flask import Blueprint, render_template, request, flash, url_for, redirect, send_from_directory, jsonify

import forms
import models as am
from moha.facility import models as fm
from moha.structure import models as sm

from moha import utils
import moha.cache_utils as cacheDel
from moha.extensions import login_required, cache
from flask_paginate import Pagination, get_page_parameter, get_per_page_parameter
from sqlalchemy import or_

from flask import current_app as app

import structlog
logger = structlog.getLogger(__name__)

blueprint = Blueprint('asset', __name__, url_prefix='/asset', static_folder='../static')


# ============= # 
#  C H A R T S  #
# ============= #
@blueprint.route('/charts')
def _charts():
	return []

# ============= #
#  A S S E T S  #
# ============= #

#  S E A R C H 
#------------
@blueprint.route('/search')
def search():
	logger.debug('Enter assets search')
	form = forms.AssetSearchForm()
	# Create the base query for the facilities
	query = am.AssetInfo.query.order_by(am.AssetInfo.id)
	# Create base queries for different select inputs of search page
	category_query = am.AssetCategoryInfo.query.order_by(am.AssetCategoryInfo.type)
	# Collect the argumetns from the Form's GET submit and set defaults
	try:
		vNum = request.args.get('vNum')
		form.vNum.default = vNum
		if vNum:
			query = query.filter(am.AssetInfo.vNum.contains(vNum))
	except TypeError:
		pass
	try:
		# Set defaults on form so that between page refreshes the selected variables remain
		categoryID = int(request.args.get('categoryID'))
		form.categoryID.default = categoryID
		if categoryID >= 0:
			category = am.AssetCategoryInfo.query.filter_by(categoryID=categoryID).first()
			query = query.filter(am.AssetInfo.id.in_(category.asset_ids))  # checks for all assets that are of the searched category or one of the child categories  # TODO maybe faster with category_ids
	except TypeError:
		pass  # user has not yet filtered so above int() fails
	try:
		status = int(request.args.get('status'))
		form.status.default = status
		if status >= 0:
			status = True if status == 1 else False
			query = query.filter_by(status=status)
	except TypeError:
		pass
	try:
		facilityID = request.args.get('facilityID')
		form.facilityID.default = facilityID
		if facilityID:
			# Searches for matches in both facilityID and facilityName
			query = query.join(am.FunctionalAreaLocationRef).join(sm.StructureInfo).join(fm.FacilityInfo).filter(or_(fm.FacilityInfo.facilityID.contains(facilityID), fm.FacilityInfo.facilityName.contains(facilityID)))
	except TypeError:
		pass

	# Finalize the queries and add an 'all' option to the SelectFields in the form
	form.categoryID.choices = [(-1,'all')] + [(t.categoryID, t.type.strip()) for t in category_query.all()]
	form.process()  # We have to call this to 're-build' the form with our new choices and defaults selected

	# Set up pagination
	page = request.args.get(get_page_parameter(), type=int, default=1)
	per_page = request.args.get(get_per_page_parameter(), type=int, default=10)
	pagination = Pagination(page=page, per_page=per_page, total=query.count(), search=False, record_name='assets', bs_version=3)
	assets = query.limit(per_page).offset((pagination.page - 1) * per_page)
	return render_template('assets/index.html', form=form, assets=assets, pagination=pagination)

#  V I E W
#--------
@blueprint.route('/<int:assetID>')
@login_required
def view_asset(assetID):
	asset = am.AssetInfo.query.filter_by(id=assetID).first_or_404()
	return render_template('assets/asset.html', asset=asset)

def _facility_locations(facilityID):
	return [(location.id, location) for location in am.FunctionalAreaLocationRef.query.filter(am.FunctionalAreaLocationRef.structureID.in_([s.id for s in sm.StructureInfo.query.filter_by(facilityID=facilityID).all()])).all()]

#  A D D
#------
@blueprint.route('/add', methods=['GET', 'POST'])
@login_required
def add_asset():
	"""Adds an Asset."""
	logger.debug('Enter add asset')
	form = forms.AssetForm()
	form.categoryID.choices = [(c.categoryID, c.type) for c in am.AssetCategoryInfo.query.order_by(am.AssetCategoryInfo.type).all()]
	# Collect two variables which may be in the request object
	facilityID = request.args.get('facilityID')
	locationID = request.args.get('locationID')
	if locationID:
		# If locationID exists it means we already know where the asset is to be assigned
		# We can remove the locationID field from the form so that the user can not select a different location
		del form.locationID  # we will take this locationID and plug it into the database directly
	elif facilityID:
		# If facilityID exists it means we know that the user will select from a location available at this facilityID
		# This single line gathers the locationID of all locations available in this facilityID
		form.locationID.choices = _facility_locations(facilityID)
	else:
		logger.error('Missing variables from request')
		flash('Some how you found an error, if you see this message again please try a performing a different task', 'info')
		pass  # catch this

	if form.validate_on_submit():
		logger.debug('Form validated')
		# If locationID is not defined from the request then locationID will come from the form
		if not locationID:
			locationID = form.locationID.data
		newAsset = am.AssetInfo.create(categoryID=form.categoryID.data,
							status=form.status.data,
							note=form.note.data,
							manufacturer=form.manufacturer.data,
							model=form.model.data,
							supplier=form.supplier.data,
							height=form.height.data,
							width=form.width.data,
							depth=form.depth.data,
							units=form.units.data,
							datePurchased=form.datePurchased.data,
							cost=form.cost.data,
							BMENum=form.BMENum.data,
							serialNum=form.serialNum.data,
							vNum=form.vNum.data,
							locationID=locationID)
		logger.info('Created assetID: {0}'.format(newAsset.id), asset=newAsset, action='create')
		cacheDel.force_update_asset_cache()
		flash('Asset Added', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('form.html', form=form, title='Add Asset')

#  U P D A T E
#------------
@blueprint.route('/<int:assetID>/update', methods=['GET', 'POST'])
@login_required
def update_asset(assetID):
	"""Updates an asset."""
	logger.debug('Enter update assetID: {0}'.format(assetID))
	asset = am.AssetInfo.query.filter_by(id=assetID).first_or_404()
	form = forms.AssetForm(obj=asset)
	form.categoryID.choices = [(c.categoryID, c.type) for c in am.AssetCategoryInfo.query.order_by(am.AssetCategoryInfo.type).all()]
	form.locationID.choices = _facility_locations(asset.location.structure.facility.facilityID)
	if form.validate_on_submit():
		logger.debug('Form validated with assetID: {0}'.format(assetID))
		am.AssetInfo.update(asset,
							categoryID=form.categoryID.data,
							status=form.status.data,
							note=form.note.data,
							manufacturer=form.manufacturer.data,
							model=form.model.data,
							supplier=form.supplier.data,
							height=form.height.data,
							width=form.width.data,
							depth=form.depth.data,
							units=form.units.data,
							datePurchased=form.datePurchased.data,
							cost=form.cost.data,
							BMENum=form.BMENum.data,
							serialNum=form.serialNum.data,
							vNum=form.vNum.data,
							locationID=form.locationID.data)
		logger.info('Updated assetID: {0}'.format(assetID), asset=asset, action='update')
		cacheDel.force_update_asset_cache()
		flash('Asset Updated', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('form.html', form=form, title='Update Asset')

#  D E L E T E 
#------------
@blueprint.route('/<int:assetID>/delete')
@login_required
def delete_asset(assetID):
	"""Removes an asset from the database."""
	logger.debug('Enter delete assetID: {0}'.format(assetID))
	form = forms.AssetForm()
	asset = am.AssetInfo.query.filter_by(id=assetID).first_or_404()
	am.AssetInfo.delete(asset)
	logger.info('Removed assetID: {0}'.format(assetID), asset=asset, action='delete')
	cacheDel.force_update_asset_cache()
	flash('Asset Deleted', 'success')
	return form.redirect(request.args.get('next'))
		
# ================= #
#  L O C A T I O N  #
# ================= #

#  A D D 
#------
@blueprint.route('/location/add', methods=['GET', 'POST'])
@login_required
def add_location():
	"""Add a functional area to a structure."""
	logger.debug('Enter add location')
	facilityID = request.args.get('facilityID')
	structureID = request.args.get('structureID')
	form = forms.LocationForm()
	form.functionalAreaID.choices = [(fa.id, fa.identifier) for fa in am.FunctionalAreaRef.query.all()]
	if structureID:
		del form.structureID
	else:
		form.structureID.choices = [(s.id, s.identifier) for s in sm.StructureInfo.query.filter_by(facilityID=facilityID).all()]
	if form.validate_on_submit():
		logger.debug('Form validated')
		if not structureID:
			structureID = form.structureID.data
		location = am.FunctionalAreaLocationRef.create(structureID=structureID, functionalAreaID=form.functionalAreaID.data)
		logger.info('Created locationID: {0}'.format(location.id), location=location, action='create')
		cacheDel.force_update_asset_cache()
		flash('Asset location added', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('form.html', form=form, title='Add Asset Location')


#  U P D A T E
#------------
@blueprint.route('/location/<int:locationID>/update', methods=['GET', 'POST'])
@login_required
def update_location(locationID):
	logger.debug('Enter update locationID: {0}'.format(locationID))
	facilityID = request.args.get('facilityID')
	location = am.FunctionalAreaLocationRef.query.filter_by(id=locationID).one()
	form = forms.TransferForm()
	form.structureID.choices = [(s.id, s.identifier) for s in sm.StructureInfo.query.filter_by(facilityID=facilityID).all()]
	if form.validate_on_submit():
		logger.debug('Form validated')
		am.FunctionalAreaLocationRef.update(location, structureID=form.structureID.data)
		logger.info('Updated locationID: {0}'.format(locationID), location=location, action='update')
		cacheDel.force_update_asset_cache()
		flash('Assets moved', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('form.html', form=form, title='Move {0} to...'.format(location.identifier))



@blueprint.route('/get_area')
def area():
	facilityID = request.args.get('facilityID')
	functionalAreaID = request.args.get('functionalAreaID')
	required_area = am.FunctionalAreaRequirementsRef.query.filter_by(facilityID=facilityID, functionalAreaID=functionalAreaID).first_or_404()
	return jsonify({'data': required_area.area})


#  D E L E T E
#------------
# Actually thing making deleting available outside of the Admin panel a bad idea.
# If it is deleted should assets be removed seperately?