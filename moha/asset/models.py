# -*- coding: utf-8 -*-
"""Building models."""
import datetime
import itertools

from moha.database import CRUDMixin, Column, Model, SurrogatePK, db, reference_col, relationship, hybrid_property
from sqlalchemy.orm.collections import attribute_mapped_collection




class AssetCategoryInfo(Model):
	__tablename__ = 'assetCategoryInfo'
	"""Asset category heirarchy

	https://bitbucket.org/zzzeek/sqlalchemy/src/master//examples/adjacency_list/adjacency_list.py?fileviewer=file-view-default

	attribute_mapped_collection is not used because it breaks flask-admin"""

	categoryID = Column(db.Integer, primary_key=True)
	parentID = Column(db.Integer, db.ForeignKey('assetCategoryInfo.categoryID'), nullable=True)
	type = Column(db.Unicode, nullable=False)
	
	children = relationship('AssetCategoryInfo', 
							cascade='all, delete-orphan', 
							backref=db.backref('parent', remote_side=[categoryID]))

	category_assets = relationship('AssetInfo')  # this defines assets that exactly match the category

	def _recursive_assets(self):
		return [[c._recursive_assets(), c.category_assets] for c in self.children]

	@hybrid_property
	def assets(self):
		"""All assets that match this category or a sub-categoy"""
		return self._flatten(self._recursive_assets() + [self.category_assets])

	@hybrid_property
	def asset_ids(self):
		return [a.id for a in self.assets]

	def __unicode__(self):
		return '{0}'.format(self.type)

	def __repr__(self):
		return '<AssetCategoryInfo(type="{0}", categoryID={1}, parentID={2}>'.format(self.type, self.categoryID, self.parentID)

	def _flatten(self, S):
		"""Flatten the recursive lists of categoryIDs
		source :: https://stackoverflow.com/questions/12472338/flattening-a-list-recursively
		"""
		if S == []:
			return S
		if isinstance(S[0], list):
			return self._flatten(S[0]) + self._flatten(S[1:])
		return S[:1] + self._flatten(S[1:])

	def _recursive_category_ids(self):
		"""Returns a list of lists containing all children and their children's categoryIDs"""
		return [[c._recursive_category_ids(), c.categoryID] for c in self.children]

	@hybrid_property
	def category_ids(self):
		"""Returns flat list of all categoryIDs that are below this category on the tree"""
		return self._flatten(self._recursive_category_ids()) + [self.categoryID]

	def dump(self, _indent=0):
		return "    " * _indent + repr(self) + \
			"\n" + \
			"".join([
				c.dump(_indent + 1)
				for c in self.children
			])


class AssetInfo(Model, SurrogatePK):
	"""An Asset of category and location"""
	__tablename__ = 'assetInfo'

	categoryID = reference_col('assetCategoryInfo', pk_name='categoryID')
	category = relationship('AssetCategoryInfo', back_populates='category_assets')

	status = Column(db.Boolean, nullable=False, default=True)
	note = Column(db.Unicode(500))

	manufacturer = Column(db.Unicode)
	model = Column(db.Unicode)
	supplier = Column(db.Unicode)

	height = Column(db.Integer)
	width = Column(db.Integer)
	depth = Column(db.Integer)
	units = Column(db.Unicode)
	
	datePurchased = Column(db.Date, nullable=False, default=datetime.date.today) 
	cost = Column(db.Integer)

	BMENum = Column(db.Unicode)
	serialNum = Column(db.Unicode)
	vNum = Column(db.Unicode)

	locationID = reference_col('functionalAreaLocationRef')
	location = relationship('FunctionalAreaLocationRef', back_populates='assets')

	@hybrid_property
	def dimensions(self):
		h = self.height if self.height else 0
		w = self.width if self.width else 0
		d = self.depth if self.depth else 0
		return '{0}{3} x {1}{3} x {2}{3}'.format(h, w, d, self.units if self.units else '')

	def __unicode__(self):
		return '{0} at {1}'.format(self.category.type, self.location.functionalArea.type)

	def __repr__(self):
		return '<AssetInfo(categoryID={0}, locationID={1}, status={2}>'.format(self.categoryID, self.locationID, self.status)

	def as_dict(self, categoryID=None):
		"""Produces the dictionary that is turned into JSON.
		Used to make markers for maps.
		"""
		data = {c.name: getattr(self, c.name) for c in self.__table__.columns}
		return data


class FunctionalAreaRef(Model, SurrogatePK):
	"""A functional Area"""
	__tablename__ = 'functionalAreaRef'

	type = Column(db.Unicode, nullable=False)

	@hybrid_property
	def identifier(self):
		return self.type

	def __unicode__(self):
		return self.type

	def __repr__(self):
		return '<FunctionalAreaRef(id={0}, type={1})>'.format(self.id, self.type)


class DefaultFunctionalAreaRequirementRef(Model, SurrogatePK):
	"""A default functionalArea of a designation.

	Whereas FunctionalArearequirementsRef allows for special case facilities have
	less or more functional area requirements than other facilities of the same designation"""
	__tablename__ = 'defaultFunctionalAreaRequirementRef'

	designationID = reference_col('designationRef')
	designation = relationship('DesignationRef', backref='default_functional_areas')

	functionalAreaID = reference_col('functionalAreaRef')
	functionalArea = relationship('FunctionalAreaRef')

	area = Column(db.Float)


class FunctionalAreaRequirementsRef(Model, SurrogatePK):
	"""A facility requirement for a functional area.

	The application logic will have to cross check this to be sure it is enforced."""
	__tablename__ = 'functionalAreaRequirementsRef'

	facilityID = reference_col('facilityInfo', pk_name='facilityID')
	facility = relationship('FacilityInfo', back_populates='required_functional_areas')

	functionalAreaID = reference_col('functionalAreaRef')
	functionalArea = relationship('FunctionalAreaRef')

	area = Column(db.Float)


class FunctionalAreaLocationRef(Model, SurrogatePK):
	"""A location.
	
	location == one structure + one functional area

	Assets are kept in these locations."""
	__tablename__ = 'functionalAreaLocationRef'

	structureID = reference_col('structureInfo', nullable=True)
	structure = relationship('StructureInfo', back_populates='functional_areas')
	
	functionalAreaID = reference_col('functionalAreaRef', nullable=True)
	functionalArea = relationship('FunctionalAreaRef', backref=db.backref('location', uselist=False))

	assets = relationship('AssetInfo', primaryjoin='FunctionalAreaLocationRef.id == AssetInfo.locationID', back_populates='location')

	@hybrid_property
	def identifier(self):
		return self.functionalArea.type

	def __unicode__(self):
		return '{0} at {1}'.format(self.functionalArea.type, self.structure.identifier)


class AssetRequirementInfo(Model, SurrogatePK):
	__tablename__ = 'assetRequirementInfo'
	"""A requirement of an asset for a location."""

	designationID = reference_col('designationRef')
	designation = relationship('DesignationRef', back_populates='asset_requirements')

	functionalAreaID = reference_col('functionalAreaRef')
	functionalArea = relationship('FunctionalAreaRef')

	quantity = Column(db.Integer, nullable=False, default=1)
	note = Column(db.Unicode(500), nullable=True)

	categoryID = reference_col('assetCategoryInfo', pk_name='categoryID')
	category = relationship('AssetCategoryInfo')

	def __unicode__(self):
		return '{0} {1} required for {2}'.format(self.quanity, self.category.type, self.designation.name)

	def __repr__(self):
		return '<AssetRequirementInfo(designationID={0}, categoryID={1}, quantity={2}, note={3}, functionalAreaID={4}'.format(self.designationID, self.categoryID, self.quantity, self.note, self.functionalAreaID)




