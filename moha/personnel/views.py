# -*- coding: utf-8 -*-
"""Personel section."""
from flask import Blueprint, flash, redirect, render_template, request, url_for, send_from_directory, jsonify
from flask_login import login_required
from flask_paginate import Pagination, get_page_parameter, get_per_page_parameter
from sqlalchemy.orm.exc import NoResultFound

from . import forms
from . import models as pm
from moha.facility import models as fm

# I know this is bad but I have to import moha directly to access cache_utils.
# Importing cache_utils as I did in other views does not work here.
# It is a mystery.
# Time is not available to me so I'm doing it this way.
import moha 
from moha import utils

import pygal
import structlog
logger = structlog.getLogger(__name__)

blueprint = Blueprint('personnel', __name__, url_prefix='/personnel', static_folder='../static')


@blueprint.route('/')
def search():
	logger.debug('Enter search personnels')
	form = forms.PositionSearchForm()
	# Create the base query for the facilities
	query = pm.PositionInfo.query
	# Create base queries for different columns of search page
	title_query = pm.TitleRef.query.order_by(pm.TitleRef.id)
	facility_query = fm.FacilityInfo.query.order_by(fm.FacilityInfo.facilityID)
	province_query = fm.ProvinceInfo.query.order_by(fm.ProvinceInfo.id)
	# Collect the arguments from the Form's GET submit and set defaults
	try:
		provinceID = int(request.args.get('province'))
		form.province.default = provinceID
		if provinceID and provinceID >= 0:
			query = query.join(fm.FacilityInfo)
			query = query.filter(fm.FacilityInfo.provinceID==provinceID)
	except TypeError:
		pass
	try:
		pn = request.args.get('pn')
		form.pn.default = pn
		if pn:
			query = query.filter(pm.PositionInfo.pn.contains(pn))
	except TypeError:
		pass
	try:
		vnpf = request.args.get('vnpf')
		form.vnpf.default = vnpf
		if vnpf:
			query = query.join(pm.PersonnelInfo).filter(pm.PersonnelInfo.vnpfNumber.contains(vnpf))
	except TypeError:
		pass
	try:
		pcv = request.args.get('pcv')
		form.pcv.default = pcv
		if pcv and pcv != 'all':
			query = query.filter(pm.PositionInfo.pcv==pcv)
	except TypeError:
		pass
	try:
		# Set defaults on form so that between page refreshes the selected variables remain
		titleID = int(request.args.get('title'))
		form.title.default = titleID
		if titleID and titleID >= 0:
			query = query.filter(pm.PositionInfo.titleID==titleID)
	except TypeError:
		pass
	try:
		jobCode = request.args.get('jobCode')
		form.jobCode.default = jobCode
		if jobCode:
			query = query.filter(pm.PositionInfo.jobCode.contains(jobCode))
	except TypeError:
		pass
	try:
		facilityID = int(request.args.get('facility'))
		form.facility.default = facilityID
		if facilityID and facilityID >= 0:
			query = query.filter(pm.PositionInfo.facilityID==facilityID)
	except TypeError:
		pass
	# Finalize the queries and add an 'all' option to the SelectFields in the form
	form.title.choices = [(-1,'all')] + [(t.id, t.type.strip()) for t in title_query.all()]
	form.pcv.choices = [('all','all')] + [('P', 'P'),('C', 'C'),('V', 'V')]
	form.facility.choices = [(-1,'all')] + [(f.facilityID, f.identifier) for f in facility_query.all()]
	form.province.choices = [(-1,'all')] + [(p.id, p.name) for p in province_query.all()]
	form.process()  # We have to call this to 're-build' the form with our new choices and defaults selected
	# Make pagination 
	page = request.args.get(get_page_parameter(), type=int, default=1)
	per_page = request.args.get(get_per_page_parameter(), type=int, default=10)
	positions = query.order_by(pm.PositionInfo.id)
	pagination = Pagination(page=page, per_page=per_page, total=positions.count(), search=False, record_name='positions', bs_version=3)
	positions = positions.limit(per_page).offset((pagination.page - 1) * per_page)
	return render_template('personnels/search.html', form=form, positions=positions, pagination=pagination)#, charts=_charts())



# ============= #
#  C H A R T S  #
# ============= #

# unknown use for now but demo of radar c
def _chart_personnel_and_positions():
	titles = pm.TitleRef.query.all()
	personnel = pm.PersonnelInfo.query.all()
	chart = pygal.Radar(style=pygal.style.Style(colors=('red','blue'), opacity=.4, opacity_hover=.7))
	chart.title = 'Personnel by Title and Positions by Title'
	chart.x_labels = [title.type for title in titles]
	chart.add('Positions', [t.total_positions for t in titles], fill=True, stroke_style={'width': 5, 'dasharray': '3, 6'})
	chart.add('Personnel', [t.total_personnel for t in titles], fill=True)
	return chart.render_data_uri()


#  I N D E X
#----------
def _chart_vacancy_by_province():
	provinces = fm.ProvinceInfo.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Vacancy by Province'
	chart.x_labels = [p.name for p in provinces] + ['Unknown']
	chart.add('Filled', [p.total_positions_filled for p in provinces] + [pm.PositionInfo.query.filter_by(facilityID=None).filter(pm.PositionInfo.personnelID != None).count()])
	chart.add('Unfilled', [p.total_positions_unfilled for p in provinces] + [pm.PositionInfo.query.filter_by(facilityID=None).filter_by(personnelID=None).count()])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@blueprint.route('/data/charts')
#@cache.cached(timeout=300, key_prefix=utils.make_cache_key)
def _charts():
	"""Display charts on index view of personnel"""
	chart1 = _chart_vacancy_by_province()
	return jsonify({'charts': [chart1]})


# ========================== #
#  S P E C I A L  V I E W S  #
# ========================== #

#  C A D R E
#----------
def _chart_percent_cadre_by_province(cadre):
	titles = pm.TitleRef.query.filter_by(cadreID=cadre.cadreID).all()
	provinces = fm.ProvinceInfo.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Position Titles of {0} Cadre by Province (%)'.format(cadre.type)
	chart.x_labels = [p.name for p in provinces] + ['Unknown']
	for t in titles:
		try:
			unknown_province = (pm.PositionInfo.query.filter(pm.PositionInfo.facilityID==None).filter_by(titleID=t.id).count() / float(pm.PositionInfo.query.filter(pm.PositionInfo.titleID.in_(cadre.cadre_title_ids)).filter(pm.PositionInfo.facilityID==None).count()) )*100
		except ZeroDivisionError:
			unknown_province = 0
		known_provinces = [( t.total_by_province(p.id) / float(p.total_positions_of_cadre(cadre.cadreID)) )*100 if p.total_positions_of_cadre(cadre.cadreID) > 0 else None for p in provinces]
		chart.add(t.type, known_provinces + [unknown_province])
	return {'title': chart.title, 'data': chart.render_data_uri()}

def _chart_titles_by_cadre(cadre):
	chart = pygal.Pie(style=pygal.style.BlueStyle)
	chart.title = 'Position Titles of {0} Cadre'.format(cadre.type)
	#chart.x_labels = [t.type for t in cadre.titles]
	chart.add('Titles', [len(t.positions) for t in cadre.titles])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@blueprint.route('/data/cadre')
#@cache.cached(timeout=300, key_prefix=utils.make_cache_key)
def data_cadre():
	cadreID = request.args.get('cadreID')
	cadre = pm.CadreRef.query.filter_by(cadreID=cadreID).one()
	chart1 = _chart_titles_by_cadre(cadre)
	chart2 = _chart_percent_cadre_by_province(cadre)
	charts = [chart1, chart2]
	return jsonify({'charts': charts})


#  P O S I T I O N S
#------------------
def _chart_title_positions_by_province(positions):
	if not positions:
		return ''  # return generic no data graph?
	provinces = fm.ProvinceInfo.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = '{0} Positions by Province'.format(positions[0].title.type)
	chart.x_labels = [p.name for p in provinces] + ['Unknown']
	filled_positions = []
	unfilled_positions = []
	for province in provinces:
		# much faster than calling sqlalchemy again and using .count()
		filled_positions.append(len([p for p in positions if p.facility and p.facility.provinceID==province.id and p.personnelID]))
		unfilled_positions.append(len([p for p in positions if p.facility and p.facility.provinceID==province.id and not p.personnelID]))
	filled_positions.append(len([p for p in positions if not p.facility and p.personnelID]))
	unfilled_positions.append(len([p for p in positions if not p.facility and not p.personnelID]))
	chart.add('Filled', filled_positions)
	chart.add('Unfilled', unfilled_positions)
	return chart.render_data_uri()


@blueprint.route('/data/positions')
#@cache.cached(timeout=300, key_prefix=utils.make_cache_key)
def data_positions():
	titleID = request.args.get('titleID')
	positions = pm.PositionInfo.query.filter_by(titleID=titleID).all()
	addButton = """<!--Find this HTML in personnel/views.py--><a class="btn btn-default btn-sm" type="button" href="{0}"><i class="fa fa-{1}"></i> <i class="fa fa-plus"></i></a>"""
	#delButton = """<a class="btn btn-danger btn-sm {2}" type="button" href="#" data-href="{0}" data-toggle="confirmation" ><i class="fa fa-{1}"></i> <i class="fa fa-minus"></i></a>"""
	data = []
	for p in positions:
		if p.title:
			cadre = dict(cadreID=p.title.cadreID,type=p.title.cadre.type)
			title = dict(titleID=p.titleID,type=p.title.type,clinical=p.title.clinical,cadre=cadre)
		else:
			title={}
		if p.facility:
			facility = dict(facilityID=p.facility.facilityID,identifier=p.facility.identifier,link='<a href="{0}">{1}</a>'.format(url_for('facility.facility', facilityID=p.facilityID), p.facility.identifier))
		else:
			facility = {}
		if p.personnel:
			personnel = dict(personnelID=p.personnelID,identifier=p.personnel.identifier,link='<a href="{0}">{1}</a>'.format(url_for('personnel.view_personnel', personnelID=p.personnel.id), p.personnel.identifier))
		else:
			personnel = {}
		addButton=addButton.format(url_for('personnel.add_placement', positionID=p.id), 'user-md' if p.title.clinical else 'user')
		#delButton=delButton.format(url_for('personnel.delete_placement', positionID=p.id), 'user-md' if p.title.clinical else 'user', 'disabled' if not p.personnel else '')
		data.append(dict(id=p.id,
						 pn=p.pn,
						 pcv=p.pcv,
						 func=p.fund,
						 cccode=p.costCenterCode,
						 ccname=p.costCenterName,
						 department=p.department,
						 jobCode=p.jobCode,
						 actitvity=p.activity,
						 salaryGrade=p.salaryGrade,
						 personnel=personnel,
						 facility=facility,
						 title=title,
						 buttons=addButton  # delButton+addButton,
						 ))
	chart = _chart_title_positions_by_province(positions)
	return jsonify({'data': data, 'chart': chart})


@blueprint.route('/sort/cadre')
def sort_cadre():
	import json
	cadre_root = pm.CadreRef.query.filter_by(type='personnel').one()
	return render_template('personnels/sort_cadre.html', cadre_root=cadre_root)


# =================== #
#  P E R S O N N E L  #
# =================== #

#  V I E W
#--------
@blueprint.route('/<int:personnelID>')
@login_required
def view_personnel(personnelID):
	person = pm.PersonnelInfo.query.filter_by(id=personnelID).first_or_404()
	return render_template('personnels/person.html', person=person)


#  A D D
#------
@blueprint.route('/add', methods=['GET', 'POST'])
@login_required
def add_personnel():
	form = forms.PersonnelForm()
	if form.validate_on_submit():
		personnel = pm.PersonnelInfo.create(vnpfNumber=form.vnpfNumber.data,
								name=form.name.data,
								)
		logger.info('Created personnel', personnel=personnel, action='create')
		moha.cache_utils.force_update_personnel_cache()
		flash('Personnel Added', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('personnels/form.html', form=form)


#  U P D A T E
#------------
@blueprint.route('/<int:personnelID>/edit', methods=['GET', 'POST'])
@login_required
def edit_personnel(personnelID):
	personnel = pm.PersonnelInfo.query.filter_by(id=personnelID).first_or_404()
	form = forms.PersonnelForm(obj=personnel)
	if form.validate_on_submit():
		logger.debug('Form validated')
		pm.PersonnelInfo.update(personnel,
								vnpfNumber=form.vnpfNumber.data,
								name=form.name.data,
								)
		logger.info('Updated personnel', personnel=personnel, action='update')
		moha.cache_utils.force_update_personnel_cache()
		flash('Personnel Information updated', 'success')
		return form.redirect(request.args.get('next'))
	else:
		utils.flash_errors(form)
	return render_template('personnels/form.html', form=form)


# =================== #
#  P O S I T I O N S  #
# =================== #

#  A D D
#------
@blueprint.route('/position/add', methods=['GET', 'POST'])
@login_required
def add_position():
	form = forms.PositionForm()
	facilityID = request.args.get('facilityID')
	form.facilityID.data = facilityID
	form.titleID.choices = [(t.id, t.type) for t in pm.TitleRef.query.all()]
	if form.validate_on_submit():
		logger.debug('Form validated')
		position = pm.PositionInfo.create(titleID=form.titleID.data, 
									  	facilityID=form.facilityID.data,
										pn=form.pn.data,
										pcv=form.pcv.data,
									  	fund=form.fund.data,
										costCenterCode=form.costCenterCode.data,
										costCenterName=form.costCenterName.data,
										activity=form.activity.data,
										department=form.department.data,
										jobCode=form.jobCode.data,
										salaryGrade=form.salaryGrade.data
										)
		logger.info('Created position', position=position, action='create')
		moha.cache_utils.force_update_personnel_cache()
		flash('New Position Created', 'success')
		return form.redirect(request.args.get('next'))
	return render_template('personnels/form.html', form=form, title='Add Position')

#  U P D A T E
#------------
@blueprint.route('/position/<int:positionID>/edit', methods=['GET', 'POST'])
@login_required
def edit_position(positionID):
	"""Information about a position is edited.

	Positions can not be moved. They remmain at the facility they were created.
	Instead the personnel move to new positions and old positions remain or deleted."""
	logger.debug('Enter update positionID: {0}'.format(positionID))
	#positionID = request.args.get('positionID')
	position = pm.PositionInfo.query.filter_by(id=positionID).first_or_404()
	form = forms.PositionForm(obj=position)
	form.titleID.choices = [(t.id, t.type) for t in pm.TitleRef.query.all()]
	del form.facilityID
	if form.validate_on_submit():
		logger.debug('Form validated')
		pm.PositionInfo.update(position,
							   titleID=form.titleID.data,
							   pn=form.pn.data,
							   pcv=form.pcv.data,
							   fund=form.fund.data,
							   costCenterCode=form.costCenterCode.data,
							   costCenterName=form.costCenterName.data,
							   activity=form.activity.data,
							   department=form.department.data,
							   jobCode=form.jobCode.data,
							   salaryGrade=form.salaryGrade.data)
		logger.info('Updated position', position=position, action='update')
		moha.cache_utils.force_update_personnel_cache()
		flash('Position Updated', 'success')
		return form.redirect(request.args.get('next'))
	return render_template('personnels/form.html', form=form, title='Update Position')




# ===================== #
#  P L A C E M E N T S  #
# ===================== #

#  A D D
#------
@blueprint.route('/add_placement', methods=['GET', 'POST'])
@login_required
def add_placement():
	logger.debug('Enter add placement')
	form = forms.PlacementForm()
	personnelID = request.args.get('personnelID')
	if personnelID:
		person = pm.PersonnelInfo.query.filter_by(id=personnelID).one()
		form.personnelID.choices = [(person.id, person.identifier)]
	else:
		form.personnelID.choices = [(p.id, p.identifier) for p in pm.PersonnelInfo.query.filter_by(position=None).all()]
		if len(form.personnelID.choices) == 0:
			flash('There are no personnel currently without a position', 'info')
			return form.redirect(request.args.get('next'))

	facilityID = request.args.get('facilityID')	
	positionID = request.args.get('positionID')
	if positionID:
		position = pm.PositionInfo.query.filter_by(id=positionID).one()
		form.positionID.choices = [(position.id, position.identifier)]
	elif not positionID and facilityID:
		form.positionID.choices = [(p.id, p.identifier) for p in pm.PositionInfo.query.filter_by(facilityID=facilityID).order_by(pm.PositionInfo.jobCode).filter_by(personnelID=None).all()]
	else:
		form.positionID.choices = [(p.id, p.identifier) for p in pm.PositionInfo.query.order_by(pm.PositionInfo.jobCode).filter_by(personnelID=None).all()]

	if form.validate_on_submit():
		logger.debug('Form validated')
		try:
			current_placement = pm.PositionInfo.query.filter_by(id=form.positionID.data).one()
			pm.PositionInfo.update(current_placement, personnelID=form.personnelID.data)
			logger.info('Update placement', placement=current_placement, action='update')
			moha.cache_utils.force_update_personnel_cache()
			flash('Placement Updated', 'success')
		except NoResultFound:
			placement = pm.PositionInfo.create(id=form.positionID.data, personnelID=form.personnelID.data)
			logger.info('Create placement', placement=placement, action='create')
			moha.cache_utils.force_update_personnel_cache()
			flash('Placement Created', 'success')
		return form.redirect(request.args.get('next'))
	return render_template('personnels/form.html', form=form)


#  D E L E T E
# -----------
@blueprint.route('/delete_placement', methods=['GET', 'POST'])
@login_required
def delete_placement():
	form = forms.PlacementForm()  # Only to use the redirect
	positionID = request.args.get('positionID')
	current_placement = pm.PositionInfo.query.filter_by(id=positionID).one()
	pm.PositionInfo.update(current_placement, personnelID=None)
	logger.info('Removed placement', placement=current_placement, action='delete')
	moha.cache_utils.force_update_personnel_cache()
	flash('Personnel Removed From Position', 'success')
	return form.redirect(request.args.get('next'))