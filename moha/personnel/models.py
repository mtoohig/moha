# -*- coding: utf-8 -*-
"""Personnel models"""

from moha.database import CRUDMixin, Column, Model, SurrogatePK, db, reference_col, relationship, synonym, select, hybrid_property, hybrid_method


# =================== #
#  P E R S O N N E L  #
# =================== #

class CadreRef(Model):
	__tablename__ = 'cadreRef'

	cadreID = Column(db.Integer, primary_key=True)
	parentID = Column(db.Integer, db.ForeignKey('cadreRef.cadreID'), nullable=True)
	type = Column(db.Unicode, nullable=False)

	children = relationship('CadreRef',
							cascade='all, delete-orphan',
							backref=db.backref('parent', remote_side=[cadreID]))

	titles = relationship('TitleRef')

	@hybrid_property
	def cadre_title_ids(self):
		return [t.id for t in self.titles]

	def _flatten(self, S):
		"""Flatten the recursive lists of cadreIDs
		source :: https://stackoverflow.com/questions/12472338/flattening-a-list-recursively
		"""
		if S == []:
			return S
		if isinstance(S[0], list):
			return self._flatten(S[0]) + self._flatten(S[1:])
		return S[:1] + self._flatten(S[1:])

	def _recursive_cadre(self):
		"""Returns a list of lists containing all children and their children's cadreIDs"""
		return [[c._recursive_cadre(), c] for c in self.children]

	@hybrid_property
	def title_ids(self):
		"""Returns flat list of all cadreIDs that are below this category on the tree"""
		return self._flatten([c.cadre_title_ids for c in (self._flatten(self._recursive_cadre()) + [self])])


	def __repr__(self):
		return '<CadreRef(cadreID={0}, parentID={1}, type={2})>'.format(self.cadreID, self.parentID, self.type)

	def __unicode__(self):
		return self.type


class TitleRef(Model, SurrogatePK):
	__tablename__ = 'titleRef'
	cadreID = reference_col('cadreRef', pk_name='cadreID')
	cadre = relationship('CadreRef', back_populates='titles')

	type = db.Column(db.Unicode, nullable=False)
	clinical = db.Column(db.Boolean, nullable=True)

	positions = relationship('PositionInfo', back_populates='title')

	@hybrid_property
	def total_positions(self):
		return len(self.positions)

	@hybrid_property
	def total_personnel(self):
		return len(self.personnel)

	@hybrid_property
	def total_positions_filled_percent(self):
		positions_filled = len([p for p in self.positions if not p.vacant])
		try:
			return positions_filled / float(len(self.positions))
		except ZeroDivisionError:
			return 0

	@hybrid_method
	def total_by_province(self, provinceID):
		return len([p for p in self.positions if p.facility and p.facility.provinceID == provinceID])


	def __init__(self, **kwargs):
		"""Create instance."""
		db.Model.__init__(self, **kwargs)

	def __unicode__(self):
		return self.type

	def __repr__(self):
		return '<TitleRef(type={0}, clinical={1})>'.format(self.type, self.clinical)


class PersonnelInfo(Model, SurrogatePK):
	__tablename__ = 'personnelInfo'

	vnpfNumber = db.Column(db.Integer)
	name = db.Column(db.Unicode(250))
	firstName = db.Column(db.Unicode(150), nullable=True)  # Do not use
	lastName = db.Column(db.Unicode(150), nullable=True)  # Do not use  -  Instead just use name. data from HR is delivered as name only and this project does not care about personnel much

	position = relationship('PositionInfo', uselist=False, back_populates='personnel')
	
	@hybrid_property
	def identifier(self):
		return '{0} ({1})'.format(self.name if self.name else self.firstName + " " + self.lastName, self.vnpfNumber)
	
	@hybrid_property
	def totalSalary(self):
		"""Calculate Total Salary of person"""
		pass

	@hybrid_property
	def employed(self):
		return True if self.position else False 

	@hybrid_property
	def position_or_unemployed(self):
		return self.position.identifier if self.position else 'Unemployed'

	@hybrid_property
	def titleID(self):
		if self.employed:
			return self.position.titleID
		else:
			return False

	def __unicode__(self):
		return '{0}'.format(self.name)


class PositionInfo(Model, SurrogatePK):
	__tablename__ = 'positionInfo'

	pn = db.Column(db.Integer)
	pcv = db.Column(db.Unicode(1))
	fund = db.Column(db.Integer)
	costCenterCode = db.Column(db.Unicode(50))
	costCenterName = db.Column(db.Unicode(150))
	activity = db.Column(db.Unicode(50))
	department = db.Column(db.Unicode(150))
	jobCode = db.Column(db.Unicode(50))
	salaryGrade = db.Column(db.Unicode(50))

	titleID = reference_col('titleRef')
	title = relationship('TitleRef', back_populates='positions')

	personnelID = reference_col('personnelInfo', nullable=True)
	personnel = relationship('PersonnelInfo', uselist=False, back_populates='position')
	
	facilityID = reference_col('facilityInfo', nullable=True, pk_name='facilityID')
	facility = relationship('FacilityInfo', uselist=False, back_populates='positions')

	@hybrid_property
	def identifier(self):
		return '{1} - {0}'.format(self.title.type, self.jobCode)

	@hybrid_property
	def vacant(self):
		return False if self.personnelID else True

	@hybrid_property
	def vacant_or_person(self):
		return 'Vacant' if self.vacant else self.personnel.identifier

	@hybrid_property
	def clinical(self):
		return self.title.clinical

	def __unicode__(self):
		return '{0} at {1}'.format(self.title.type, self.jobCode)

	def __repr__(self):
		return '<PositionInfo(titleID={0}, facilityID={1})>'.format(self.titleID, self.facilityID)

	def as_dict(self):
		"""Produces the dictionary that is turned into JSON.
		Used to make markers for maps.
		"""
		data = {c.name: getattr(self, c.name) for c in self.__table__.columns}
		data['identifier'] = self.identifier
		data['vacant_or_person'] = self.vacant_or_person
		data['vacant'] = self.vacant
		data['clinical'] = self.clinical
		data['filled'] = not self.vacant
		return data

