# -*- coding: utf-8 -*-
"""Personnel Forms"""
import datetime

from flask import request
from flask_wtf import Form
from wtforms import StringField, SelectField, IntegerField, DateField, BooleanField, TextAreaField, HiddenField, SubmitField
from wtforms.validators import DataRequired

from flask import current_app as app

from moha.public.forms import RedirectForm


class PositionForm(RedirectForm):
	"""Position Form"""

	pcv = SelectField('PCV',
						choices=[('P', 'P'),('C', 'C'),('V', 'V')],
						validators=[DataRequired()])

	pn = IntegerField('PN')
	
	fund = IntegerField('Fund')

	costCenterCode = StringField('Cost Centre Code')

	costCenterName = StringField('Cost Centre Name')

	activity = StringField('Activity')

	department = StringField('Department')

	jobCode = StringField('Job Code')

	salaryGrade = StringField('Salary Grade')

	facilityID = HiddenField('Facility ID')

	titleID = SelectField('Position Title',
							choices=[],
							coerce=int,
							validators=[DataRequired()])


	def validate(self):
		"""Validate the form."""
		initial_validation = super(PositionForm, self).validate()
		if not initial_validation:
			return False

		return True


class PersonnelForm(RedirectForm):
	"""Personnel Form"""
	
	vnpfNumber = StringField('VNPF Number')
	name = StringField('Name', validators=[DataRequired()])

	def validate(self):
		"""Validate the form."""
		initial_validation = super(PersonnelForm, self).validate()
		if not initial_validation:
			return False

		return True


class PlacementForm(RedirectForm):
	"""Placement Form"""

	positionID = SelectField('Position Selection',
							choices=[],
							coerce=int,
							validators=[DataRequired()])

	personnelID = SelectField('Personnel Selection',
							choices=[],
							coerce=int,
							validators=[DataRequired()])


	def validate(self):
		"""Validate the form."""
		initial_validation = super(PlacementForm, self).validate()
		if not initial_validation:
			return False

		return True


class PositionSearchForm(Form):
	"""Search Personnel List."""

	pn = StringField('PN')
	vnpf = StringField('VNPF')
	title = SelectField('Title', choices=[], coerce=int)
	pcv = SelectField('PCV', choices=[])
	jobCode = StringField('Job Code')
	facility = SelectField('Facility', choices=[])
	province = SelectField('Province', choices=[], coerce=int)

	def validate(self):
		"""Validate the form."""
		initial_validation = super(PositionSearchForm, self).validate()
		if not initial_validation:
			return False

		return True