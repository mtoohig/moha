var cross = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'plus',
	markerColor: 'red',
});

var crossSpin = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'plus',
	markerColor: 'red',
	spin: true,
})


var hospital = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'hospital-o',
	markerColor: 'white',
	iconColor: 'black',
});

var hospitalSpin = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'hospital-o',
	markerColor: 'white',
	iconColor: 'black',
	spin: true,
})


var hSquare = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'h-square',
	markerColor: 'blue',
});

var hSquareSpin = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'h-square',
	markerColor: 'blue',
	spin: true,
})


var crossSquare = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'plus-square',
	markerColor: 'green',
});

var crossSquareSpin = L.AwesomeMarkers.icon({
	prefix: 'fa',
	icon: 'plus-square',
	markerColor: 'green',
	spin: true,
})


