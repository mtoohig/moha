# -*- coding: utf-8 -*-
"""User views."""
from flask import current_app as app
from flask import Blueprint, render_template, redirect, flash, request, url_for, flash
from flask_login import login_required, login_user

from moha.user.forms import RegisterForm
from moha.user.models import User
from moha.extensions import Message, mail
from moha.utils import flash_errors

from itsdangerous import URLSafeTimedSerializer
from datetime import datetime

import logging
logger = logging.getLogger('moha.user.views')

blueprint = Blueprint('user', __name__, url_prefix='/users', static_folder='../static')


@blueprint.route('/')
@login_required
def members():
	"""List members."""
	return render_template('users/members.html')


def generate_confirmation_token(email):
	serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
	return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])

def confirm_token(token, expiration=3600):
	serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
	try:
		email = serializer.loads(token, salt=app.config['SECURITY_PASSWORD_SALT'], max_age=expiration)
	except:
		return False
	return email


@blueprint.route('/register/', methods=['GET', 'POST'])
def register():
	"""Register new user.

	A user will register with a proper vanuatu.gov.vu email account, then the 
	user must be confirmed by a current user of MOHA from the admin panel.
	"""
	logger.debug('Enter register new user')
	form = RegisterForm(request.form, csrf_enabled=False)
	if form.validate_on_submit():
		logger.debug('Form validated')
		newUser=User.create(username=form.email.data, 
							email=form.email.data, 
							password=form.password.data, 
							first_name=form.first_name.data, 
							last_name=form.last_name.data,
							confirmed=True,
							active=True)
		logger.info('New User Created: {0} - {1}'.format(newUser.full_name, newUser.email))
		# User is not auto logged in after restering. A current user must approve of their access.
		#login_user(newUser)
		#logger.info('Logged in: {0}'.format(newUser.username))
		flash('Welcome, {0}'.format(newUser.full_name), 'success')
		flash('Please manage the data here responsibly, you can edit or delete anything.', 'info')
		return redirect(url_for('public.home'))
	else:
		flash_errors(form)
	return render_template('public/register.html', form=form)


"""
@blueprint.route('/confirm/<token>')
def confirm_email(token):
	try:
		email = confirm_token(token)
	except:
		flash('The confirmation link is invalid or has expired.', 'danger')
	user = User.query.filter_by(email=email).first_or_404()
	if user.confirmed:
		flash('Account already confirmed. Please login.', 'success')
	else:
		user.confirmed = True
		user.confirmed_on = datetime.now()
		User.update(user, confirmed=True, confirmed_on=datetime.now()) # Not needed with my CRUDMixim #.filter_by(email=email).first_or_404()
		flash('You have confirmed your account. Thanks!', 'success')
	return redirect(url_for('public.home'))



def send_email(to, subject, template):
	msg = Message(subject=subject, recipients=[to], sender=app.config['MAIL_DEFAULT_SENDER'])
	msg.html = template
	with app.app_context():
		mail.send(msg)
"""