# -*- coding: utf-8 -*-
"""Structure views."""
import os
import pygal

from flask import Blueprint, render_template, request, flash, url_for, redirect, g, jsonify
from flask import current_app as app
from flask_login import login_required
from flask_paginate import Pagination, get_page_parameter, get_per_page_parameter

from moha.extensions import csrf_protect, cache

import forms
import models as sm
import business as bus

from moha.facility import models as fm
from moha import utils
import moha.cache_utils as cacheDel

import structlog
logger = structlog.getLogger(__name__)

blueprint = Blueprint('structure', __name__, url_prefix='/structure', static_folder='../static')


'''
def _export_facility_network(facilityID):
	import networkx as nx
	structures = sm.StructureInfo.query.filter_by(facilityID=facilityID).all()
	G=nx.DiGraph()
	G.add_nodes_from([s.id for s in structures])
	for node in structures:
		G.node[node.id]['identifier'] = node.identifier
		G.node[node.id]['status'] = node.status
		for srvc in node.services:
				G.add_edge(node.id, srvc.id, {'structure_type': srvc.structure_type, 'status': srvc.status})

	node_colors = []
	edge_colors = []
	edge_styles = []
	for node, srvc, data in G.edges(data=True):
		# Edge Color
		if data['structure_type'] in ['WSR', 'WST']:
			color = 'blue'
		elif data['structure_type'] in ['P']:
			color = 'orange'
		elif data['structure_type'] in ['S']:
			color = 'gray'
		elif data['structure_type'] in ['C']:
			color = 'purple'
		elif data['structure_type'] in ['CST']:
			color = 'lightblue'
		else:
			color = 'black'
		edge_colors.append(color)

		# Edge Style
		if not data['status']:
			style='dashed'
		else:
			style='solid'
		edge_styles.append(style)

		# Node Color
		if not data['status']:
			color = 'red'
		else:
			color = 'green'
		node_colors.append(color)


	pos=nx.spring_layout(G)
	
	directory = os.path.join(app.config['FILE_FOLDER'], facilityID)
	if not os.path.exists(directory):
		os.makedirs(directory)
	G=nx.path_graph(4)
	nx.write_gexf(G, os.path.join(directory, 'facility_services_network.gexf'))


# To be depreciated for a js interative network
# This is depreciated in favor of sigma.js in the browser(templates)
@blueprint.after_request
def test(response):
	"""After every request in this blueprint the networkx models of the 
	facility must be redrawn to make sure they are accurate.
	"""
	facilityID = g.get('facilityID')
	_export_facility_network(facilityID)
	return response
'''


#  S E A R C H 
#------------
def _chart_structure_by_province(structure_type, disable_xml_declaration=False):
	provinces = fm.ProvinceInfo.query.all()
	structures = sm.StructureInfo.query.filter_by(structure_type=structure_type).all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Structures by Province'
	chart.x_labels = [p.name for p in provinces]

	return {'title': chart.title, 'data': chart.render_data_uri()}

def _chart_percent_designation_by_province(disable_xml_declaration=False):
	provinces = fm.ProvinceInfo.query.all()
	designations = fm.DesignationRef.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Designations by Province (%)'
	chart.value_formatter = lambda x: '{}%'.format(x)
	chart.x_labels = [p.name for p in provinces]
	for d in designations:
		chart.add(d.name, [(d.total_designations(p.id)/float(p.total_facilities))*100 if p.total_facilities > 0 else None for p in provinces])
	return {'title': chart.title, 'data': chart.render_data_uri()}

def _chart_facility_condition(disable_xml_declaration=False):
	facilities = fm.FacilityInfo.query.all()
	chart = pygal.Pie(style=pygal.style.BlueStyle)
	chart.title = 'Facility Status'
	chart.show_legend = False
	chart.add('Status', [{'value': len([f for f in facilities if f.status and not f.damaged]), 'label': 'Open', 'color': 'green'},
						{'value': len([f for f in facilities if not f.status and not f.damaged]), 'label': 'Closed', 'color': 'black'},
						{'value': len([f for f in facilities if f.status and f.damaged]), 'label': 'Open/Damaged', 'color': 'orange'},
						{'value': len([f for f in facilities if not f.status and f.damaged]), 'label': 'Closed/Damaged', 'color': 'red'}])
	return {'title': chart.title, 'data': chart.render_data_uri()}

def _chart_percent_facility_condition_by_province(disable_xml_declaration=False):
	provinces = fm.ProvinceInfo.query.all()
	chart = pygal.StackedBar(style=pygal.style.BlueStyle)
	chart.title = 'Facility Status by Province (%)'
	chart.value_formatter = lambda x: '{}%'.format(x)
	chart.show_legend = False
	chart.x_labels = [p.name for p in provinces]
	chart.add('', [{'value': p.facilities_open/float(len(p.facilities))*100, 'label': 'Open', 'color': 'green'} if p.total_facilities > 0 else None for p in provinces])
	chart.add('', [{'value': p.facilities_closed/float(len(p.facilities))*100, 'label': 'Closed', 'color': 'black'} if p.total_facilities > 0 else None for p in provinces])
	chart.add('', [{'value': p.facilities_damaged_open/float(len(p.facilities))*100, 'label': 'Damaged/Open', 'color': 'orange'} if p.total_facilities > 0 else None for p in provinces])
	chart.add('', [{'value': p.facilities_damaged_closed/float(len(p.facilities))*100, 'label': 'Damaged/Closed', 'color': 'red'} if p.total_facilities > 0 else None for p in provinces])
	return {'title': chart.title, 'data': chart.render_data_uri()}

def _chart_facility_rank():
	"""This is a test and not a complete ranking or report"""
	provinces = fm.ProvinceInfo.query.all()
	facilities = fm.FacilityInfo.query.all()
	chart = pygal.XY(style=pygal.style.BlueStyle)
	chart.stroke = False
	chart.title = 'Facility Condition'
	for f in facilities:
		chart.add(f.facilityName, [])
	return {'title': chart.title, 'data': chart.render_data_uri()}

@blueprint.route('/data/charts')
@cache.cached(timeout=300, key_prefix=utils.make_cache_key)
def _charts():
	structure_type = request.args.get('structure_type')
	logger.debug('Enter _charts')
	chart1 = None#_chart_structure_by_province(structure_type)
	chart2 = _chart_percent_designation_by_province()
	chart3 = _chart_facility_condition()
	chart4 = _chart_percent_facility_condition_by_province()
	chart5 = _chart_facility_rank()
	logger.debug('Exit _charts')
	return jsonify({'charts': [chart1, chart2, chart3, chart4, chart5]})



# ===================== #
#  S T R U C T U R E S  #
# ===================== #

#  S E A R C H 
#------------
#@csrf_protect.exempt
@blueprint.route('/search/', methods=['GET', 'POST'])
@csrf_protect.exempt
def search():
	"""List structures."""
	logger.debug('Enter structure search')
	form = forms.StructureSearchForm()
	# Create the base query for the structures for all structures 
	# Create base queries for different columns of search page
	query = sm.StructureInfo.query.order_by(sm.StructureInfo.facilityID).order_by(sm.StructureInfo.structure_type).join(fm.FacilityInfo)
	type_query = None
	province_query = fm.ProvinceInfo.query.order_by(fm.ProvinceInfo.id)
	island_query = fm.IslandInfo.query.order_by(fm.IslandInfo.name)
	healthZone_query = fm.HealthZoneInfo.query.order_by(fm.HealthZoneInfo.name)
	designation_query = fm.DesignationRef.query.order_by(fm.DesignationRef.name)
	# Collect the argumetns from the Form's GET submit and set defaults to the form

	# Filter results
	#---------------

	# If structure_type filter main query
	structure_type = form.structure_type.data
	if structure_type != 'all':

		# Get the type tables for the selected structure_type
		if structure_type == 'B':
			structure_table = sm.BuildingInfo
			type_table = sm.BuildingRef
		elif structure_type == 'WSR':
			structure_table = sm.WaterSourceInfo
			type_table = sm.WaterSourceRef
		elif structure_type == 'WST':
			structure_table = sm.WaterStorageInfo
			type_table = sm.WaterStorageRef
		elif structure_type == 'P':
			structure_table = sm.PowerInfo
			type_table = sm.PowerRef
		elif structure_type == 'S':
			structure_table = sm.SanitationInfo
			type_table = sm.SanitationRef
		elif structure_type == 'C':
			structure_table = sm.CommunicationInfo
			type_table = sm.CommunicationRef
		elif structure_type == 'CST':
			structure_table = sm.ColdStorageInfo
			type_table = sm.ColdStorageRef
		elif structure_type == 'V':
			structure_table = sm.VehicleInfo
			type_table = sm.VehicleRef
		elif structure_type == 'BO':
			structure_table = sm.BoatInfo
			type_table = sm.BoatRef
		else:
			logger.error('Unknown structure_type: {0}'.format(structure_type))

		# Create the base query for the structures -- Overwrite query using StructureInfo before
		query = structure_table.query.order_by(structure_table.facilityID).join(fm.FacilityInfo)

		# Create query for all types that can populate type in the form
		type_query = type_table.query.order_by(type_table.type)

		# If a type is choosen add it to the filter on base query
		if form.typeID.data >= 0:
			query = query.join(type_table).filter(type_table.id==form.typeID.data)

	if form.provinceID.data >= 0:
		query = query.filter(fm.FacilityInfo.provinceID==form.provinceID.data)
		island_query = island_query.filter(fm.IslandInfo.provinceID==form.provinceID.data)
	if form.islandID.data >= 0:
		query = query.filter(fm.FacilityInfo.islandID==form.islandID.data)
	if form.healthZoneID.data >= 0:
		query = query.filter(fm.FacilityInfo.healthZoneID==form.healthZoneID.data)
	if form.designationID.data >= 0:
		query = query.filter(fm.FacilityInfo.designationID==form.designationID.data)

	# Finalize the queries and add an 'all' option to the SelectFields in the form
	form.structure_type.choices = [('all','all')] + [('B','Building'),('WSR','Water Source'),('WST','Water Storage'),('P','Power'),('S','Sanitation'),('C','Communication'),('CST','Cold Storage'),('V','Vehicle'),('BO','Boat')]
	if type_query:
		form.typeID.choices = [(-1,'all')] + [(t.id, t.type) for t in type_query.all()]
	else:
		del form.typeID
	form.provinceID.choices = [(-1,'all')] + [(p.id, p.name.strip()) for p in province_query.all()]
	form.islandID.choices = [(-1,'all')] + [(i.id, i.name.strip()) for i in island_query.all()]
	form.healthZoneID.choices = [('-1','all')] + [(h.id, h.name.strip()) for h in healthZone_query.all()]
	form.designationID.choices = [(-1,'all')] + [(d.id, d.name.strip()) for d in designation_query.all()]

	# Set up pagination
	page = request.args.get(get_page_parameter(), type=int, default=1)
	per_page = request.args.get(get_per_page_parameter(), type=int, default=10)	
	pagination = Pagination(page=page, per_page=per_page, total=query.count(), search=False, record_name='structures', bs_version=3)
	structures = query.limit(per_page).offset((pagination.page - 1) * per_page)
	return render_template('structures/structures.html', form=form, structures=structures, pagination=pagination)

#  A D D
#------
@blueprint.route('/<structure_type>/add', methods=['GET', 'POST'])
@login_required
def add_structure(structure_type):
	"""Add a structure."""
	logger.debug('Enter add structure')
	facilityID = request.args.get('facilityID')
	form = bus._get_structure_form(structure_type, facilityID)
	if form.validate_on_submit():
		logger.debug('Form validated')
		logger.debug('Entering business to create structure')
		bus._add_structure(form, structure_type)
		cacheDel.force_update_structure_cache()
		flash('Structure Created', 'success')
		redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
		return redirect(redirect_url)
	else:
		utils.flash_errors(form)
	template = 'structures/form_{0}.html'.format(structure_type) if os.path.exists(os.path.join(app.config['APP_DIR'], 'templates', 'structures', 'form_{0}.html'.format(structure_type))) else 'structures/form.html'
	return render_template(template, form=form, title='Add')

#  U P D A T E 
#------------
@blueprint.route('/<structure_type>/<int:structureID>/update', methods=['GET', 'POST'])
@login_required
def update_structure(structure_type, structureID):
	"""Update a structure."""
	logger.debug('Enter update structureID: {0}'.format(structureID))
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).first_or_404()
	form = bus._get_structure_form(structure_type, facilityID, obj=structure)
	if form.validate_on_submit():
		logger.debug('Form validated')
		logger.debug('Entering business to update structure')
		bus._update_structure(form, structure_type, structure)
		cacheDel.force_update_structure_cache()
		flash('Structure Updated', 'success')
		redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
		return redirect(redirect_url)
	else:
		utils.flash_errors(form)
	template = 'structures/form_{0}.html'.format(structure_type) if os.path.exists(os.path.join(app.config['APP_DIR'], 'templates', 'structures', 'form_{0}.html'.format(structure_type))) else 'structures/form.html'
	return render_template(template, form=form, title='Update')

#  D E L E T E 
#------------
@blueprint.route('/<structure_type>/<int:structureID>/delete', methods=['GET', 'POST'])
@login_required
def delete_structure(structure_type, structureID):
	"""Delete a structure."""
	logger.debug('Enter delete structureID: {0}'.format(structureID))
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).first_or_404()
	if structure.functional_areas:
		logger.debug('Cancel delete structureID: {0} due to functional_areas'.format(structureID))
		flash('Can not remove this structure until you remove or transfer the assets to another structure.', 'info')
		redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
		return redirect(redirect_url)
	sm.StructureInfo.delete(structure)
	logger.info('Deleted structureID: {0}'.format(structureID), structure=structure, action='delete')
	cacheDel.force_update_structure_cache()
	flash('Structure Removed', 'success')
	redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
	return redirect(redirect_url)


# ========================= #
#   I N S P E C T I O N S   #
# ========================= #

#  A D D 
#------
@blueprint.route('/<structure_type>/<int:structureID>/inspection/add', methods=['GET', 'POST'])
@login_required
def add_inspection(structure_type, structureID):
	"""Add an inspection."""
	logger.debug('Enter add inspection')
	facilityID = request.args.get('facilityID')
	form = bus._get_inspection_form(structure_type, structureID)
	if form.validate_on_submit():
		logger.debug('Form validated')
		logger.debug('Enter business to create inspection')
		bus._add_inspection(form, structure_type)
		cacheDel.force_update_structure_cache()	
		flash('Inspection Added', 'success')
		redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
		return redirect(redirect_url)
	else:
		utils.flash_errors(form)
	template = 'structures/form_{0}_inspection.html'.format(structure_type) if os.path.exists(os.path.join(app.config['APP_DIR'], 'templates', 'structures', 'form_{0}_inspection.html'.format(structure_type))) else 'structures/form.html'
	return render_template(template, form=form, title='Add Inspection')#{0} Inspection'.format(structure_type))

# U P D A T E
#-----------
@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/update', methods=['GET', 'POST'])
@login_required
def update_inspection(structure_type, structureID, inspectionID):
	"""Update an inspection."""
	logger.debug('Enter update inspectionID: {0}'.format(inspectionID))
	facilityID = request.args.get('facilityID')
	inspection = sm.StructureInspection.query.filter_by(id=inspectionID).one()
	form = bus._get_inspection_form(structure_type, facilityID, inspection)
	if form.validate_on_submit():
		logger.debug('Form validated')
		logger.debug('Enter business to update inspection')
		bus._update_inspection(form, structure_type, inspection)
		cacheDel.force_update_structure_cache()
		flash('Inspection Updated', 'success')
		redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
		return redirect(redirect_url)
	else:
		utils.flash_errors(form)
	template = 'structures/form_{0}_inspection.html'.format(structure_type) if os.path.exists(os.path.join(app.config['APP_DIR'], 'templates', 'structures', 'form_{0}_inspection.html'.format(structure_type))) else 'structures/form.html'
	return render_template(template, form=form, title='Update Inspection')#{0} Inspection'.format(structure_type))

# D E L E T E 
#-----------
@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/delete', methods=['GET', 'POST'])
@login_required
def delete_inspection(structure_type, structureID, inspectionID):
	"""Delete a building's inspection."""
	logger.debug('Enter delete inspectionID: {0}'.format(inspectionID))
	facilityID = request.args.get('facilityID')
	inspection = sm.StructureInspection.query.filter_by(id=inspectionID).first_or_404()
	sm.StructureInspection.delete(inspection)
	logger.info('Deleted inspectionID: {0}'.format(inspectionID), inspection=inspection, action='delete')
	cacheDel.force_update_structure_cache()
	flash('Inspection Removed', 'success')
	redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
	return redirect(redirect_url)


# =========== #
#  F I L E S  #
# =========== #

#
##  TODO Set Default, delete and edit pictures not configured yet.
#

'''
@blueprint.route('/<structure_type>/<int:structureID>/files')
@login_required
def structure_files(structure_type, structureID):
	"""View and edit photos"""
	logger.debug('Enter structure files')
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).one()
	files = structure.files
	pics = structure.inspection.pics if structure.inspection else []
	return render_template('structures/set_default_pic.html', structure=structure, files=files, pics=pics)
'''


@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/make_default')
@login_required
def make_default_file(structure_type, structureID, inspectionID):
	"""Set an inspection picture as the default picture."""
	facilityID = request.args.get('facilityID')
	fileID = request.args.get('fileID')
	logger.debug('Enter make default fileID: {0} for inspectionID: {1}'.format(fileID, inspectionID))
	file_type = request.args.get('file_type')
	if file_type == 'picture':
		for f in sm.InspectionPicRef.query.filter_by(inspectionID=inspectionID).filter_by(default=True).all():
			sm.InspectionPicRef.update(f, default=False)
		file = sm.InspectionPicRef.query.filter_by(fileID=fileID).filter_by(id=inspectionID).one()
		sm.InspectionPicRef.update(file, default=True)
		logger.info('New default picture inspectionID: {0} fileID: {1}'.format(inspectionID, fileID), file=file)
		flash('Default Picture Updated', 'success')
	else:
		logger.warn('Unsupported file_type: {0}'.format(file_type))
		flash('This file type is not supported for setting a default', 'warning')
		#TODO catch this
	return redirect(url_for('facility.facility', facilityID=facilityID))


#  A D D
#------
#@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/add_file', methods=['GET', 'POST'])
@blueprint.route('/<structure_type>/<int:structureID>/add_file', methods=['GET', 'POST'])
@login_required
def add_structure_file(structure_type, structureID, inspectionID=None):
	"""Add files or pictures to association tables between FileInfo and StructureInfo or StructureInspection.

	:param::file_type: required :: either 'pic' or 'file'

	:param::inspectionID: optional :: if present will attach the file at the inspection level


	It is built to allow multiple file uploads but not tested yet.

	"""
	logger.debug('Enter add structure file')
	facilityID = request.args.get('facilityID')
	file_type = request.args.get('file_type')
	form = forms.FileForm()
	if file_type == 'picture':
		form.inspectionID.choices = [(i.id, i.inspectionDate) for i in sm.StructureInspection.query.filter_by(structureID=structureID).order_by(sm.StructureInspection.inspectionDate).all()]
	else:
		del form.inspectionID
	default_selector = True if file_type == 'picture' else False
	title = 'Upload {0}'.format(file_type.capitalize())
	if form.validate_on_submit():
		logger.debug('Form validated')
		default = True if request.form.get('default') == 'default' else False
		files_uploaded = request.files.getlist('file')
		if files_uploaded:
			pic_count = 0
			file_count = 0
			for file in files_uploaded:
				if file_type == 'picture' and utils.allowed_picture(file.filename):
					newFile = utils.add_file(file, form, app.config['PICTURE_FOLDER'], facilityID, fm.FileInfo)
					if default:
						for p in sm.InspectionPicRef.query.filter_by(default=True).all():
							sm.InspectionPicRef.update(p, default=False)
							logger.debug('Removed default from fileID: {0}'.format(p.id))
					sm.InspectionPicRef.create(fileID=newFile.id, id=form.inspectionID.data, default=default)
					logger.info('Created fileID: {0} association with inspectionID: {1}'.format(newFile.id, form.inspectionID.data), file=newFile, action='create')
					pic_count += 1
				elif file_type == 'file' and utils.allowed_file(file.filename):
					newFile = utils.add_file(file, form, app.config['FILE_FOLDER'], facilityID, fm.FileInfo)
					if default:
						for f in sm.StructureFileRef.query.filter_by(default=True).all():
							sm.StructureFileRef.update(f, default=False)
							logger.debug('Removed default from fileID: {0}'.format(f.id))
					sm.StructureFileRef.create(fileID=newFile.id, id=structureID, default=default)
					logger.info('Created fileID: {0} association with structureID: {1}'.format(newFile.id, structureID), file=newFile, action='create')
					file_count += 1					
				else:
					logger.warn('File type {0} is not a valid choice'.format(file_type))
					flash('{0} - file type not allowed when you are trying to upload {1}s.'.format(file.filename, file_type), 'warning')
			#  After all pictures are uploaded, redirect. Theory at least, multi-upload not supported yet.
			if file_count > 0:
				logger.debug('{0} files uploaded to structureID: {1}'.format(file_count, structureID))
				flash('{0} Files Added'.format(file_count), 'success')
			if pic_count > 0:
				logger.debug('{0} pictures uploaded to structureID: {1}'.format(pic_count, structureID))
				flash('{0} Pictures Added'.format(pic_count), 'success')
			redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
			return redirect(redirect_url)
		else:
			flash('No files were uploaded, please try again.', 'warning')
	else:
		utils.flash_errors(form)
	return render_template('structures/form_file.html', form=form, title=title, default=default_selector)
"""
@blueprint.route('/<structure_type>/<int:structureID>/add_file', methods=['GET', 'POST'])
@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/add_file', methods=['GET', 'POST'])
@login_required
def add_structure_file(structure_type, structureID, inspectionID=None):
	
	logger.debug('Enter add structure file')
	facilityID = request.args.get('facilityID')
	form = forms.FileForm()
	form.inspectionID.choices = [(i.id, i.inspectionDate) for i in sm.StructureInspection.query.filter_by(structureID=structureID).order_by(sm.StructureInspection.inspectionDate).all()]
	file_type = request.args.get('file_type')
	default_selector = True if file_type == 'picture' else False
	title = 'Upload {0}'.format(file_type.capitalize())
	if form.validate_on_submit():
		logger.debug('Form validated')
		default = True if request.form.get('default') == 'default' else False
		files_uploaded = request.files.getlist('file')
		if files_uploaded:
			pic_count = 0
			file_count = 0
			for file in files_uploaded:
				if file_type == 'picture' and utils.allowed_picture(file.filename):
					if inspectionID:
						newFile = utils.add_file(file, form, app.config['PICTURE_FOLDER'], facilityID, fm.FileInfo)
						if default:
							for p in sm.InspectionPicRef.query.filter_by(default=True).all():
								sm.InspectionPicRef.update(p, default=False)
								logger.debug('Removed default from fileID: {0}'.format(p.id))
						sm.InspectionPicRef.create(fileID=newFile.id, id=inspectionID, default=default)
						logger.info('Created fileID: {0} association with inspectionID: {1}'.format(newFile.id, inspectionID), file=newFile, action='create')
						pic_count += 1
					else:	
						newFile = utils.add_file(file, form, app.config['PICTURE_FOLDER'], facilityID, fm.FileInfo)
						if default:
							for p in sm.StructurePicRef.query.filter_by(default=True).all():
								sm.StructurePicRef.update(p, default=False)
								logger.debug('Removed default from fileID: {0}'.format(p.id))
						sm.StructurePicRef.create(fileID=newFile.id, id=structureID, default=default)
						logger.info('Created fileID: {0} association with structureID: {1}'.format(newFile.id, structureID), file=newFile, action='create')
						pic_count += 1
				elif file_type == 'file' and utils.allowed_file(file.filename):
					if inspectionID:
						newFile = utils.add_file(file, form, app.config['FILE_FOLDER'], facilityID, fm.FileInfo)
						if default:
							for f in sm.InspectionFileRef.query.filter_by(default=True).all():
								sm.InspectionFileRef.update(f, default=False)
								logger.debug('Removed default from fileID: {0}'.format(f.id))
						sm.InspectionFileRef.create(fileID=newFile.id, id=inspectionID, default=default)
						logger.info('Created fileID: {0} association with inspectionID: {1}'.format(newFile.id, inspectionID), file=newFile, action='create')
						file_count += 1
					else:
						newFile = utils.add_file(file, form, app.config['FILE_FOLDER'], facilityID, fm.FileInfo)
						if default:
							for f in sm.StructureFileRef.query.filter_by(default=True).all():
								sm.StructureFileRef.update(f, default=False)
								logger.debug('Removed default from fileID: {0}'.format(f.id))
						sm.StructureFileRef.create(fileID=newFile.id, id=structureID, default=default)
						logger.info('Created fileID: {0} association with structureID: {1}'.format(newFile.id, structureID), file=newFile, action='create')
						file_count += 1
				else:
					logger.warn('File type {0} is not a valid choice'.format(file_type))
					flash('{0} - file type not allowed when you are trying to upload {1}s.'.format(file.filename, file_type), 'warning')
			#  After all pictures are uploaded, redirect. Theory at least, multi-upload not supported yet.
			if file_count > 0:
				logger.debug('{0} files uploaded to structureID: {1}'.format(file_count, structureID))
				flash('{0} Files Added'.format(file_count), 'success')
			if pic_count > 0:
				logger.debug('{0} pictures uploaded to structureID: {1}'.format(pic_count, structureID))
				flash('{0} Pictures Added'.format(pic_count), 'success')
			redirect_url = request.args.get('next') or url_for('facility.facility', facilityID=facilityID)
			return redirect(redirect_url)
		else:
			flash('No files were uploaded, please try again.', 'warning')
	else:
		utils.flash_errors(form)
	return render_template('structures/form_file.html', form=form, title=title, default=default_selector)
"""

#  U P D A T E
#------------
@blueprint.route('/<structure_type>/<int:structureID>/file/<int:fileID>/update', methods=['GET', 'POST'])
@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/file/<int:fileID>/update', methods=['GET', 'POST'])
@login_required
def update_structure_file(structure_type, structureID, fileID, inspectionID=None):
	logger.debug('Enter edit fileID: {0}'.format(fileID))
	facilityID = request.args.get('facilityID')
	file = fm.FileInfo.query.filter_by(id=fileID).first_or_404()
	form = forms.FileForm(dateCreated=file.fileDateCreated, description=file.fileDesc)
	del form.file
	del form.inspectionID
	if form.validate_on_submit():
		fm.FileInfo.update(file, 
							fileDateCreated=form.dateCreated.data,
							fileDesc=form.description.data)
		logger.info('Updated fileID: {0}'.format(fileID), file=file, action='update')
		return redirect(url_for('facility.facility', facilityID=facilityID))
	else:
		utils.flash_errors(form)
	return render_template('structures/form.html', form=form, title='Update File')

#  D E L E T E
#------------
@blueprint.route('/<structure_type>/<int:structureID>/file/<int:fileID>/delete', methods=['GET', 'POST'])
@blueprint.route('/<structure_type>/<int:structureID>/inspection/<int:inspectionID>/file/<int:fileID>/delete', methods=['GET', 'POST'])
@login_required
def delete_structure_file(structure_type, structureID, fileID, inspectionID=None):
	"""Delete a file from a structure or structure's inspection.

	:param::file_type: required :: either 'picture' or 'file'

	:param::inspectionID: optional :: if present will remove the file from the inspection
	"""
	logger.debug('Enter delete fileID: {0}'.format(fileID))
	facilityID = request.args.get('facilityID')
	form = forms.FileForm()
	file_type = request.args.get('file_type')
	if file_type == 'picture':
		if inspectionID:
			file = sm.InspectionPicRef.query.filter_by(fileID=fileID, id=inspectionID).one()
			sm.InspectionPicRef.delete(file)
		else:
			file = sm.StructurePicRef.query.filter_by(fileID=fileID, id=structureID).one()
			sm.StructurePicRef.delete(file)
		logger.info('Removed fileID: {0}'.format(fileID), file=file, action='delete')
		flash('Picture removed', 'success')
	elif file_type == 'file':
		if inspectionID:
			file = sm.InspectionFileRef.query.filter_by(fileID=fileID, id=inspectionID).one()
			sm.InspectionFileRef.delete(file)
		else:
			file = sm.StructureFileRef.query.filter_by(fileID=fileID, id=structureID).one()
			sm.StructureFileRef.delete(file)
		logger.info('Removed fileID: {0}'.format(fileID), file=file, action='delete')
		flash('File removed', 'success')
	else:
		logger.warn('File type {0} is not a valid choice'.format(file_type))
		flash('Error removing file', 'warning')
	return form.redirect(request.args.get('next'))



# ================= #
#  S E R V I C E S  #
# ================= #

@blueprint.route('/<int:structureID>/services')
@login_required
def services(structureID):
	logger.debug('Enter services structureID: {0}'.format(structureID), structureID=structureID)
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).one()
	structures = sm.StructureInfo.query.filter_by(facilityID=facilityID).filter(sm.StructureInfo.structure_type!=structure.structure_type).all()
	return render_template('structures/form_services.html', structure=structure, structures=structures)

#  A D D  S E R V I C E 
#------
@blueprint.route('/<structure_type>/<int:structureID>/add_service/<srvc_structure_type>/<int:srvc_structureID>')
@login_required
def add_service(structure_type, structureID, srvc_structure_type, srvc_structureID):
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).one()
	srvc_structure = sm.StructureInfo.query.filter_by(id=srvc_structureID).one()
	structure.services.append(srvc_structure)
	sm.db.session.add(structure)
	sm.db.session.commit()
	logger.debug('Added service', service_to=structureID, service_from=srvc_structureID, action='create')
	return redirect(url_for('structure.services', facilityID=facilityID, structure_type=structure_type, structureID=structureID))

#  D E L E T E  S E R V I C E 
#------------
@blueprint.route('/<structure_type>/<int:structureID>/delete_service/<srvc_structure_type>/<int:srvc_structureID>')
@login_required
def delete_service(structure_type, structureID, srvc_structure_type, srvc_structureID):
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).one()
	srvc_structure = sm.StructureInfo.query.filter_by(id=srvc_structureID).one()
	structure.services.remove(srvc_structure)
	sm.db.session.commit()
	logger.info('Removed service', service_to=structureID, service_from=srvc_structureID, action='delete')
	return redirect(url_for('structure.services', facilityID=facilityID, structure_type=structure_type, structureID=structureID))

#  A D D  S E R V I C I N G 
#------
@blueprint.route('/<structure_type>/<int:structureID>/add_servicing/<srvc_structure_type>/<int:srvc_structureID>')
@login_required
def add_servicing(structure_type, structureID, srvc_structure_type, srvc_structureID):
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).one()
	srvc_structure = sm.StructureInfo.query.filter_by(id=srvc_structureID).one()
	structure.servicing.append(srvc_structure)
	sm.db.session.add(structure)
	sm.db.session.commit()
	logger.debug('Added service', service_from=structureID, service_to=srvc_structureID, action='create')
	return redirect(url_for('structure.services', facilityID=facilityID, structure_type=structure_type, structureID=structureID))

#  D E L E T E  S E R V I C I N G 
#------------
@blueprint.route('/<structure_type>/<int:structureID>/delete_servicing/<srvc_structure_type>/<int:srvc_structureID>')
@login_required
def delete_servicing(structure_type, structureID, srvc_structure_type, srvc_structureID):
	facilityID = request.args.get('facilityID')
	structure = sm.StructureInfo.query.filter_by(id=structureID).one()
	srvc_structure = sm.StructureInfo.query.filter_by(id=srvc_structureID).one()
	structure.servicing.remove(srvc_structure)
	sm.db.session.commit()
	logger.info('Removed service', service_from=structureID, service_to=srvc_structureID, action='delete')
	return redirect(url_for('structure.services', facilityID=facilityID, structure_type=structure_type, structureID=structureID))

