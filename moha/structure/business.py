# -*- coding: utf-8 -*-
"""Structure business."""

import os
import datetime
import openpyxl

import models as sm
import forms

from flask import current_app as app

import structlog
logger = structlog.getLogger(__name__)


def _get_structure_form(structure_type, facilityID, obj=None):

	def _populate_building_form(form):
		form.typeID.choices = [(b.id, b.type) for b in sm.BuildingRef.query.all()]
		form.roofID.choices = [(r.id, r.type) for r in sm.RoofRef.query.all()]
		form.constructionID.choices = [(c.id, c.type) for c in sm.ConstructionRef.query.all()]
		return form
	def _populate_water_source_form(form):
		form.typeID.choices = [(w.id, w.type) for w in sm.WaterSourceRef.query.all()]
		return form
	def _populate_water_storage_form(form):
		form.typeID.choices = [(w.id, w.type) for w in sm.WaterStorageRef.query.all()]
		return form
	def _populate_power_form(form):
		form.typeID.choices = [(p.id, p.type) for p in sm.PowerRef.query.all()]
		return form
	def _populate_sanitation_form(form):
		form.typeID.choices = [(s.id, s.type) for s in sm.SanitationRef.query.all()]
		return form
	def _populate_communication_form(form):
		form.typeID.choices = [(c.id, c.type) for c in sm.CommunicationRef.query.all()]
		return form
	def _populate_cold_storage_form(form):
		form.typeID.choices = [(c.id, c.type) for c in sm.ColdStorageRef.query.all()]
		return form
	def _populate_vehicle_form(form):
		form.typeID.choices = [(v.id, v.type) for v  in sm.VehicleRef.query.all()]
		form.engineTypeID.choices = [(e.id, e.type) for e in sm.EngineTypeRef.query.all()]
		form.gearboxTypeID.choices = [(g.id, g.type) for g in sm.GearboxTypeRef.query.all()]
		return form
	def _populate_boat_form(form):
		form.typeID.choices = [(b.id, b.type) for b in sm.BoatRef.query.all()]
		return form

	if structure_type == 'B':
		return _populate_building_form(forms.BuildingForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'WSR':
		return _populate_water_source_form(forms.WaterSourceForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'WST':
		return _populate_water_storage_form(forms.WaterStorageForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'P':
		return _populate_power_form(forms.PowerForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'S':
		return _populate_sanitation_form(forms.SanitationForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'C':
		return _populate_communication_form(forms.CommunicationForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'CST':
		return _populate_cold_storage_form(forms.ColdStorageForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'V':
		return _populate_vehicle_form(forms.VehicleForm(facilityID=facilityID, obj=obj))
	elif structure_type == 'BO':
		return _populate_boat_form(forms.BoatForm(facilityID=facilityID, obj=obj))
	else:
		logger.error('Caught unsupported structure_type: {0}'.format(structure_type))


def _get_inspection_form(structure_type, structureID, inspection_obj=None):

	def _populate_building_form(form):
		del form.avg  # this value will be calculated from the average of all its components
		del form.note  # this seems unnecessary due to each component having its own note 
		return form
	def _populate_water_source_form(form):
		return form
	def _populate_water_storage_form(form):
		return form
	def _populate_power_form(form):
		return form
	def _populate_sanitation_form(form):
		return form
	def _populate_communication_form(form):
		return form
	def _populate_cold_storage_form(form):
		return form
	def _populate_vehicle_form(form):
		del form.avg
		return form
	def _populate_boat_form(form):
		del form.avg
		return form

	if structure_type == 'B':
		form = _populate_building_form(forms.BuildingInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'WSR':
		form = _populate_water_source_form(forms.WaterSourceInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'WST':
		form = _populate_water_storage_form(forms.WaterStorageInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'P':
		form = _populate_power_form(forms.PowerInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'S':
		form = _populate_sanitation_form(forms.SanitationInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'C':
		form = _populate_communication_form(forms.CommunicationInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'CST':
		form = _populate_cold_storage_form(forms.ColdStorageInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'V':
		form = _populate_vehicle_form(forms.VehicleInspectionForm(structureID=structureID, obj=inspection_obj))
	elif structure_type == 'BO':
		form = _populate_boat_form(forms.BoatInspectionForm(structureID=structureID, obj=inspection_obj))
	else:
		logger.error('Caught unsupported structure_type: {0}'.format(structure_type))

	return form


def _add_structure(form, structure_type):

	def _add_building(form):
		structure = sm.BuildingInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									area=form.area.data,
									roofID=form.roofID.data,
									constructionID=form.constructionID.data)
		return structure

	def _add_water_source(form):
		structure = sm.WaterSourceInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									capacity=form.capacity.data)
		return structure

	def _add_water_storage(form):
		structure = sm.WaterStorageInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									capacity=form.capacity.data)
		return structure

	def _add_power(form):
		structure = sm.PowerInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)
		return structure

	def _add_sanitation(form):
		structure = sm.SanitationInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)
		return structure

	def _add_communication(form):
		structure = sm.CommunicationInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)
		return structure

	def _add_cold_storage(form):
		structure = sm.ColdStorageInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)
		return structure

	def _add_vehicle(form):
		structure = sm.VehicleInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									vin=form.vin.data,
									plate=form.plate.data,
									engineSize=form.engineSize.data,
									engineNum=form.engineNum.data,
									engineTypeID=form.engineTypeID.data,
									gearboxTypeID=form.gearboxTypeID.data)
		return structure

	def _add_boat(form):
		structure = sm.BoatInfo.create(facilityID=form.facilityID.data,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									assetNum=form.assetNum.data)
		return structure



	if structure_type == 'B':
		structure = _add_building(form)
	elif structure_type == 'WSR':
		structure = _add_water_source(form)
	elif structure_type == 'WST':
		structure = _add_water_storage(form)
	elif structure_type == 'P':
		structure = _add_power(form)				
	elif structure_type == 'S':
		structure = _add_sanitation(form)
	elif structure_type == 'C':
		structure = _add_communication(form)
	elif structure_type == 'CST':
		structure = _add_cold_storage(form)
	elif structure_type == 'V':
		structure = _add_vehicle(form)
	elif structure_type == 'BO':
		structure = _add_boat(form)
	else:
		logger.error('Caught unsupported structure_type: {0}'.format(structure_type))
		flash('The received structure_type "{0}" is not an accepted structure_type and your request can not be processed', 'danger')
		return None

	logger.info('Created structureID: {0}'.format(structure.id), structure=structure, action='create')


def _update_structure(form, structure_type, structure_obj):

	def _update_building(structure_obj, form):
		sm.BuildingInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									area=form.area.data,
									roofID=form.roofID.data,
									constructionID=form.constructionID.data)

	def _update_water_source(structure_obj, form):
		sm.WaterSourceInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									capacity=form.capacity.data)

	def _update_water_storage(structure_obj, form):
		sm.WaterStorageInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									capacity=form.capacity.data)

	def _update_power(structure_obj, form):
		sm.PowerInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)

	def _update_sanitation(structure_obj, form):
		sm.SanitationInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)

	def _update_communication(structure_obj, form):
		sm.CommunicationInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)

	def _update_cold_storage(structure_obj, form):
		sm.ColdStorageInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data)

	def _update_vehicle(structure_obj, form):
		sm.VehicleInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									vin=form.vin.data,
									plate=form.plate.data,
									engineSize=form.engineSize.data,
									engineNum=form.engineNum.data,
									engineTypeID=form.engineTypeID.data,
									gearboxTypeID=form.gearboxTypeID.data)

	def _update_boat(structure_obj, form):
		sm.BoatInfo.update(structure_obj,
									status=form.status.data,
									typeID=form.typeID.data,
									number=form.number.data,
									description=form.description.data,
									dateConstructed=form.dateConstructed.data,
									assetNum=form.assetNum.data)
		

	if structure_type == 'B':
		_update_building(structure_obj, form)
	elif structure_type == 'WSR':
		_update_water_source(structure_obj, form)
	elif structure_type == 'WST':
		_update_water_storage(structure_obj, form)
	elif structure_type == 'P':
		_update_power(structure_obj, form)
	elif structure_type == 'S':
		_update_sanitation(structure_obj, form)
	elif structure_type == 'C':
		_update_communication(structure_obj, form)
	elif structure_type == 'CST':
		_update_cold_storage(structure_obj, form)
	elif structure_type == 'V':
		_update_vehicle(structure_obj, form)
	elif structure_type == 'BO':
		_update_boat(structure_obj, form)
	else:
		logger.error('Caught unsupported structure_type: {0}'.format(structure_type))
		flash('The received structure_type "{0}" is not an accepted structure_type and your request can not be processed', 'danger')
		return None
	
	logger.info('Updated structureID: {0}'.format(structure_obj.id), structure=structure_obj, action='update')


def _condition_calculatation(form):
		"""Calculate the building's average condition.
			The form variables must end with 'Cond' for the calculation to work.

			example:
				windowCond
				shutterCond

		"""
		components = []
		for field in form:
			if field.name.endswith('Cond') and field.data != 0 and field.data is not None:
				components.append(field.data)
		try:
			condition = int( sum(components) / len(components) )
		except ZeroDivisionError:
			condition = 0
		except TypeError:
			"""I found this when an invalid form field was present resulting in a
			NULL response from the submitted form for this field.

			Also, this function is used to calculate the value of the "avg" field
			in the database tables for structure inspections and the "avg" field
			can not be NULL so no worry about an empty "condition" value getting
			into the database. 

			We will create another check later to make sure it doesn't happen though
			through a malformed Excel upload or something in the future.
			"""
			logger.error('TypeError trying to divide the sum of the list by the length of the list: ({0})'.format(components))
			flash('There was an error and a message was logged. Please review the form and try submitting again. The condition was not calculated and you will have to fix your form and resubmit.', 'danger')
		except:
			logger.error('Error with list: ({0})'.format(components))
			flash('There was an error and a message was logged. Please review the form and try submitting again. The condition was not calculated and you will have to fix your form and resubmit.', 'danger')


		return condition


def _add_inspection(form, structure_type):

	def _add_building_inspection(form):
		buildingCond = _condition_calculatation(form)
		inspection = sm.BuildingInspection.create(structureID=form.structureID.data,
									compilationDate=datetime.datetime.now(), 
									inspectionDate=form.inspectionDate.data, 
									inspectorName=form.inspectorName.data,
									avg=buildingCond, #  calculated value removed from Form
									visibleSub=form.visibleSub.data,
									subCond=form.subCond.data,
									subNote=form.subNote.data,
									inWallCond=form.inWallCond.data,
									inWallNote=form.inWallNote.data,
									exWallCond=form.exWallCond.data,
									exWallNote=form.exWallNote.data,
									windowCond=form.windowCond.data,
									windowNote=form.windowNote.data,
									shutterCond=form.shutterCond.data,
									shutterNote=form.shutterNote.data,
									inDoorCond=form.inDoorCond.data,
									inDoorNote=form.inDoorNote.data,
									exDoorCond=form.exDoorCond.data,
									exDoorNote=form.exDoorNote.data,
									floorCond=form.floorCond.data,
									floorNote=form.floorNote.data,
									verandahCond=form.verandahCond.data,
									verandahNote=form.verandahNote.data,
									cycloneTies=form.cycloneTies.data,
									cycloneScrews=form.cycloneScrews.data,
									roofCond=form.roofCond.data,
									roofNote=form.roofNote.data,
									roofFrameCond=form.roofFrameCond.data,
									roofFrameNote=form.roofFrameNote.data,
									ceilingCond=form.ceilingCond.data,
									ceilingNote=form.ceilingNote.data,
									fasciaCond=form.fasciaCond.data,
									fasciaNote=form.fasciaNote.data,
									gutterCond=form.gutterCond.data,
									gutterNote=form.gutterNote.data,
									columnCond=form.columnCond.data,
									columnNote=form.columnNote.data,
									beamCond=form.beamCond.data,
									beamNote=form.beamNote.data,
									electricalCond=form.electricalCond.data,
									electricalNote=form.electricalNote.data,
									plumbingCond=form.plumbingCond.data,
									plumbingNote=form.plumbingNote.data)
		return inspection
		

	def _add_water_source_inspection(form):
		inspection = sm.WaterSourceInspection.create(structureID=form.structureID.data,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data)
		return inspection

	def _add_water_storage_inspection(form):
		inspection = sm.WaterStorageInspection.create(structureID=form.structureID.data,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data)
		return inspection

	def _add_power_inspection(form):
		inspection = sm.PowerInspection.create(structureID=form.structureID.data,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data)
		return inspection

	def _add_sanitation_inspection(form):
		inspection = sm.SanitationInspection.create(structureID=form.structureID.data,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data)
		return inspection

	def _add_communication_inspection(form):
		inspection = sm.CommunicationInspection.create(structureID=form.structureID.data,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data)
		return inspection

	def _add_cold_storage_inspection(form):
		inspection = sm.ColdStorageInspection.create(structureID=form.structureID.data,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data)
		return inspection

	def _add_vehicle_inspection(form):
		condition = _condition_calculatation(form)
		inspection = sm.VehicleInspection.create(structureID=form.structureID.data,
									avg=condition,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									exteriorCond=form.exteriorCond.data,
									exteriorNote=form.exteriorNote.data,
									interiorCond=form.interiorCond.data,
									interiorNote=form.interiorNote.data,
									lightsCond=form.lightsCond.data,
									lightsNote=form.lightsNote.data,
									brakeCond=form.brakeCond.data,
									brakeNote=form.brakeNote.data,
									steeringCond=form.steeringCond.data,
									steeringNote=form.steeringNote.data,
									suspensionCond=form.suspensionCond.data,
									suspensionNote=form.suspensionNote.data,
									exhaustCond=form.exhaustCond.data,
									exhaustNote=form.exhaustNote.data,
									tyresCond=form.tyresCond.data,
									tyresNote=form.tyresNote.data,
									fuelSystemCond=form.fuelSystemCond.data,
									fuelSystemNote=form.fuelSystemNote.data,
									mileage=form.mileage.data)
		return inspection

	def _add_boat_inspection(form):
		condition = _condition_calculatation(form)
		inspection = sm.BoatInspection.create(structureID=form.structureID.data,
									avg=condition,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									engineCond=form.engineCond.data,
									engineNote=form.engineNote.data,
									hullCond=form.hullCond.data,
									hullNote=form.hullNote.data)
		return inspection

	if structure_type == 'B':
		inspection = _add_building_inspection(form)
	elif structure_type == 'WSR':
		inspection = _add_water_source_inspection(form)
	elif structure_type == 'WST':
		inspection = _add_water_storage_inspection(form)
	elif structure_type == 'P':
		inspection = _add_power_inspection(form)				
	elif structure_type == 'S':
		inspection = _add_sanitation_inspection(form)
	elif structure_type == 'C':
		inspection = _add_communication_inspection(form)
	elif structure_type == 'CST':
		inspection = _add_cold_storage_inspection(form)
	elif structure_type == 'V':
		inspection = _add_vehicle_inspection(form)
	elif structure_type == 'BO':
		inspection = _add_boat_inspection(form)
	else:
		logger.error('Caught unsupported structure_type: {0}'.format(structure_type))
		flash('The received structure_type "{0}" is not an accepted structure_type and your request can not be processed', 'danger')
		return None

	logger.info('Created inspectionID: {0}'.format(inspection.id), inspection=inspection, action='create')


def _update_inspection(form, structure_type, inspection_obj):

	def _update_building_inspection(form, inspection_obj):
		buildingCond = _condition_calculatation(form)
		sm.BuildingInspection.update(inspection_obj,
									compilationDate=datetime.datetime.now(), 
									inspectionDate=form.inspectionDate.data, 
									inspectorName=form.inspectorName.data,
									avg=buildingCond,  # calculated value removed from Form
									visibleSub=form.visibleSub.data,
									subCond=form.subCond.data,
									subNote=form.subNote.data,
									inWallCond=form.inWallCond.data,
									inWallNote=form.inWallNote.data,
									exWallCond=form.exWallCond.data,
									exWallNote=form.exWallNote.data,
									windowCond=form.windowCond.data,
									windowNote=form.windowNote.data,
									shutterCond=form.shutterCond.data,
									shutterNote=form.shutterNote.data,
									inDoorCond=form.inDoorCond.data,
									inDoorNote=form.inDoorNote.data,
									exDoorCond=form.exDoorCond.data,
									exDoorNote=form.exDoorNote.data,
									floorCond=form.floorCond.data,
									floorNote=form.floorNote.data,
									verandahCond=form.verandahCond.data,
									verandahNote=form.verandahNote.data,
									cycloneTies=form.cycloneTies.data,
									cycloneScrews=form.cycloneScrews.data,
									roofCond=form.roofCond.data,
									roofNote=form.roofNote.data,
									roofFrameCond=form.roofFrameCond.data,
									roofFrameNote=form.roofFrameNote.data,
									ceilingCond=form.ceilingCond.data,
									ceilingNote=form.ceilingNote.data,
									fasciaCond=form.fasciaCond.data,
									fasciaNote=form.fasciaNote.data,
									gutterCond=form.gutterCond.data,
									gutterNote=form.gutterNote.data,
									columnCond=form.columnCond.data,
									columnNote=form.columnNote.data,
									beamCond=form.beamCond.data,
									beamNote=form.beamNote.data,
									electricalCond=form.electricalCond.data,
									electricalNote=form.electricalNote.data,
									plumbingCond=form.plumbingCond.data,
									plumbingNote=form.plumbingNote.data)

	def _update_water_source_inspection(form, inspection_obj):
		sm.WaterSourceInspection.update(inspection_obj,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									)

	def _update_water_storage_inspection(form, inspection_obj):
		sm.WaterStorageInspection.update(inspection_obj,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									)

	def _update_power_inspection(form, inspection_obj):
		sm.PowerInspection.update(inspection_obj,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									)

	def _update_sanitation_inspection(form, inspection_obj):
		sm.SanitationInspection.update(inspection_obj,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									)

	def _update_communication_inspection(form, inspection_obj):
		sm.CommunicationInspection.update(inspection_obj,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									)

	def _update_cold_storage_inspection(form, inspection_obj):
		sm.ColdStorageInspection.update(inspection_obj,
									avg=form.avg.data,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									)

	def _update_vehicle_inspection(form, inspection_obj):
		condition = _condition_calculatation(form)
		sm.VehicleInspection.update(inspection_obj,
									avg=condition,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									exteriorCond=form.exteriorCond.data,
									exteriorNote=form.exteriorNote.data,
									interiorCond=form.interiorCond.data,
									interiorNote=form.interiorNote.data,
									lightsCond=form.lightsCond.data,
									lightsNote=form.lightsNote.data,
									brakeCond=form.brakeCond.data,
									brakeNote=form.brakeNote.data,
									steeringCond=form.steeringCond.data,
									steeringNote=form.steeringNote.data,
									suspensionCond=form.suspensionCond.data,
									suspensionNote=form.suspensionNote.data,
									exhaustCond=form.exhaustCond.data,
									exhaustNote=form.exhaustNote.data,
									tyresCond=form.tyresCond.data,
									tyresNote=form.tyresNote.data,
									fuelSystemCond=form.fuelSystemCond.data,
									fuelSystemNote=form.fuelSystemNote.data,
									mileage=form.mileage.data)

	def _update_boat_inspection(form, inspection_obj):
		condition = _condition_calculatation(form)
		sm.BoatInspection.update(inspection_obj,
									structureID=form.structureID.data,
									avg=condition,
									note=form.note.data,
									inspectionDate=form.inspectionDate.data,
									compilationDate=datetime.datetime.now(),
									inspectorName=form.inspectorName.data,
									engineCond=form.engineCond.data,
									engineNote=form.engineNote.data,
									hullCond=form.hullCond.data,
									hullNote=form.hullNote.data)



	if structure_type == 'B':
		_update_building_inspection(form, inspection_obj)
	elif structure_type == 'WSR':
		_update_water_source_inspection(form, inspection_obj)
	elif structure_type == 'WST':
		_update_water_storage_inspection(form, inspection_obj)
	elif structure_type == 'P':
		_update_power_inspection(form, inspection_obj)				
	elif structure_type == 'S':
		_update_sanitation_inspection(form, inspection_obj)
	elif structure_type == 'C':
		_update_communication_inspection(form, inspection_obj)
	elif structure_type == 'CST':
		_update_cold_storage_inspection(form, inspection_obj)
	elif structure_type == 'V':
		_update_vehicle_inspection(form, inspection_obj)
	elif structure_type == 'BO':
		_update_boat_inspection(form, inspection_obj)
	else:
		logger.error('Caught unsupported structure_type: {0}'.format(structure_type))
		flash('The received structure_type "{0}" is not an accepted structure_type and your request can not be processed', 'danger')
		return None

	logger.info('Updated inspectionID: {0}'.format(inspection_obj.id), inspection=inspection_obj, action='update')



#
#
#
#
#
#
#
#
#
# TODO: IS this used anywhere
def download_building_inspection(facilityID, buildingID, inspectionID):
	building = models.BuildingInformation.query.filter_by(buildingID=buildingID).one()
	inspection = models.BuildingInspectionInformation.query.filter_by(inspectionID=inspectionID).one()
	
	# Create an Excel file in memory
	wb = openpyxl.workbook.Workbook()
	ws = wb.active

	from openpyxl.formatting.rule import ColorScale, FormatObject
	from openpyxl.styles import Color

	first = FormatObject(type='min')
	mid = FormatObject(type='num', val=40)
	last = FormatObject(type='max')

	from openpyxl.formatting.rule import ColorScaleRule
	rule = ColorScaleRule(start_type='percentile', start_value=10, start_color='a0ff6d',
						  mid_type='percentile', mid_value=50, mid_color='ffb93d',
						  end_type='percentile', end_value=90, end_color='ff8a6d')

	components = [	('Sub-Structure', inspection.subCond, inspection.subNote),
					('Interior Walls', inspection.inWallCond, inspection.inWallNote),
					('Exterior Walls', inspection.exWallCond, inspection.exWallNote),
					('Windows', inspection.windowCond, inspection.windowNote),
					('Shutters', inspection.shutterCond, inspection.shutterNote),
					('Interior Doors', inspection.inDoorCond, inspection.inDoorNote),
					('Exterior Doors', inspection.exDoorCond, inspection.exDoorNote),
					('Floor', inspection.floorCond, inspection.floorNote),
					('Verandah', inspection.verandahCond, inspection.verandahNote),
					('Roof', inspection.roofCond, inspection.roofNote),
					('Ceiling', inspection.ceilingCond, inspection.ceilingNote),
					('Fascia', inspection.fasciaCond, inspection.fasciaNote),
					('Gutter and Pipes', inspection.gutterCond, inspection.gutterNote),
					('Columns', inspection.columnCond, inspection.columnNote),
					('Beams', inspection.beamCond, inspection.beamNote),
					('Electrical', inspection.electricalCond, inspection.electricalNote),
					('Plumbing', inspection.plumbingCond, inspection.plumbingNote)
				]
	row = 5			
	for component in components:
		row = row + 1
		ws.cell(row=row, column=1, value=component[0])
		ws.cell(row=row, column=2, value=component[1])
		ws.cell(row=row, column=3, value=component[2])
		
		if component[1]:
			pass
			
	# Three color scale
	ws.conditional_formatting.add('B6:B20', rule)

	wb.save(filename=os.path.join(app.config['EXCEL_FOLDER'],'testingxlcomponents.xlsx'))

	return 'testingxlcomponents.xlsx'