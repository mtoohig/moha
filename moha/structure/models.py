# -*- coding: utf-8 -*-
"""Building models."""
#from sqlalchemy.ext.orderinglist import ordering_list

import datetime

from flask import url_for, render_template_string
from moha.facility.models import FileInfo
from moha.database import CRUDMixin, Column, Model, SurrogatePK, db, reference_col, relationship, hybrid_property, hybrid_method, synonym
from moha.extensions import bcrypt

'''
These are the database table models for structures.

Structures include:
* Buildings
* Water
* Power
* Sanitation

Each Structure has 4 tables

Tables include:
* Information
  * Basic information of the structure
* Inspection
  * Inspections conducted for the structure such as condition
* Service (except Building)
  * Connects a service structure (water/power/sanitation) to a building
* File
  * Files about a structure

'''


# ===================================== #
#  S T R U C T U R E   S E R V I C E S  #
# ===================================== #

ServiceRef = db.Table(
    'ServiceRef', Model.metadata,
    Column('serviceID', db.Integer, db.ForeignKey('structureInfo.id'), primary_key=True),
    Column('servicingID', db.Integer, db.ForeignKey('structureInfo.id'), primary_key=True)
    )


# ========================= #
#  C O M P O N E N E N T S  #
# ========================= #

class BuildingComponentInfo(Model, SurrogatePK):
    __tablename__ = 'buildingComponentInfo'
    name = Column(db.Unicode, nullable=False)


# =================== #
#  S T R U C T U R E  #
# =================== #

class StructureFileRef(Model):
    __tablename__ = 'structureFileRef'
    fileID = reference_col('fileInfo', primary_key=True)
    file = relationship('FileInfo', backref='structureFiles')

    id = synonym('structureID')
    structureID = reference_col('structureInfo', primary_key=True)
    structure = relationship('StructureInfo', back_populates='files')

    default = Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return '<StructureFileRef(fileID={0}, structureID={1}, default={2})>'.format(self.fileID, self.structureID, self.default)


class BuildingRef(Model, SurrogatePK):
    __tablename__ = 'buildingRef'
    type = Column(db.Unicode)
    criticality = Column(db.Integer)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<BuildingRef(type={0}, criticality={1})>'.format(self.type, self.criticality)


class RoofRef(Model, SurrogatePK):
    __tablename__ = 'roofRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<RoofRef(type={0})>'.format(self.type)


class ConstructionRef(Model, SurrogatePK):
    __tablename__ = 'constructionRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<ConstructionRef(type={0})>'.format(self.type)


class WaterSourceRef(Model, SurrogatePK):
    __tablename__ = 'waterSourceRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<WaterSourceRef(type={0})>'.format(self.type)


class WaterStorageRef(Model, SurrogatePK):
    __tablename__ = 'waterStorageRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<WaterStorageRef(type={0})>'.format(self.type)


class PowerRef(Model, SurrogatePK):
    __tablename__ = 'powerRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<PowerRef(type={0})>'.format(self.type)


class SanitationRef(Model, SurrogatePK):
    __tablename__ = 'sanitationRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<SanitationRef(type={0})>'.format(self.type)


class CommunicationRef(Model, SurrogatePK):
    __tablename__ = 'communicationRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<CommunicationRef(type={0})>'.format(self.type)


class ColdStorageRef(Model, SurrogatePK):
    __tablename__ = 'coldStorageRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<ColdStorageRef(type={0})>'.format(self.type)


class VehicleRef(Model, SurrogatePK):
    __tablename__ = 'vehicleRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<VehicleRef(type={0})>'.format(self.type)


class EngineTypeRef(Model, SurrogatePK):
    __tablename__ = 'engineTypeRef'
    type = Column(db.Unicode, nullable=False)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<EngineType(type={0})>'.format(self.type)


class GearboxTypeRef(Model, SurrogatePK):
    __tablename__ = 'gearboxTypeRef'
    type = Column(db.Unicode, nullable=False)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<GearboxType(type={0})>'.format(self.type)


class BoatRef(Model, SurrogatePK):
    __tablename__ = 'boatRef'
    type = Column(db.Unicode)

    def __unicode__(self):
        return self.type

    def __repr__(self):
        return '<BoatRef(type={0})>'.format(self.type)


class StructureInfo(Model, SurrogatePK):
    __tablename__ = 'structureInfo'
    structure_type = Column(db.Unicode(4), nullable=False)
    __mapper_args__ = {'polymorphic_on': structure_type}
    
    number = Column(db.Integer)
    description = Column(db.Unicode, nullable=True)
    dateConstructed = Column(db.Date, nullable=True)
    status = Column(db.Boolean, nullable=False, default=True)

    functional_areas = relationship('FunctionalAreaLocationRef',
                            cascade='save-update, merge, delete',
                            back_populates='structure')
    
    @hybrid_property
    def identifier(self):
        return '{0}-{1} {2}'.format(self.structure_type, self.number, self.type.type)

    @hybrid_property
    def inspection(self):
        if self.inspections:
            return self.inspections[0]
        else:
            return None

    @hybrid_property
    def recent_inspections(self):
        if self.inspections:
            return self.inspections[:3]
        else:
            return None

    @hybrid_property
    def condition(self):
        if self.inspections:
            return self.inspections[0].condition
        else:
            return None

    @hybrid_property
    def default_pic(self):
        if self.inspection:
            try:
                return [pic for pic in self.inspection.pics if pic.default==True][0]  # only return one pic
            except IndexError:  # except no pictures yet
                return None
        else:  # else no inspections yet
            return None

    inspections = relationship('StructureInspection', 
                            order_by='StructureInspection.inspectionDate.desc()', 
                            cascade="save-update, merge, delete",
                            back_populates='structure')

    files = relationship('StructureFileRef', cascade="save-update, merge, delete")

    services = db.relationship('StructureInfo', 
                            secondary=ServiceRef, 
                            primaryjoin='structureInfo.c.id == ServiceRef.c.serviceID', 
                            secondaryjoin='structureInfo.c.id == ServiceRef.c.servicingID',
                            backref=db.backref('servicing'))

    facilityID = reference_col('facilityInfo', pk_name='facilityID')
    facility = db.relationship('FacilityInfo', back_populates='structures')

    @property
    def structure_info(self):
        return [dict(name='number',value=self.number),
                dict(name='description',value=self.description),
                dict(name='Date Available', value=self.dateConstructed),
                dict(name='Status', value='Operational' if self.status else 'Non-Operational')]

    def _empty_popup_html(self):

        template = """<div style="min-width: 200px;" id="popup-id-{{structure.id}}">
                        <h5><a href="{{link}}">{{structure.identifier}}</a></h5>
                          <div class="thumbnail">
                            <img src="{{ url_for('static', filename='img/loading.gif') }}" class="img-rounded img-responsive">
                          </div>
                       </div>
                   """
        return render_template_string(template, linke=url_for('facility.facility', facilityID=self.facilityID), structure=self)

    def as_dict(self):
        data = {}
        #data = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        data['structureID'] = self.id
        data['facilityID'] = self.facilityID
        data['facilityName'] = self.facility.facilityName
        data['latitude'] = self.facility.latitude
        data['longitude'] = self.facility.longitude
        data['facilityLink'] = url_for('facility.facility', facilityID=self.facilityID)
        data['condition'] = self.condition
        data['empty_popup'] = self._empty_popup_html()
        data['identifier'] = self.identifier
        data['second_line'] = self.facility.identifier
        data['icon'] = self.icon
        data['color'] = self.color
        return data

    def __unicode__(self):
        return '{0} at facility: {1}'.format(self.identifier, self.facility.facilityID)

    def __repr__(self):
        return '<StructureInfo(structure_type={0}, number={1}, description={2}, status={3})>'.format(self.structure_type, self.number, self.description, self.status)


class BuildingInfo(StructureInfo):
    __tablename__ = 'buildingInfo'
    __mapper_args__ = {'polymorphic_identity': u'B'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'home'
    color = 'black'
    single_name = 'building'
    plural_name = 'buildings'

    area = Column(db.Integer)
    
    constructionID = reference_col('constructionRef')
    construction = relationship('ConstructionRef', backref='buildings')

    roofID = reference_col('roofRef')
    roof = relationship('RoofRef', backref='buildings')

    typeID = reference_col('buildingRef')
    type = relationship('BuildingRef', backref='buildings')

    facility = relationship('FacilityInfo', backref='buildings')

    @hybrid_property
    def criticality(self):
        return self.type.criticality

    @hybrid_property
    def basic_info(self):
        return self.structure_info + [dict(name='area', value=self.area),
                                      dict(name='construction', value=self.construction),
                                      dict(name='roof', value=self.roof)]


class WaterSourceInfo(StructureInfo):
    __tablename__ = 'waterSourceInfo'
    __mapper_args__ = {'polymorphic_identity': u'WSR'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'tint'
    color = 'blue'
    single_name = 'water source'
    plural_name = 'water sources'

    capacity = Column(db.Integer, nullable=False)

    typeID = reference_col('waterSourceRef')
    type = relationship('WaterSourceRef', backref='waterSources')

    facility = relationship('FacilityInfo', backref='waterSources')

    @hybrid_property
    def basic_info(self):
        return self.structure_info + [dict(name='capacity', value=self.capacity)]


class WaterStorageInfo(StructureInfo):
    __tablename__ = 'waterStorageInfo'
    __mapper_args__ = {'polymorphic_identity': u'WST'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'tint'
    color = 'blue'
    single_name = 'water storage'
    plural_name = 'water storages'

    capacity = Column(db.Integer)

    typeID = reference_col('waterStorageRef')
    type = relationship('WaterStorageRef', backref='waterStorages')

    facility = relationship('FacilityInfo', backref='waterStorages')

    @hybrid_property
    def basic_info(self):
        return self.structure_info + [dict(name='capacity', value=self.capacity)]


class PowerInfo(StructureInfo):
    __tablename__ = 'powerInfo'
    __mapper_args__ = {'polymorphic_identity': u'P'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'bolt'
    color = 'orange'
    single_name = 'power'
    plural_name = 'powers'

    typeID = reference_col('powerRef')
    type = relationship('PowerRef', backref='powers')

    facility = relationship('FacilityInfo', backref='powers')

    @hybrid_property
    def basic_info(self):
        return self.structure_info


class SanitationInfo(StructureInfo):
    __tablename__ = 'sanitationInfo'
    __mapper_args__ = {'polymorphic_identity': u'S'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'shower'
    color = 'gray'
    single_name = 'sanitation'
    plural_name = 'sanitations'


    typeID = reference_col('sanitationRef')
    type = relationship('SanitationRef', backref='sanitations')

    facility = relationship('FacilityInfo', backref='sanitations')

    @hybrid_property
    def basic_info(self):
        return self.structure_info


class CommunicationInfo(StructureInfo):
    __tablename__ = 'communicationInfo'
    __mapper_args__ = {'polymorphic_identity': u'C'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'phone'
    color = 'green'
    single_name = 'communication'
    plural_name = 'communications'

    typeID = reference_col('communicationRef')
    type = relationship('CommunicationRef', backref='communications')

    facility = relationship('FacilityInfo', backref='communications')

    @hybrid_property
    def basic_info(self):
        return self.structure_info


class ColdStorageInfo(StructureInfo):
    __tablename__ = 'coldStorageInfo'
    __mapper_args__ = {'polymorphic_identity': u'CST'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    icon = 'snowflake-o'
    color = 'lightblue'
    single_name = 'cold storage'
    plural_name = 'cold storages'

    typeID = reference_col('coldStorageRef')
    type = relationship('ColdStorageRef', backref='coldStorages')

    facility = relationship('FacilityInfo', backref='coldStorages')

    @hybrid_property
    def basic_info(self):
        return self.structure_info


class TransportationInfo(StructureInfo):
    __tablename__ = 'transportationInfo'
    __mapper_args__ = {'polymorphic_identity': u'T'}
    id = Column(None, db.ForeignKey('structureInfo.id'), primary_key=True)
    single_name = 'transportation'
    plural_name = 'transportations'

    facility = relationship('FacilityInfo', backref='transportations')

    @hybrid_property
    def basic_info(self):
        return self.structure_info


class VehicleInfo(TransportationInfo):
    __tablename__ = 'vehicleInfo'
    __mapper_args__ = {'polymorphic_identity': u'V'}
    id = Column(None, db.ForeignKey('structureInfo.id'), db.ForeignKey('transportationInfo.id'), primary_key=True)
    icon = 'car'
    color = 'red'
    single_name = 'vehicle'
    plural_name = 'vehicles'

    brand = Column(db.Unicode)
    vin = Column(db.Unicode)
    plate = Column(db.Unicode)
    engineSize = Column(db.Unicode)
    engineNum = Column(db.Unicode)
    engineSize = Column(db.Unicode)

    engineTypeID = reference_col('engineTypeRef')
    engineType = relationship('EngineTypeRef', backref='vehicles')

    gearboxTypeID = reference_col('gearboxTypeRef')
    gearboxType = relationship('GearboxTypeRef', backref='vehicles')

    typeID = reference_col('vehicleRef')
    type = relationship('VehicleRef', backref='vehicles')
    
    facility = relationship('FacilityInfo', backref='vehicles')

    @hybrid_property
    def basic_info(self):
        return self.structure_info + [dict(name='Brand', value=self.brand),
                                      dict(name='VIN Number', value=self.vin),
                                      dict(name='Plate Number', value=self.plate),
                                      dict(name='Engine Size', value=self.engineSize),
                                      dict(name='Engine Number', value=self.engineNum),
                                      dict(name='Engine Type', value=self.engineType.type),
                                      dict(name='Gearbox Type', value=self.gearboxType.type)]


class BoatInfo(TransportationInfo):
    __tablename__ = 'boatInfo'
    __mapper_args__ = {'polymorphic_identity': u'BO'}
    id = Column(None, db.ForeignKey('structureInfo.id'), db.ForeignKey('transportationInfo.id'), primary_key=True)
    icon = 'ship'
    color = 'pink'
    single_name = 'boat'
    plural_name = 'boats'

    assetNum = Column(db.Unicode)

    typeID = reference_col('boatRef')
    type = relationship('BoatRef', backref='boats')

    facility = relationship('FacilityInfo', backref='boats')

    @hybrid_property
    def basic_info(self):
        return self.structure_info + [dict(name='Number', value=self.assetNum)]



# ========================================= #
#  S T R U C T U R E   I N S P E C T I O N  #
# ========================================= #

class InspectionPicRef(Model):
    __tablename__ = 'inspectionPicRef'
    fileID = reference_col('fileInfo', primary_key=True)
    file = relationship('FileInfo', backref='inspectionPics')

    id = synonym('inspectionID')
    inspectionID = reference_col('structureInspection', primary_key=True)
    inspection = relationship('StructureInspection', back_populates='pics')

    default = Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return '<StructurePicRef(fileID={0}, inspectionID={1}, default={2})>'.format(self.fileID, self.inspectionID, self.default)


class InspectionFileRef(Model):
    __tablename__ = 'inspectionFileRef'
    fileID = reference_col('fileInfo', primary_key=True)
    file = relationship('FileInfo', backref='inspectionFiles')

    id = synonym('inspectionID')
    inspectionID = reference_col('structureInspection', primary_key=True)
    inspection = relationship('StructureInspection', back_populates='files')

    default = Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return '<StructureFileRef(fileID={0}, inspectionID={1}, default={2})>'.format(self.fileID, self.inspectionID, self.default)


class StructureInspection(Model, SurrogatePK):
    __tablename__ = 'structureInspection'
    structure_type = Column(db.Unicode(4), nullable=False)
    __mapper_args__ = {'polymorphic_on': structure_type, 'polymorphic_identity': u''}  ## Remove identity later if useless

    structureID = reference_col('structureInfo')
    structure = relationship('StructureInfo', back_populates='inspections')

    avg = Column(db.Integer, nullable=False)
    note = Column(db.Unicode, nullable=True)

    inspectionDate = Column(db.Date, nullable=False, default=datetime.datetime.now)
    compilationDate = Column(db.Date, nullable=False, default=datetime.datetime.now)
    inspectorName = Column(db.Unicode, nullable=False)

    files = relationship('InspectionFileRef', cascade="save-update, merge, delete")
    pics = relationship('InspectionPicRef', cascade="save-update, merge, delete")

    @hybrid_property
    def age(self):
        return (datetime.date.today() - self.inspectionDate).days

    @hybrid_property
    def condition(self):
        return self.avg

    def __unicode__(self):
        return 'Date: {0}, condition: {1}'.format(self.inspectionDate, self.condition)

    def __repr__(self):
        return '<StructureInspection(structure_type={0}, structureID={1}, avg={2}, note={3})>'.format(self.structure_type, self.structureID, self.avg, self.note)


class BuildingInspection(StructureInspection):
    __tablename__ = 'buildingInspection'
    __mapper_args__ = {'polymorphic_identity': u'B'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)

    cycloneTies = db.Column(db.Boolean)
    cycloneScrews = db.Column(db.Boolean)
    visibleSub = db.Column(db.Boolean)
    subCond = db.Column(db.Integer)
    subNote = db.Column(db.Unicode)
    inWallCond = db.Column(db.Integer)
    inWallNote = db.Column(db.Unicode)
    exWallCond = db.Column(db.Integer)
    exWallNote = db.Column(db.Unicode)
    windowCond = db.Column(db.Integer)
    windowNote = db.Column(db.Unicode)
    shutterCond = db.Column(db.Integer)
    shutterNote = db.Column(db.Unicode)
    inDoorCond = db.Column(db.Integer)
    inDoorNote = db.Column(db.Unicode)
    exDoorCond = db.Column(db.Integer)
    exDoorNote = db.Column(db.Unicode)
    floorCond = db.Column(db.Integer)
    floorNote = db.Column(db.Unicode)
    verandahCond = db.Column(db.Integer)
    verandahNote = db.Column(db.Unicode)
    roofCond = db.Column(db.Integer)
    roofNote = db.Column(db.Unicode)
    roofFrameCond = db.Column(db.Integer)
    roofFrameNote = db.Column(db.Unicode)
    ceilingCond = db.Column(db.Integer)
    ceilingNote = db.Column(db.Unicode)
    fasciaCond = db.Column(db.Integer)
    fasciaNote = db.Column(db.Unicode)
    gutterCond = db.Column(db.Integer)
    gutterNote = db.Column(db.Unicode)
    columnCond = db.Column(db.Integer)
    columnNote = db.Column(db.Unicode)
    beamCond = db.Column(db.Integer)
    beamNote = db.Column(db.Unicode)
    electricalCond = db.Column(db.Integer)
    electricalNote = db.Column(db.Unicode)
    plumbingCond = db.Column(db.Integer)
    plumbingNote = db.Column(db.Unicode)

    @hybrid_property
    def condition(self):
        score = sum([self.inWallCond,
                self.exWallCond,
                self.windowCond,
                self.shutterCond,
                self.inDoorCond,
                self.exDoorCond,
                self.floorCond,
                self.verandahCond,
                self.roofCond,
                self.roofFrameCond,
                self.ceilingCond,
                self.fasciaCond,
                self.gutterCond,
                self.columnCond,
                self.beamCond,
                self.electricalCond,
                self.plumbingCond,
                self.subCond])
        for True in [self.cycloneScrews, self.cycloneTies]:
            score += 1
        score = score / float(self.structure.type.criticality)
        if score <= 20:
            return 1
        elif score <= 38:
            return 2
        elif score <= 58:
            return 3
        elif score <= 76:
            return 4
        else:
            return 5

    @property
    def components(self):
        """Components of the inspection for this structure_type."""
        return [{'name':'Cyclone Ties','condition':self.cycloneTies},
                {'name':'Cyclone Screws','condition':self.cycloneScrews},
                {'name':'Visible Sub','condition':self.visibleSub},
                {'name':'Substructure','condition':self.subCond,'note':self.subNote},
                {'name':'Interior Walls','condition':self.inWallCond,'note':self.inWallNote},
                {'name':'Exterior Walls','condition':self.exWallCond,'note':self.exWallNote},
                {'name':'Windows','condition':self.windowCond,'note':self.windowNote},
                {'name':'Shutters','condition':self.shutterCond,'note':self.shutterNote},
                {'name':'Interior Doors','condition':self.inDoorCond,'note':self.inDoorNote},
                {'name':'Exterior Doors','condition':self.exDoorCond,'note':self.exDoorNote},
                {'name':'Floor','condition':self.floorCond,'note':self.floorNote},
                {'name':'Verandah','condition':self.verandahCond,'note':self.verandahNote},
                {'name':'Roof','condition':self.roofCond,'note':self.roofNote},
                {'name':'Roof Franmework','condition':self.roofFrameCond,'note':self.roofFrameNote},
                {'name':'Ceiling','condition':self.ceilingCond,'note':self.ceilingNote},
                {'name':'Fascia','condition':self.fasciaCond,'note':self.fasciaNote},
                {'name':'Gutter and Pipes','condition':self.gutterCond,'note':self.gutterNote},
                {'name':'Columns','condition':self.columnCond,'note':self.columnNote},
                {'name':'Beams','condition':self.beamCond,'note':self.beamNote},
                {'name':'Electrical','condition':self.electricalCond,'note':self.electricalNote},
                {'name':'Plumbing','condition':self.plumbingCond,'note':self.plumbingNote}]

class WaterSourceInspection(StructureInspection):
    __tablename__ = 'waterSourceInspection'
    __mapper_args__ = {'polymorphic_identity': u'WSR'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)


class WaterStorageInspection(StructureInspection):
    __tablename__ = 'waterStorageInspection'
    __mapper_args__ = {'polymorphic_identity': u'WST'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)


class PowerInspection(StructureInspection):
    __tablename__ = 'powerInspection'
    __mapper_args__ = {'polymorphic_identity': u'P'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)


class SanitationInspection(StructureInspection):
    __tablename__ = 'sanitationInspection'
    __mapper_args__ = {'polymorphic_identity': u'S'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)


class CommunicationInspection(StructureInspection):
    __tablename__ = 'communicationInspection'
    __mapper_args__ = {'polymorphic_identity': u'C'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)


class ColdStorageInspection(StructureInspection):
    __tablename__ = 'coldStorageInspection'
    __mapper_args__ = {'polymorphic_identity': u'CST'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)


class VehicleInspection(StructureInspection):
    __tablename__ = 'vehicleInspection'
    __mapper_args__ = {'polymorphic_identity': u'V'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)

    exteriorCond = Column(db.Integer)
    exteriorNote = Column(db.Unicode)
    interiorCond = Column(db.Integer)
    interiorNote = Column(db.Unicode)
    lightsCond = Column(db.Integer)
    lightsNote = Column(db.Unicode)
    brakeCond = Column(db.Integer)
    brakeNote = Column(db.Unicode)
    steeringCond = Column(db.Integer)              
    steeringNote = Column(db.Unicode)
    suspensionCond = Column(db.Integer)
    suspensionNote = Column(db.Unicode)
    exhaustCond = Column(db.Integer) 
    exhaustNote = Column(db.Unicode)
    tyresCond = Column(db.Integer)
    tyresNote = Column(db.Unicode)
    fuelSystemCond = Column(db.Integer)
    fuelSystemNote = Column(db.Unicode)

    mileage = Column(db.Float)

    @property
    def components(self):
        """Components of the inspection for this structure_type."""
        return [
                {'name':'Mileage','condition':self.mileage},
                {'name':'Exterior','condition':self.exteriorCond,'note':self.exteriorNote},
                {'name':'Interior','condition':self.interiorCond,'note':self.interiorNote},
                {'name':'Lights','condition':self.lightsCond,'note':self.lightsNote},
                {'name':'Brakes','condition':self.brakeCond,'note':self.brakeNote},
                {'name':'Steering','condition':self.steeringCond,'note':self.steeringNote},
                {'name':'Suspension','condition':self.suspensionCond,'note':self.suspensionNote},
                {'name':'Exhaust','condition':self.exhaustCond,'note':self.exhaustNote},
                {'name':'Tyres','condition':self.tyresCond,'note':self.tyresNote},
                {'name':'Fuel System','condition':self.fuelSystemCond,'note':self.fuelSystemNote}
                ]


class BoatInspection(StructureInspection):
    __tablename__ = 'boatInspection'
    __mapper_args__ = {'polymorphic_identity': u'BO'}
    id = Column(None, db.ForeignKey('structureInspection.id'), primary_key=True)

    engineCond = Column(db.Integer)
    engineNote = Column(db.Unicode)
    hullCond = Column(db.Integer)
    hullNote = Column(db.Unicode)

    @property
    def components(self):
        """Components of the inspection for this structure_type."""
        return [
                {'name':'Engine','condition':self.engineCond,'note':self.engineNote},
                {'name':'Hull','condition':self.hullCond,'note':self.hullNote}
                ]