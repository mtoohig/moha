# -*- coding: utf-8 -*-
"""Click commands."""
import os
from glob import glob
from subprocess import call

import click
from flask import current_app
from flask.cli import with_appcontext
from werkzeug.exceptions import MethodNotAllowed, NotFound

from moha.facility import models as fm
from moha.structure import models as sm
from moha.asset import models as am
from moha.personnel import models as pm

import utils

HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
TEST_PATH = os.path.join(PROJECT_ROOT, 'tests')


@click.command()
def test():
    """Run the tests."""
    import pytest
    rv = pytest.main([TEST_PATH, '--verbose'])
    exit(rv)


@click.command()
def populate_db():
    """Populate Basic Info"""
    pass

@click.command()
@click.option('-f', '--fix-imports', default=False, is_flag=True,
              help='Fix imports using isort, before linting')
def lint(fix_imports):
    """Lint and check code style with flake8 and isort."""
    skip = ['requirements']
    root_files = glob('*.py')
    root_directories = [
        name for name in next(os.walk('.'))[1] if not name.startswith('.')]
    files_and_directories = [
        arg for arg in root_files + root_directories if arg not in skip]

    def execute_tool(description, *args):
        """Execute a checking tool with its arguments."""
        command_line = list(args) + files_and_directories
        click.echo('{}: {}'.format(description, ' '.join(command_line)))
        rv = call(command_line)
        if rv != 0:
            exit(rv)

    if fix_imports:
        execute_tool('Fixing import order', 'isort', '-rc')
    execute_tool('Checking code style', 'flake8')


@click.command()
def clean():
    """Remove *.pyc and *.pyo files recursively starting at current directory.

    Borrowed from Flask-Script, converted to use Click.
    """
    for dirpath, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            if filename.endswith('.pyc') or filename.endswith('.pyo'):
                full_pathname = os.path.join(dirpath, filename)
                click.echo('Removing {}'.format(full_pathname))
                os.remove(full_pathname)


@click.command()
@click.option('--url', default=None,
              help='Url to test (ex. /static/image.png)')
@click.option('--order', default='rule',
              help='Property on Rule to order by (default: rule)')
@with_appcontext
def urls(url, order):
    """Display all of the url matching routes for the project.

    Borrowed from Flask-Script, converted to use Click.
    """
    rows = []
    column_length = 0
    column_headers = ('Rule', 'Endpoint', 'Arguments')

    if url:
        try:
            rule, arguments = (
                current_app.url_map
                           .bind('localhost')
                           .match(url, return_rule=True))
            rows.append((rule.rule, rule.endpoint, arguments))
            column_length = 3
        except (NotFound, MethodNotAllowed) as e:
            rows.append(('<{}>'.format(e), None, None))
            column_length = 1
    else:
        rules = sorted(
            current_app.url_map.iter_rules(),
            key=lambda rule: getattr(rule, order))
        for rule in rules:
            rows.append((rule.rule, rule.endpoint, None))
        column_length = 2

    str_template = ''
    table_width = 0

    if column_length >= 1:
        max_rule_length = max(len(r[0]) for r in rows)
        max_rule_length = max_rule_length if max_rule_length > 4 else 4
        str_template += '{:' + str(max_rule_length) + '}'
        table_width += max_rule_length

    if column_length >= 2:
        max_endpoint_length = max(len(str(r[1])) for r in rows)
        # max_endpoint_length = max(rows, key=len)
        max_endpoint_length = (
            max_endpoint_length if max_endpoint_length > 8 else 8)
        str_template += '  {:' + str(max_endpoint_length) + '}'
        table_width += 2 + max_endpoint_length

    if column_length >= 3:
        max_arguments_length = max(len(str(r[2])) for r in rows)
        max_arguments_length = (
            max_arguments_length if max_arguments_length > 9 else 9)
        str_template += '  {:' + str(max_arguments_length) + '}'
        table_width += 2 + max_arguments_length

    click.echo(str_template.format(*column_headers[:column_length]))
    click.echo('-' * table_width)

    for row in rows:
        click.echo(str_template.format(*row[:column_length]))


@click.command()
@with_appcontext
def thumbs():
    """Make thumbnails for all pictures.

    A development helper command"""
    for root, dirs, files in os.walk(current_app.config['PICTURE_FOLDER']):
        if not os.path.basename(root) == 'thumbs':
            if not 'thumbs' in dirs:
                os.makedirs(os.path.join(root, 'thumbs'))
            for file in files:
                if utils.allowed_picture(file):
                    if not os.path.exists(os.path.join(root, 'thumbs', file)):
                        utils.make_thumbnail(os.path.join(root, 'thumbs'), os.path.join(root, file), file)
        else:
            continue  # skip reading over files in 'thumbs' directories


@click.command()
@with_appcontext
def tupia():
    """Uncomment sections as needed"""
    import pickle
    import difflib

    """ Initial Asset Category Matches
    a = pickle.load(open('a.p', 'r'))  # tupia assets
    b = pickle.load(open('b.p', 'r'))  # known assets
    try:
        c = pickle.load(open('c.p', 'r'))
    except:
        c = {}
    for i in a:
        if i['name'] in c.keys():
            continue
        matches = difflib.get_close_matches(i['name'], b.keys())
        print i['name']
        print matches
        choice = int(raw_input(':: '))
        if choice == -1:
            c[i['name']] = 'New Asset'
            continue
        c[i['name']] = b[matches[choice]]
        pickle.dump(c, open('c.p', 'w'))
    """


    """ Make list of facilities from tupia data for matching with our facilities
    a = pickle.load(open('a.p', 'r'))  # tupia assets
    c = pickle.load(open('c.p', 'r'))  # tupia assets key with CategoryID as value
    facilities = {}
    for i in a:
        for f in i['facilities'].keys():
            if f not in facilities.keys():
                facilities[f] = None 
    pickle.dump(facilities, open('f.p', 'w'))      
    """

    """ Match tupia facility names with the offical name and attach facilityID
    f = pickle.load(open('f.p', 'r'))
    facilities = fm.FacilityInfo.query.all()
    facilities = {f.facilityName: f.facilityID for f in facilities}
    try:
        d = pickle.load(open('d.p', 'r'))
    except:
        d = {}
    for i in f.keys():
        matches = difflib.get_close_matches(i, facilities.keys())
        print i
        print matches
        choice = int(raw_input(':: '))
        if choice == -1:
            d[i] = 'Unknown Facility'
            continue
        d[i] = facilities[matches[choice]]
        pickle.dump(d, open('d.p', 'w'))
    """

    """Add tupia assets to database """
    import datetime
    a = pickle.load(open('a.p', 'r'))  # Tupia Assets
    c = pickle.load(open('c.p', 'r'))  # key=Tupia Asset Name : value=categoryID 
    d = pickle.load(open('d.p', 'r'))  # key=Tupia facility Name : value=facilityID
    bldg_id = 28  # BuildingTypeID of "Tupia-Assets"
    fa_id = 17  # FunctionalAreaID of "Tupia-Assets" 

    for i in a:  # For asset in Tupia Assets
        categoryID = c[i['name']]
        for f, num in i['facilities'].iteritems():
            facilityID = d[f]
            if facilityID == 'Unknown Facility':
                continue

            # Check if Tupia-Asset building and functionalArea exists else make it
            facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).one()
            if not bldg_id in [b.type.id for b in facility.buildings]:
                bldg = sm.BuildingInfo.create(facilityID=facilityID, typeID=bldg_id, description="Temporary Structure for Tupia Assets", dateConstructed=datetime.datetime.now(), roofID=1, constructionID=1)
                fa = am.FunctionalAreaLocationRef.create(structureID=bldg.id, functionalAreaID=fa_id)
            else:
                bldg = sm.BuildingInfo.query.filter_by(facilityID=facilityID, typeID=bldg_id).one()
                fa = am.FunctionalAreaLocationRef.query.filter_by(structureID=bldg.id, functionalAreaID=fa_id).one()

            # Add the asset x times to the functional area
            try:
                number = int(num)
            except:
                continue

            for i in range(number):
                am.AssetInfo.create(categoryID=categoryID, locationID=fa.id, note="Tupia Data, late 2017, exact location is unknown")


@click.command()
@click.argument('excel_file', type=click.Path(exists=True))
@click.option('--sheet', prompt='Name of sheet with personnel data')
@with_appcontext
def pull_personnel(excel_file, sheet):
    import openpyxl
    import pickle
    # Open the workbook
    try:
        wb = openpyxl.load_workbook(excel_file)
    except:
        click.Abort
    # Load the worksheet with our data
    try:
        if sheet in [s.title for s in wb.worksheets]:
            ws = wb.get_sheet_by_name(sheet)
        else:
            click.echo('Sheet name not found in workbook')
            click.Abort
    except:
        click.echo('Failed to load worksheet specified')
        click.Abort

    VNPF_COL = click.prompt('VNPF Column').upper()
    PCV_COL = click.prompt('PCV Column').upper()
    NAME_COL = click.prompt('Employee Name Column').upper()
    CCC_COL = click.prompt('Cost Centre Code Column').upper()
    CCN_COL = click.prompt('Cost Centre Name Column').upper()
    ACT_COL = click.prompt('Activity Column').upper()
    DEPT_COL = click.prompt('Deptartment/Unit Column').upper()
    JC_COL = click.prompt('Job Code Column').upper()
    TITLE_COL = click.prompt('Position Title Column').upper()
    CADRE_COL = click.prompt('Cadre Column').upper()
    PN_COL = click.prompt('PN Column').upper()

    #VNPF_COL = 'C'  # For testing - future files will have unknown column ordering
    #PCV_COL = 'D'  # For testing - future files will have unknown column ordering
    #NAME_COL = 'E'  # For testing - future files will have unknown column ordering
    #CCC_COL = 'G'  # For testing - future files will have unknown column ordering
    #CCN_COL = 'H'  # For testing - future files will have unknown column ordering
    #ACT_COL = 'I'  # For testing - future files will have unknown column ordering
    #DEPT_COL = 'J'  # For testing - future files will have unknown column ordering
    #JC_COL = 'L'  # For testing - future files will have unknown column ordering
    #TITLE_COL = 'M'  # For testing - future files will have unknown column ordering
    #CADRE_COL = 'N'  # For testing - future files will have unknown column ordering
    #PN_COL = 'O'  # For testing - future files will have unknown column ordering

    START_ROW = click.prompt('Row number that data begins', type=int)
    MAX_ROW = ws.max_row

    click.echo('Will now begin collecting data from rows {0} to {1}'.format(START_ROW, MAX_ROW))

    personnel = []
    with click.progressbar(xrange(START_ROW, MAX_ROW)) as rows:
        count = START_ROW
        for row in rows:
            row = str(row)

            vnpf = ws[VNPF_COL + row].value
            pcv = ws[PCV_COL + row].value
            name = ws[NAME_COL + row].value
            ccc = ws[CCC_COL + row].value
            ccn = ws[CCN_COL + row].value
            act = ws[ACT_COL + row].value
            dept = ws[DEPT_COL + row].value
            jc = ws[JC_COL + row].value
            title = ws[TITLE_COL + row].value
            cadre = ws[CADRE_COL + row].value
            pn = ws[PN_COL + row].value

            try:
                person = dict(vnpf=int(vnpf) if vnpf else None,
                              pcv=pcv,
                              name=name,
                              ccc=ccc,
                              ccn=ccn,
                              act=act,
                              dept=dept,
                              jc=jc,
                              title=title,
                              cadre=cadre,
                              pn=int(pn) if pn else None)
            except:
                click.echo('Error with row #{0}. Fix the row then try again'.format(count))
                click.Abort

            personnel.append(person)
            count += 1

    pickle.dump(personnel, open('personnel.pkl', 'w'))
    click.echo('Now run the "update_personnel" command')



@click.command()
@click.argument('input', type=click.File('r'), default='personnel.pkl')
@with_appcontext
def update_personnel(input):
    """ Deletes personnel and Position info from database.
        Then adds to personnel and positions.

        Appends any newly found titles and cadre. Does not delete old titles or cadre.
    """
    import pickle
    personnel = pickle.load(input)


    # CONTINUE HERE TUESDAY OR WHENEVER

    # Remove old personnel
    for old_person in pm.PersonnelInfo.query.all():
        pm.PersonnelInfo.delete(old_person)
    # Remove old positions
    for old_position in pm.PositionInfo.query.all():
        pm.PositionInfo.delete(old_position)


    # title dictionary - key: name value: titleID
    titles = {}

    # cadre dictionary - key: cadre value cadreID
    cadres = {}

    # facility dictionary - key: jc value facilityID
    facilities = {}

    for person in personnel:

        # Try to get a known titleID from titles
        try:
            titleID = titles[person['title']]
        # Title is unknown
        except:
            # Try to pull title from database
            try:
                title = pm.TitleRef.query.filter_by(type=person['title']).one()
                titleID = title.id
                cadreID = title.cadreID
            # Title is still unknown
            except:
                # Try to find cadre in database
                try:
                    cadre = pm.CadreRef.query.filter_by(type=person['cadre']).one()
                # Add cadre to database
                except:
                    cadre = pm.CadreRef.create(type=person['cadre'])
                    # Add cadre to cadres
                    cadres[person['cadre']] = cadre.cadreID
                # Lastly add title to database
                title = pm.TitleRef.create(type=person['title'], cadreID=cadre.cadreID)
                # Add title to titles
                titles[person['title']] = title.id
                titleID = title.id



        # Try to get the known cadreID from cadres
        try:
            cadreID = cadres[person['cadre']]
        except:
            try:
                cadre = pm.CadreRef.query.filter_by(type=person['cadre']).one()
                cadreID = cadre.cadreID
            except:
                cadre = pm.CadreRef.create(type=person['cadre'])
                cadres[person['cadre']] = cadre.cadreID
                cadreID = cadre.cadreID


        # Try to get the known facilityID from facilities
        try:
            facilityID = facilities[person['jc']]
        except:
            jobCode = str(person['jc'])[2:]
            facility = fm.FacilityInfo.query.filter_by(facilityID=jobCode).first()
            if not facility:
                facilitYID = None
                facilities[person['jc']] = None
            else:
                facilityID = facility.facilityID
                facilities[person['jc']] = facilityID


        # Now add the person to the database

