# -*- coding: utf-8 -*-
"""Extensions module. Each extension is initialized in the app factory located in app.py."""
from flask import redirect, url_for, flash

from flask_bcrypt import Bcrypt
from flask_caching import Cache
from flask_debugtoolbar import DebugToolbarExtension
from flask_login import LoginManager, login_required, current_user
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect, CSRFError
#from flask_uploads import UploadSet, IMAGES, configure_uploads
from flask_mail import Mail, Message
from flask_admin import Admin, expose, AdminIndexView

# https://gist.github.com/marteinn/5546293
class HomeView(AdminIndexView):
    """Override the home view to expose the root directory so that
    we can render a custom home page.
    """
    
    @expose("/")
    def index(self):
        if not current_user.is_authenticated:
            flash('You must be logged in to view the administrative panels', 'warning')
            return redirect(url_for('public.home'))
        return self.render('admin/home.html')


bcrypt = Bcrypt()
csrf_protect = CSRFProtect()
login_manager = LoginManager()
login_manager.login_view = 'public.home'
login_manager.login_message = u'Login required to proceed'
login_manager.login_message_category = 'info'
db = SQLAlchemy()
migrate = Migrate()
cache = Cache()
debug_toolbar = DebugToolbarExtension()
mail = Mail()
admin = Admin(name='MOHA', url='/', template_mode='bootstrap3', index_view=HomeView(name='Home'))
