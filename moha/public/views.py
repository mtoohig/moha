# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user

from moha.extensions import login_manager, cache
from moha.public.forms import LoginForm, MapFacilityForm, MapStructureForm, MapAssetForm, MapPersonnelForm
from moha.user.models import User
from moha.facility import models as fm
from moha.structure import models as sm
from moha.asset import models as am
from moha.personnel import models as pm
from moha.utils import flash_errors

import structlog
logger = structlog.getLogger(__name__)

blueprint = Blueprint('public', __name__, static_folder='../static')


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    #
    # This is how Flask-Login stores the user information in the session
    #
    return User.get_by_id(int(user_id))


@blueprint.route('/', methods=['GET', 'POST'])
def home():
    """Home page."""
    logger.debug('Enter public home')
    loginForm = LoginForm(request.form)
    mapFacilityForm = MapFacilityForm()
    mapFacilityForm.designationID.choices = [(-1,'---')] + [(d.id, d.name) for d in fm.DesignationRef.query.all()]
    mapStructureForm = MapStructureForm()
    mapStructureForm.structure_type.choices = [('all','all')] + [('B','Building'),('WSR','Water Source'),('WST','Water Storage'),('P','Power'),('S','Sanitation'),('C','Communication'),('CST','Cold Storage'),('V','Vehicle'),('BO','Boat')]
    mapAssetForm = MapAssetForm()
    mapAssetForm.categoryID.choices = [(-1,'---')] + [(c.categoryID, c.type) for c in am.AssetCategoryInfo.query.all()]
    mapPersonnelForm = MapPersonnelForm()
    mapPersonnelForm.cadreID.choices = [(-1,'---')] + [(c.cadreID, c.type) for c in pm.CadreRef.query.all()]
    # Handle logging in
    if loginForm.validate_on_submit():
        login_user(loginForm.user)  # user object comes from the form validation
        logger.info("Logged in")
        flash('Welcome back, {0}.'.format(loginForm.user.full_name), 'success')
        return loginForm.redirect(request.args.get('next'))
    else:
        flash_errors(loginForm)
    return render_template('public/home.html', loginForm=loginForm, mapFacilityForm=mapFacilityForm, mapStructureForm=mapStructureForm, mapAssetForm=mapAssetForm, mapPersonnelForm=mapPersonnelForm)


@blueprint.route('/logout')
@login_required
def logout():
    """Logout."""
    logger.info("Logged out")  # log before action so that the user_id will be available in log file
    logout_user()
    flash('You are logged out.', 'success')
    return redirect(url_for('public.home'))


@blueprint.route('/about')
def about():
    """About page."""
    return render_template('public/about.html')
