# -*- coding: utf-8 -*-
"""Public forms."""
from urlparse import urlparse, urljoin
from flask import request, url_for, redirect
from flask_wtf import Form
from wtforms import PasswordField, StringField, HiddenField, SelectField, IntegerField, DateField
from wtforms.validators import DataRequired, Email
from werkzeug.exceptions import NotFound, HTTPException

from moha.user.models import User

from flask import current_app as app


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


def get_redirect_target():
    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target


class RedirectForm(Form):
    next = HiddenField()

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        if not self.next.data:
            self.next.data = get_redirect_target() or ''

    def redirect(self, endpoint='index', **values):
        if is_safe_url(self.next.data):
            return redirect(self.next.data)
        target = get_redirect_target()
        return redirect(target or url_for(endpoint, **values))


class LoginForm(RedirectForm):
    """Login form."""

    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])

    def validate(self):
        """Validate the form."""
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.query.filter_by(email=self.email.data).first()

        if not self.user:
            self.email.errors.append('Unknown email address')
            return False

        if not self.user.check_password(self.password.data):
            self.password.errors.append('Invalid password')
            return False

        if not self.user.confirmed:
            self.email.errors.append('Please contact another user of this application so that you may be granted full access.')
            return False
        return True


class MapFacilityForm(Form):
    """Map Facility Form"""

    Five_Point_Scale = [(-1, '---'),(1, '1'),(2, '2'),(3, '3'),(4, '4'),(5, '5')]

    designationID = SelectField('Designation',
                                choices=[],
                                )

    status = SelectField('Status/Damage',
                                choices=[(-1,'---'),(0,'Open/Not Damaged'),(1,'Open/Damaged'),(2,'Closed/Not Damaged'),(3,'Closed/Damaged')], default=-1)

    condition = SelectField("Condition",
                            choices=Five_Point_Scale,
                            coerce=int)

    great_less = SelectField("Condition is...",
                            choices=[(0, 'Exactly'),(1, 'Greater than'),(2, 'Less than')], default=0)

    min_overdue = IntegerField("Percent Inspections Overdue", default=0)

    max_overdue = IntegerField("Percent Inspections Overdue", default=100)

    min_operational = IntegerField("Operational Percent", default=0)

    max_operational = IntegerField("Operational Percent", default=100)

    def validate(self):
        """Validate the form."""
        initial_validation = super(MapFacilityForm, self).validate()
        if not initial_validation:
            return False

        return True


class MapStructureForm(Form):
    """Map Form for Structures"""

    Five_Point_Scale = [(-1, '---'),(0, 'Not Inspected'),(1, '1'),(2, '2'),(3, '3'),(4, '4'),(5, '5')]

    structure_type = SelectField('Structure Type', choices=[])

    sub_type = SelectField('Sub-Type', choices=[])

    condition = SelectField("Condition",
                            choices=Five_Point_Scale,
                            coerce=int)

    great_less = SelectField("Condition is...",
                            choices=[(0, 'Exactly'),(1, 'Greater than'),(2, 'Less than')])

    age = IntegerField("Days since inspection")

    def validate(self):
        """Validate the form."""
        initial_validation = super(MapStructureForm, self).validate()
        if not initial_validation:
            return False

        return True


class MapAssetForm(Form):
    """Map Facility Form"""

    categoryID = SelectField('Asset Category',
                                choices=[],
                                )

    status = SelectField('Status',
                                choices=[(-1,'---'),(0,'Operational'),(1,'Non-Operational')]) 

    great_less = SelectField("Avialable...",
                            choices=[(1, 'Since'),(2, 'Prior to')])

    date = DateField('Available date', format="%d/%m/%Y")

    def validate(self):
        """Validate the form."""
        initial_validation = super(MapAssetForm, self).validate()
        if not initial_validation:
            return False

        return True


class MapPersonnelForm(Form):
    """Map Facility Form"""

    cadreID = SelectField('Cadre', choices=[])

    great_less = SelectField("Positions filled...",
                            choices=[(1, 'Greater than'),(2, 'Less than')])

    filled = IntegerField('Percent Positions Filled')

    def validate(self):
        """Validate the form."""
        initial_validation = super(MapPersonnelForm, self).validate()
        if not initial_validation:
            return False

        return True