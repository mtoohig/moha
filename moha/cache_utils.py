from moha.extensions import cache
import moha.map.views as map_views

import structlog
logger = structlog.getLogger(__name__)


def force_update_facility_cache():
	logger.debug('Enter delete facility cache')
	cache.delete_memoized(map_views._facilities)
	cache.delete_memoized(map_views._load_facility)

def force_update_structure_cache():
	logger.debug('Enter delete structure cache')
	cache.delete_memoized(map_views._structures)
	cache.delete_memoized(map_views._load_structure)

def force_update_asset_cache():
	logger.debug('Enter delete asset cache')
	cache.delete_memoized(map_views._assets)
	cache.delete_memoized(map_views._load_assets)

def force_update_personnel_cache():
	logger.debug('Enter delete personnel cache')
	cache.delete_memoized(map_views._personnel)
	cache.delete_memoized(map_views._load_personnel)