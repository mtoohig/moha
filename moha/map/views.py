# -*- coding: utf-8 -*-
"""Map views"""
import datetime

from flask import Blueprint, jsonify, request, flash, render_template
from moha.extensions import cache
from moha.facility import models as fm
from moha.structure import models as sm
from moha.asset import models as am
from moha.personnel import models as pm

from moha import utils

import structlog
logger = structlog.getLogger(__name__)

blueprint = Blueprint('map', __name__, url_prefix='/map', static_folder='../static')


"""
These functions generate map data.

Their functions are cached so that it will load quickly on the user side.

Business functions that create/update a facility delete the cache so the map will be forced to update.
"""



# ============================ #
#  M A P  F A C I L I T I E S  #
# ============================ #

@cache.memoize(timeout=604800)
def _facilities(designationID, status, condition, great_less, min_overdue, max_overdue, min_operational, max_operational):
	facilities = fm.FacilityInfo.query
	# Filter the results
	if designationID >= 0:
		facilities = facilities.filter_by(designationID=designationID)
	# Filter the results
	if status >= 0:
		if status == 0:  # Open/Not Damaged
			facilities = facilities.filter_by(status=True, damaged=False)
		elif status == 1:  # Open/Damaged
			facilities = facilities.filter_by(status=True, damaged=True)
		elif status == 2:  # Closed/Not Damaged
			facilities = facilities.filter_by(status=False, damaged=False)
		elif status == 3:  # Closed/Damaged
			facilities = facilities.filter_by(status=False, damaged=True)
		else:
			logger.error('Unknown status code {0} provided from MapForm'.format(status))
	# Load the facilities
	facilities = facilities.all()
	# Filter the results
	if condition >= 0 and great_less >= 0:
		if great_less == 1:  # greater or equal to condition
			facilities = [f for f in facilities if f.condition() >= condition]
		elif great_less == 2:  # less or equal than condition
			facilities = [f for f in facilities if f.condition() <= condition]
		else:
			facilities = [f for f in facilities if f.condition() == condition]
	if min_overdue > 0 or max_overdue <= 100:
		facilities = [f for f in facilities if max_overdue >= f.overdue_percent()*100 >= min_overdue]
	if min_operational > 0 or max_operational <= 100:
		facilities = [f for f in facilities if max_operational >= f.operational_percent()*100 >= min_operational]
	return [f.as_dict() for f in facilities]

@cache.memoize(timeout=604800)
def _load_facility(facilityID, small):
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	stypes =   [('B', 'Buildings', 'home'),
				('WSR', 'Water Sources', 'tint'),
				('WST', 'Water Storages', 'tint'),
				('P', 'Power Sources', 'bolt'),
				('S', 'Sanitations', 'shower'),
				('C', 'Communications', 'phone'),
				('CST', 'Cold Storages', 'snowflake'),
				('V', 'Vehicles', 'car'),
				('BO', 'Boats', 'ship')]
	tr = """<tr><td>{type}</td><td style="text-align:center;vertical-align: middle;">{condition}</td></tr>"""
	none_defined = """<label class="label label-danger">None</label>"""
	rows = ""
	for s in stypes:
		if facility.has_structure_type(s[0]):
			condition = facility.condition(s[0])
			rows += tr.format(type=s[1], condition=condition)
		else:
			rows += tr.format(type=s[1], condition=none_defined)
	return render_template('maps/{0}.html'.format('facility_small_popup' if small else 'facility_popup'), facility=facility, rows=rows)

@blueprint.route('/facilities/')
def facilities():
	logger.debug('Enter map facilities')
	# Get designation selection from form
	try:
		designationID = int(request.args.get('designationID'))
	except:
		designationID = -1
	try:
		status = int(request.args.get('status'))
	except:
		status = -1
	# Get condition
	try:
		condition = int(request.args.get('condition'))
		great_less = int(request.args.get('great_less'))
	except:
		condition = -1
		great_less = -1
	# Get overdue
	try:
		min_overdue = int(request.args.get('min_overdue'))
		max_overdue = int(request.args.get('max_overdue'))
	except:
		min_overdue = -1
		max_overdue = 101
	# Get Operational
	try:
		min_operational = int(request.args.get('min_operational'))
		max_operational = int(request.args.get('max_operational'))
	except:
		min_operational = -1
		max_operational = 101
	data = _facilities(designationID, status, condition, great_less, min_overdue, max_overdue, min_operational, max_operational)
	return jsonify({"data": data})

@blueprint.route('/load_facility')
def load_facility():
	logger.debug('Enter load_facility')
	small = request.args.get('small')
	facilityID = request.args.get('facilityID')
	data = _load_facility(facilityID, small)
	return jsonify({"data": data})



# ============================ #
#  M A P  S T R U C T U R E S  #
# ============================ #

# No cache because I don't want to build a delete_memoized function into admin.py
# Also this function is so simple it doesn't need to be cached
@blueprint.route('/get_sub_type')
def sub_type():
	"""Returns sub types of a structure_type"""
	structure_type = request.args.get('structure_type')
	if structure_type == 'B':
		stypes = sm.BuildingRef.query.all()
	elif structure_type == 'C':
		stypes = sm.CommunicationRef.query.all()
	elif structure_type == 'CST':
		stypes = sm.ColdStorageRef.query.all()
	elif structure_type == 'S':
		stypes = sm.SanitationRef.query.all()
	elif structure_type == 'P':
		stypes = sm.PowerRef.query.all()
	elif structure_type == 'V':
		stypes = sm.VehicleRef.query.all()
	elif structure_type == 'BO':
		stypes = sm.BoatRef.query.all()
	elif structure_type == 'WSR':
		stypes = sm.WaterSourceRef.query.all()
	elif structure_type == 'WST':
		stypes = sm.WaterStorageRef.query.all()
	else:
		return jsonify({"success": False})

	data = [(-1, '---')] + [(st.id, st.type) for st in stypes]
	return jsonify({"success": True, "data": data})


@cache.memoize(timeout=604800)
def _structures(request):
	# Get structure type
	try:
		structure_type = request.args.get('structure_type')
	except:
		pass
	# Filter the results
	if structure_type != 'all' and structure_type is not None:
		# Get the type tables for the selected structure_type
		if structure_type == 'B':
			structure_table = sm.BuildingInfo
		elif structure_type == 'WSR':
			structure_table = sm.WaterSourceInfo
		elif structure_type == 'WST':
			structure_table = sm.WaterStorageInfo
		elif structure_type == 'P':
			structure_table = sm.PowerInfo
		elif structure_type == 'S':
			structure_table = sm.SanitationInfo
		elif structure_type == 'C':
			structure_table = sm.CommunicationInfo
		elif structure_type == 'CST':
			structure_table = sm.ColdStorageInfo
		elif structure_type == 'V':
			structure_table = sm.VehicleInfo
		elif structure_type == 'BO':
			structure_table = sm.BoatInfo
		else:
			logger.error('Unknown structure_type: {0}'.format(structure_type))

		structures = structure_table.query

		try:
			sub_type = int(request.args.get('sub_type'))
		except:
			sub_type = -1
		# Filter the structure by typeID
		if sub_type > 0:
			structures = structures.filter_by(typeID=sub_type)

	else:
		structures = sm.StructureInfo.query

	structures = structures.all()

	# Get condition
	try:
		condition = int(request.args.get('condition'))
		great_less = int(request.args.get('great_less'))
	except:
		condition = -1
		great_less = -1
	# Filter the results
	if condition >= 0 and great_less >= 0:
		if condition == 0:  # not inspected
			structures = [s for s in structures if s.inspection is None]
		elif great_less == 0:  # exact to condition
			structures = [s for s in structures if s.condition == condition and s.condition is not None]
		elif great_less == 1:  # greater or equal to condition
			structures = [s for s in structures if s.condition >= condition and s.condition is not None]
		else:  # less or equal than condition
			structures = [s for s in structures if s.condition <= condition and s.condition is not None]

	# Get inspection age
	try:
		age = int(request.args.get('age'))
	except:
		age = -1
	if age >= 0:
		structures = [s for s in structures if s.inspection]
		structures = [s for s in structures if s.inspection.age >= age]
	return [s.as_dict() for s in structures]

@cache.memoize(timeout=604800)
def _load_structure(structureID):
	structure = sm.StructureInfo.query.filter_by(id=structureID).first_or_404()
	return render_template('maps/structure_popup.html', structure=structure)


@blueprint.route('/structures')
def structures():
	logger.debug('Enter map structures')
	data = _structures(request)
	return jsonify({"data": data})


@blueprint.route('/load_structure')
def load_structure():
	logger.debug('Enter load structure')
	structureID = request.args.get('structureID')
	data = _load_structure(structureID)
	return jsonify({"data": data})



# ==================== #
#  M A P  A S S E T S  #
# ==================== #

@cache.memoize(timeout=604800)
def _assets(categoryID, status, date, great_less):
	# Filter map only loads if a category is selected
	facilities = fm.FacilityInfo.query.all()
	category = am.AssetCategoryInfo.query.filter_by(categoryID=categoryID).first()
	if category:
		# Get the list of all sub categories
		cat_ids = category.category_ids
		facility_list = []
		for f in facilities:
			for a in f.assets:
				# filter assets by date that the asset is available/purchased
				if date and great_less >= 0:
					if great_less == 1:  # Since
						if a.datePurchased < datetime.datetime.strptime(date, '%d/%m/%Y').date():
							continue  # skip older than date assets
					elif great_less == 2:  # Prior
						if a.datePurchased >= datetime.datetime.strptime(date, '%d/%m/%Y').date():
							continue  # skip younger than date assets
					else:
						pass
				# Find assets whose category ID is in the list of sub-categoryIDs
				if a.categoryID in cat_ids:
					# Add the facility to the list if the status of the asset matches what the form says
					if status == 0 and a.status == True:
						facility_list.append(f)
					elif status == 1 and a.status == False:
						facility_list.append(f)
					elif status == -1:
						facility_list.append(f)
					else:
						continue
					break  # break so we don't continue to loop through a facility's assets once we find one match
		facilities = facility_list
	return [f.as_dict(categoryID=categoryID) for f in facilities]

@cache.memoize(timeout=604800)
def _load_assets(facilityID, categoryID):
	# Get the objects
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	category = am.AssetCategoryInfo.query.filter_by(categoryID=categoryID).first_or_404()
	# Get all sub category ids
	cat_ids = category.category_ids
	# Get the facility's assets if asset's categoryID is in the list of cat_ids
	assets = [a for a in facility.assets if a.categoryID in cat_ids]
	# Re-organize the assets in a dictionary for easy parsing in the popup template
	a = {}
	for asset in assets:
		s = asset.location.structure.identifier
		fa = asset.location.functionalArea.type
		cat = asset.category.type
		try:
			a[s][fa][cat] += 1
		except:
			try:
				a[s][fa][cat] = 0
				a[s][fa][cat] += 1
			except:
				a[s] = {}
				a[s][fa] = {}
				a[s][fa][cat] = 0
				a[s][fa][cat] += 1
	return render_template('maps/asset_popup.html', facility=facility, assets=a)

@blueprint.route('/assets')
def assets():
	"""Show assets across the map"""
	logger.debug('Enter map assets')
	# Get categoryID 
	try:
		categoryID = int(request.args.get('categoryID'))
	except:
		categoryID = -1
	# Get status
	try:
		status = int(request.args.get('status'))
	except:
		status = -1
	# Get date
	try:
		date = request.args.get('date')
		great_less = int(request.args.get('great_less'))
	except:
		date = -1
		great_less = -1
	# Return markers if a category is selected
	if categoryID >= 0:
		data = _assets(categoryID, status, date, great_less)
		return jsonify({"data": data})
	else:
		return jsonify({"success": False})

@blueprint.route('/load_assets')
def load_assets():
	logger.debug('Enter load_assets')
	facilityID = request.args.get('facilityID')
	categoryID = request.args.get('categoryID')
	data = _load_assets(facilityID, categoryID)
	return jsonify({"data": data})



# ========================== #
#  M A P  P E R S O N N E L  #
# ========================== #

@cache.memoize(timeout=604800)
def _personnel(filled, great_less, cadreID):
	# Get all facilities
	facilities = fm.FacilityInfo.query.all()
	cadre = pm.CadreRef.query.filter_by(cadreID=cadreID).first_or_404()
	# Get the list of all titles' IDs under this cadreID
	title_ids = cadre.cadre_title_ids
	facility_list = []
	# If the facility has a position that belongs to the cadreID
	# Add the facility to the list 
	for f in facilities:
		for p in f.positions:
			if p.titleID in title_ids:
				facility_list.append(f)
				break
	return [f.as_dict() for f in facility_list]

@cache.memoize(timeout=604800)
def _load_personnel(facilityID, cadreID):
	# Get objects
	facility = fm.FacilityInfo.query.filter_by(facilityID=facilityID).first_or_404()
	cadre = pm.CadreRef.query.filter_by(cadreID=cadreID).first_or_404()
	# Get list of all title ids from this and all sub-cadre
	title_ids = cadre.title_ids
	positions = [p for p in facility.positions if p.titleID in title_ids]
	# Re-organize personnel into dictinary for easy parsing in the popup template	
	p = {}
	for position in positions:
		c = position.title.cadre.type
		t = position.title.type
		pn = position.pn
		try:
			p[c][t][pn] = position.as_dict()
		except:
			try:
				p[c][t] = {}
				p[c][t][pn] = position.as_dict()
			except:
				p[c] = {}
				p[c][t] = {}
				p[c][t][pn] = position.as_dict()
	return render_template('maps/personnel_popup.html', facility=facility, personnel=p)

@blueprint.route('/personnel')
def personnel():
	try:
		filled = int(request.args.get('filled'))
		great_less = int(request.args.get('great_less'))
	except:
		filled = -1
		great_less = -1
	# Try to get the cadre from the form
	try:
		cadreID = int(request.args.get('cadreID'))
	except:
		cadreID = -1
	# Only if there is a cadre do we load the map
	if cadreID >= 0:
		data = _personnel(filled, great_less, cadreID)
		return jsonify({"data": data})
	else:
		return jsonify({"success": False})

@blueprint.route('/load_personnel')
def load_personnel():
	logger.debug('Enter load personnel')
	facilityID = request.args.get('facilityID')
	cadreID = request.args.get('cadreID')
	data = _load_personnel(facilityID, cadreID)
	return jsonify({"data": data})	