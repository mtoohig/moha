"""
A filter to attach a 'request_id' to each log. 

source:
http://blog.mcpolemic.com/2016/01/18/adding-request-ids-to-flask.html

Ideal for giving the end-user a id that they can use to report their issue.
""" 

import uuid
import logging
import flask

from flask_login import current_user

# Generate a new request ID, optionally including an original request ID
def generate_request_id(original_id=''):
    new_id = uuid.uuid4()

    if original_id:
        new_id = "{},{}".format(original_id, new_id)

    return new_id

# Returns the current request ID or a new one if there is none
# In order of preference:
#   * If we've already created a request ID and stored it in the flask.g context local, use that
#   * If a client has passed in the X-Request-Id header, create a new ID with that prepended
#   * Otherwise, generate a request ID and store it in flask.g.request_id
def request_id():
    if getattr(flask.g, 'request_id', None):
        return flask.g.request_id

    headers = flask.request.headers
    original_request_id = headers.get("X-Request-Id")
    new_uuid = generate_request_id(original_request_id)
    flask.g.request_id = new_uuid

    return new_uuid

class RequestIdFilter(logging.Filter):
    # This is a logging filter that makes the request ID available for use in
    # the logging format. Note that we're checking if we're in a request
    # context, as we may want to log things before Flask is fully loaded.
    def filter(self, record):
        record.request_id = request_id() if flask.has_request_context() else ''
        return True


def user_id():
    if not current_user.is_anonymous:
        return current_user.id
    else:
        return 'guest'

class UserIdFilter(logging.Filter):

    def filter(self, record):
        record.user_id = user_id() if flask.has_request_context() else ''
        return True