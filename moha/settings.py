# -*- coding: utf-8 -*-
"""Application configuration."""
import os
import logging


class Config(object):
    """Base configuration."""

    DEBUG = True
    # Project
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    SECRET_KEY = os.environ.get('moha_SECRET', 'secret-key')  # TODO: Change me

    DEBUG_TB_INTERCEPT_REDIRECTS = False
    
    # Logging
    LOG_FILENAME = os.path.join(APP_DIR, 'log.txt')
    LOG_FILESIZE = 1024 * 1024 * 50  # 50 MB
    LOG_BACKUPCOUNT = 5

    # WTForms
    WTF_CSRF_SECRET_KEY = 'secret'
    WTF_CSRF_TIME_LIMIT = 3600
    
    # Uploads
    PICTURE_FOLDER = os.path.abspath(os.path.join(APP_DIR, 'static', 'files', 'pictures'))
    FILE_FOLDER = os.path.abspath(os.path.join(APP_DIR, 'static', 'files', 'files'))
    EXCEL_TEMPLATE_FOLDER = os.path.abspath(os.path.join(APP_DIR, 'static', 'files', 'excel_templates'))
    TEMP_FOLDER = os.path.abspath(os.path.join(APP_DIR, 'static', 'files', 'tmp'))
    MAP_FOLDER = os.path.abspath(os.path.join(APP_DIR, 'static', 'files', 'maps'))

    ALLOWED_PICTURE_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'bmp'])
    ALLOWED_SITEPLAN_EXTENSIONS = set(['pdf'])
    ALLOWED_FILE_EXTENSIONS = set(['xls', 'xlsx', 'doc', 'docx', 'ppt', 'pptx']) | ALLOWED_SITEPLAN_EXTENSIONS
    ALLOWED_EXTENSIONS = ALLOWED_PICTURE_EXTENSIONS | ALLOWED_FILE_EXTENSIONS  # union of 'sets'
    MAX_CONTENT_LENGTH = 32 * 1024 * 1024 # Upload limit

    # Mail
    MAIL_DEBUG=True
    MAIL_SERVER = '10.255.27.10'
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_DEFAULT_SENDER = 'mtoohig@vanuatu.gov.vu'
    MAIL_ADMINS = ['mtoohig@vanuatu.gov.vu']#,'gkalpokas@vanuatu.gov.vu']

    # Database
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Password hashing
    BCRYPT_LOG_ROUNDS = 13


class ProdConfig(Config):
    """Production configuration."""

    ENV = 'prod'
    DEBUG = False
    DEBUG_TB_ENABLED = False  # Disable Debug toolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    LOG_LEVEL = logging.INFO

    SQLALCHEMY_DATABASE_URI = 'mssql+pymssql://emfms_admin:emfmspwd@10.255.134.91/MOH_ASSETS'

    # Cache
    CACHE_TYPE = 'redis'  # Can be 'simple', "memcached", "redis", etc.
    #CACHE_DEFAULT_TIMEOUT = 300
    CACHE_KEY_PREFIX = '__moha__'  # Asset Unit Webapp
    CACHE_REDIS_HOST = 'localhost'
    CACHE_REDIS_PORT = 6379
    CACHE_REDIS_DB = 0


class DevConfig(Config):
    """Development configuration."""

    ENV = 'dev'
    DEBUG = True
    DEBUG_TB_ENABLED = True

    LOG_LEVEL = logging.DEBUG

    SQLALCHEMY_DATABASE_URI = 'mssql+pymssql://emfms_admin:emfmspwd@10.255.134.91/MOH_ASSETS'
    
    # Cache
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.


class TestConfig(Config):
    """Test configuration."""

    TESTING = True
    DEBUG = True

    BCRYPT_LOG_ROUNDS = 4  # For faster tests; needs at least 4 to avoid "ValueError: Invalid rounds"
    WTF_CSRF_ENABLED = False  # Allows form testing
